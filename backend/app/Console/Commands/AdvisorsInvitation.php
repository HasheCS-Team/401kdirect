<?php

namespace App\Console\Commands;

use \Illuminate\Support\Facades\DB;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Collection\AdvisorList;

use Event;
use App\Events\AdvisorsInvitationEvent;


class AdvisorsInvitation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'advisors_invite:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing Advisor Invitation Emails Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $advisors = DB::table('advisor_collection as ac')
        //                 ->select('ac.Id', 'ac.FirstName', 'ac.LastName', 'ac.Email', 'ac.IsInvited', 'ac.InvitationToken', 'ac.InvitationKey')
        //                 ->where('InvitationKey','Yes')
        //                 ->where('IsInvited',0)
        //                 ->where('InvitationToken',NULL)
        //                 ->get();
        // // print_r($advisors);die;

        // $helperFunctions = app(\App\Helpers\Classes\HelperFunctions::class);

        // foreach ($advisors as $value) {

        //     $advisor = AdvisorList::where('Id',$value->Id)->first();
        //     $randomPassword = str_random(8);
        //     $token = $helperFunctions->encrypt($value->Email);
        //     $value->InvitationLink = env('ANGULAR_APP_PATH').'invitation-signup/'.$advisor->Id.'/'.str_replace('/', '', $token);

        //     $event = Event::fire(new AdvisorsInvitationEvent($value));
        //     if ($event) {
        //         $advisor->IsInvited = 1;
        //         $advisor->InvitationKey = 'Sent';
        //         $advisor->InvitationToken = $token;
        //         $advisor->save();

        //         \Log::info('Opportunity Reminder Email sent: Advisor Id:'.$value->Id);
        //     } else {
        //         $advisor->InvitationKey = 'No';
        //         $advisor->save();
                
        //         \Log::info('Opportunity Reminder Email not sent: Advisor Id:'.$value->Id);
        //     }
        // }
    }
}
