<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

//Events
use Event;
//facades used in controller
use Socialite;
use Auth;
use Input;
use Hash;
use Config;
use Mail;
use DB;
use File;
use Carbon\Carbon;
use Image;

use RuntimeException;
use App\Models\FundCategory;
use App\Models\InvestmentOption as IO;

class CopyProductData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy:productData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd('stop');

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        try {
            \DB::beginTransaction();

            $data = \DB::table('providers_rj')->get();
            $providersData = [];
            if($data) {
                foreach ($data as $key => $value) {
                    // $data = \DB::table('fund_categories_live')->where('UserType', '=', 'provider')->where('UserId', '=', $value->UserId)->get();
                    $data = \DB::table('fund_categories_live')->whereIn('UserType', ['Provider', 'provider'])->where('UserId', '=', $value->UserId)->get();
                    // echo "<pre>";
                    // print_r($data);exit();
                    $toAddRecord = [];
                    if ($data) {
                        foreach ($data as $record) {
                            $nameFundCategory = $record->Name;
                            if ($nameFundCategory == 'diversified emerging markets') {
                                $nameFundCategory = 'Emerging Market Equity';
                            }
                            $objFundCategory = FundCategory::where('Name', '=', $nameFundCategory)->where('UserType', '=', 'broker_dealer_product')->first();
                            $toAddRecord[$record->Id] = $objFundCategory->Id;
                        }
                    }
                    // echo '<pre>'; print_r($toAddRecord);
                    // dd($toAddRecord);
                if ($toAddRecord) {
                    foreach ($toAddRecord as $key => $row) {
                        $newFundCategoryId = $row;
                        $record = \DB::table('investment_options_live')->where('FundCategoryId', '=', $key)->get(); //todo table name will be investment_options_street
                        $record = json_decode(json_encode($record), True);
                        array_walk($record, function (&$key) use ($newFundCategoryId, $value) {
                            $key['ProductId'] = 1; // todo need to check product id before running this script
                            $key['FundCategoryId'] = $newFundCategoryId;
                            $key['ProviderId'] = $value->UserId;
                            unset($key['Id']);
                        });
                        $providersData[$value->UserId][] = $record;
                        IO::insert($record);
                    }
                    // dd($providersData);
                }
                }
                echo 'sucess';
                 DB::commit();
            }


        } catch (RuntimeException $e) {
            DB::rollback();
            echo $e;
            echo 'error';
        }
    }
}
