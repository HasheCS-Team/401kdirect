<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Collection\InvestmentOption;
use App\Models\User;
use App\Models\UserLogin;
use App\Models\Users\Advisor;
use App\Models\AdvisorCompany;
use App\Models\Users\Provider;
use App\Models\Users\InvestmentManager;
use App\Models\InvestmentManager\InvestmentManagerOpportunity as IMO;
use App\Models\Provider\Wholesaler;
use App\Models\Provider\WholesalerZips;
use App\Models\InvestmentManager\WholesalerZips as IMWholesalerZips;
use App\Models\InvestmentManager\Wholesaler as WholesalerIM;
use App\Models\Activity\Activity;
use App\Models\Collection\AdvisorList;
use App\Models\UnsubscribeUser;
use App\Models\FeeFormula;
use App\Models\FeePricingRule;

use App\Helpers\Classes\HelperFunctions;

use App\Traits\Controllers\AdminUserCrud;
use App\Traits\UserPasswordReset;

use Illuminate\Http\Request;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

//Events
use Event;
use App\Events\ProposalEmailEvent;
use App\Events\SignUpEvent;
use App\Events\SignUpAccountVerifyEvent;
use App\Events\SignUpAdvisorAccountApproveEvent;
use App\Events\InviteAdvisorSignupEvent;
use App\Events\NewAccountEvent;
use App\Events\AdvisorApprovedEmail;
use App\Events\AdminInviteApprovedEvent;
use App\Models\Users\SaleDeskUser;
use App\Models\SaleDeskAdvisorSession;
use App\Events\ProviderConnectionEvent;
//facades used in controller
use Socialite;
use Auth;
use Input;
use Hash;
use Config;
use Mail;
use DB;
use File;
use Carbon\Carbon;
use Image;

use RuntimeException;
use App\Models\FundCategory;

class MergeStreetDataInRJ extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merge:streetRj';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        //todo add source coulumn in users table....add 'street_user_id' in users table......ad 'street_user_id' in Fundcategory table
        // todo add "street_company_id" column in advisor_companies
        //first i will insert data in user table. then other relation data will be found bu thier old id
        // todo add column source in rj db
        // Delete already added funds on rj by query
        //todo below is sql
        //DELETE f,i FROM fund_categories AS f
        //JOIN investment_options AS i ON i.fundCategoryId = f.Id
        //WHERE f.UserType = 'Provider';

        // todo rename cuna mutual manual CUNA Mutual Retirement Solutions to CUNA Mutual
        // todo dont take tension of fee shedule from street providers...
        // todo need to change DB name before running updated script
        // todo need to delete massmutual RJ PW before running updated script
echo "stop here";exit();
dd("rurrururru");

        try {
            \DB::beginTransaction();

            /**
             *
             * ------------------------------------ Remove duplicates of advisor and sales desk from users tables
             *
             */

            $sql = 'SELECT
                  *
                FROM
                  users
                WHERE street_user_id = 0
                GROUP BY
                  Email
                HAVING COUNT(*)> 1';

            $result = DB::SELECT($sql);
            if ($result) {
                foreach ($result as $row) {
                    $parts =  explode('@', $row->Email);
                    $name  =  $parts[0].'_duplicate';
                    // Stick the @ back onto the domain since it was chopped off.
                    $email =  $name. "@" . $parts[1];
                    User::where('Email', '=', $row->Email)->where('AccountType', '=', 'Advisor')->withTrashed()->update(['Email' => $email]);
                }
            }

            // Make street tables users data unique emails of advisors and sale desks
            $sql = 'SELECT
                  *
                FROM
                  users_street
                GROUP BY
                  Email
                HAVING COUNT(*)> 1';

            $result = DB::SELECT($sql);
            if ($result) {
                foreach ($result as $row) {
                    $parts =  explode('@', $row->Email);
                    $name  =  $parts[0].'_duplicate';
                    // Stick the @ back onto the domain since it was chopped off.
                    $email =  $name. "@" . $parts[1];
                    DB::table('users_street')->where('Email', '=', $row->Email)->where('AccountType', '=', 'Advisor')->withTrashed()->update(['Email' => $email]);
                }
            }

            /**  copy street users and paste in RJ
             * ----------------------------------------------------------------------------------------------
             * ----------------------------------------------------------------------------------------------
             */
            $record = $duplicates = [];
            $data = \DB::table('users_street')->get();
            foreach ($data as $row) {
                $accountType = strtolower($row->AccountType);
//                if (in_array($accountType, ['provider', 'provider wholesaler', 'investment manager', 'im wholesaler', 'advisor'])) {
                if (in_array($accountType, ['provider', 'provider wholesaler', 'advisor'])) {
                    // Duplicate checks
                    $obj = User::where('Email', '=', $row->Email)->first();
                    if ($obj) {
                        $duplicates[$row->AccountType][] = $obj->toArray();
                        if (in_array($accountType, ['advisor'])) {
                            $obj->Password = $row->Password;
                            $obj->save();
                        }
                    } else {
                        $row->street_user_id = $row->Id;
                        $row->RjFlag = 0;
                        unset($row->Id);
                        $record[] = (array)$row;
                    }
                }
            }

            foreach (array_chunk($record,500) as $t) {
                User::insert($t);
            }

            echo 'step1';
            echo 'Duplicates : '.count($duplicates);
            echo 'Records : '.count($record);
            // exit;

//            dd('step1-complete');
            //todo important rjflag will be zero
            /**
             * -------------------------------------------------------------------------------------------------
             *                      END
             * -------------------------------------------------------------------------------------------------
             */

            /**
             *
             * -----------------------------------------Add advisors data in advisors table.copy from advisors-street and paste in advisors table
             *
             */

            $uniqueAdvisors = [];
            $data = \DB::table('users')->where('street_user_id', '>', 0)->where('AccountType', '=', 'Advisor')->get();
            foreach ($data as $row) {
                $advisor = (array)\DB::table('advisors_street')->where('UserId', '=', $row->street_user_id)->first();
                $advisor['UserId'] = $row->Id;
                $uniqueAdvisors[] = $advisor;
            }
            Advisor::insert($uniqueAdvisors);

            echo "unique advisors : ".count($uniqueAdvisors);

            /**
             *
             * ----------------------------------------END
             *
             */

            /**
             *
             * ------------------------------------------------ Add advisor companies data
             *
             */

            $streetAdvisorCompanies = \DB::table('advisor_companies_street')->get();
            $toAddStreetAdvisorCompanies = [];
            foreach ($streetAdvisorCompanies as $row) {
                $email = '';
                $objUser = User::where('street_user_id', '=', $row->AdvisorId)->first();
                if ($objUser) {
                    $id = $objUser->Id;
                } else {
                    $email = \DB::table('users_street')->where('Id', '=', $row->AdvisorId)->first()->Email;
                    $id = User::where('Email', '=', $email)->first()->Id;
                }

                //todo need to check which columns will reset to default
                $row->street_company_id = $row->Id;
                $row->PlanIdExcel = null;
                $row->AdvisorEmail = null;
                $row->AdvisorId = $id;
                $row->ProviderId = null;
                $row->OldCurrentProvider = 0;
                $row->CurrentProviderName = null;
                $row->ParentId = null;
                $row->Email = null;
                $row->FiduciaryDocStatus = null;
                $row->CurrentBroker = null;
                $row->Percentage = null;
                $row->Compensation = null;
                $row->CreatedBy = $id;
                $row->UpdatedBy = $id;
                $row->RjFlag = 0;
                unset($row->Id);
                $count = AdvisorCompany::where('PlanName', '=', $row->PlanName)->where('AdvisorId', $id)->count();
                if( $count == 0 ) {
                    $toAddStreetAdvisorCompanies[] = (array)$row;
                }
            }
            AdvisorCompany::insert($toAddStreetAdvisorCompanies);

            echo 'step-3';

            /**
             *
             * ------------------------------------------------- END
             *
             */

            /**
             * Copy providers data from street and paste in RJ.. Keep RJ providers id in case of same provider
             * ------------------------------------------------------------------------------------------------
             * ------------------------------------------------------------------------------------------------
             */

            // Question: what we will insert in case of providers as their is different data in providers.some are same.
            $data = \DB::table('users')->where('street_user_id', '>', 0)->where('AccountType', '=', 'Provider')->get();
            $rjProviders = \DB::table('providers')->get();
            $rjProvidersFirmNames = array_column($rjProviders, 'FirmName');
            $streetProviders = $toAddProviders = [];
            foreach ($data as $row) {
                $streetProviders[$row->Id] = \DB::table('providers_street')->where('UserId', '=', $row->street_user_id)->first();
            }
            // get array difference
            foreach ($streetProviders as $key => $row) {
                if (!in_array($row->FirmName, $rjProvidersFirmNames) && $row) {
                    $row->UserId = $key; // replace new key
                    $toAddProviders[] = (array)$row;
                }
            }
            Provider::insert($toAddProviders);
            echo 'step-4';
//            dd('step2-complete');
            /**
             * -----------------------------------------------------------------------------------------------------------
             *                       END
             * -----------------------------------------------------------------------------------------------------------
             *
             */

            /** add provider wholesalers data
             * ------------------------------------------------------------------------------------------------------------
             * ------------------------------------------------------------------------------------------------------------
             */

            // Now prepare data to insert provider wholesalers
           $arrStreetProviders = $arrStreetRJProviders = [];    
            $arrProviders = \DB::table('providers_street')->get();
            if ($arrProviders) {
                foreach ($arrProviders as $key => $row) {
                   $objRJProviders = \DB::table('providers_rj')->where('FirmName', '=', $row->FirmName)->first();
                   if ($objRJProviders) {
                        $arrStreetRJProviders[] = $objRJProviders->UserId;
                   } else {
                        $arrStreetProviders[] = $row->UserId;
                   }
                }
            }

            $arrStreetPW = $tempArrStreetPW = [];
            if ($arrStreetProviders) {
                foreach ($arrStreetProviders as $key => $row) {
                    $pId = User::where('street_user_id', '=', $row)->first()->Id;
                    $pw = \DB::table('providers_wholesaler_street')->where('ProviderId', '=', $row)->get();
                    if ($pw) {
                        foreach ($pw as $key => $value) {
                            $tempUserId = $value->UserId;
                            $pw[$key]->UserId = User::where('street_user_id', '=', $value->UserId)->first()->Id;
                            $pw[$key]->ProviderId = $pId;
                            $newUserId = $value->UserId;
                            $tempArrStreetPW[$tempUserId] = $newUserId;
                            $pwData = json_decode(json_encode($pw[$key]), True);
                            $arrStreetPW[] = $pwData;
                        }
                    }
                }
                Wholesaler::insert($arrStreetPW);

                $tempArrZip = [];    
                if ($tempArrStreetPW) {
                    foreach ($tempArrStreetPW as $key => $value) {
                        $streetzips = \DB::table('providers_wholesaler_zips_street')->where('WholesalerId', '=', $key)->get();
                        array_walk($streetzips, function (&$key) use ($value) {
                            $key->WholesalerId = $value;
                            unset($key->Id);
                        });
                        if (!empty($streetzips)) {
                            $streetzips = json_decode(json_encode($streetzips), True);
                            $tempArrZip[] = count($streetzips);
                            WholesalerZips::insert($streetzips);
                        }
                    }
                }
            }

            $arrStreetRJPW = $tempArrStreetRJPW = [];
            $tempArrZip = [];
            if ($arrStreetRJProviders) {
                foreach ($arrStreetRJProviders as $key => $row) {
                    $pw_default = \DB::table('providers_wholesaler')->where('ProviderId', '=', $row)->where('Status', '=', "Default_Wholesaler")->first();

                    $pws1 = \DB::table('providers_wholesaler')->where('ProviderId', '=', $row)->get();
                    if (count($pws1) < 3) {
                        $pws = \DB::table('providers_wholesaler')->where('ProviderId', '=', $row)->where('Status', '<>', "Default_Wholesaler")->first();
                        $pws_user = User::where('Id', $pws->UserId)->first();
                        
                        // assign proposals to default here
                        if ($pws) {
                            \DB::table('provider_stats')->where('WholesalerId', '=', $pws->UserId)->update(['WholesalerId' => $pw_default->UserId]);
                        }

                        // check if wholesaler exists in street. If not then update default wholesaler email
                        $pw_in_str = \DB::table('users_street')->where('Email', '=', $pws_user->Email)->first();
                        if (!$pw_in_str) {
                            User::where('Id', $pw_default->UserId)->update(['Email' => $pws_user->Email]);
                            User::where('Id', '=', $pws->UserId)->forceDelete();
                        }
                        
                        // Need to remove provider wholesaler from user and provider whole saler table
                        Wholesaler::where('UserId', '=', $pws->UserId)->forceDelete();
                        WholesalerZips::where('WholesalerId', '=', $pws->UserId)->forceDelete();

                        $objUser = User::where('Id', '=', $row)->first();
                        if ($objUser->street_user_id) {
                            $providerIdStreet = $objUser->street_user_id;
                        } else {
                            $email = $objUser->Email;
                            $providerIdStreet = \DB::table('users_street')->where('Email', '=', $email)->first()->Id;
                        }

                        $arrProvidersWholerSalerStreet= \DB::table('providers_wholesaler_street')->where('ProviderId', '=', $providerIdStreet)->where('Status', '<>','Default_Wholesaler')->get();
                        foreach ($arrProvidersWholerSalerStreet as $key => $value) {
                            $tempUserId = $value->UserId;
                            $pwEmailStreet = \DB::table('users_street')->where('Id', '=', $value->UserId)->first()->Email;
                            $pwIdRJ = \DB::table('users')->where('Email', '=', $pwEmailStreet)->first()->Id;
                            $value->UserId = $pwIdRJ;
                            $value->ProviderId = $row;
                            $tempArrStreetRJPW[$tempUserId] = $pwIdRJ;
                        }
                        $arrProvidersWholerSalerStreet = json_decode(json_encode($arrProvidersWholerSalerStreet), True);
                        Wholesaler::insert($arrProvidersWholerSalerStreet);
                    }

                    if (count($pws1) > 2) {
                        //todo need to double check...right now we are deleting one wholesaler with secondary access
                    }
                }

                if ($tempArrStreetRJPW) {
                    foreach ($tempArrStreetRJPW as $keyFurrk => $valueFurrk) {
                        $streetzips = \DB::table('providers_wholesaler_zips_street')->where('WholesalerId', '=', $keyFurrk)->get();
                        array_walk($streetzips, function (&$key) use ($valueFurrk) {
                            $key->WholesalerId = $valueFurrk;
                            unset($key->Id);
                        });
                        if ($streetzips) {
                            $streetzips = json_decode(json_encode($streetzips), True);
                            $tempArrZip[] = count($streetzips);
                            WholesalerZips::insert($streetzips);    
                        }
                    }
                }
            }

            echo 'step-6';
//          dd('step3-complete');

            /**
             * -----------------------------------------------------------------------------------------------------------
             *                       END
             * -----------------------------------------------------------------------------------------------------------
             *
             */

            /** start adding investment managers data
             * -------------------------------------------------------------------------------------------------
             * -------------------------------------------------------------------------------------------------
             */

            // $streetInvestmentManagers = \DB::table('investment_managers_street')->get();
            // $toAddInvestmentManagers = $toAddInvestmentManagersWholeSalers = [];
            // foreach ($streetInvestmentManagers as $row) {
            //     $userId = \DB::table('users')->where('street_user_id', '=', $row->UserId)->first()->Id;
            //     $row->UserId = $userId;
            //     $row->CreatedBy = $userId;
            //     $row->UpdatedBy = $userId;
            //     $toAddInvestmentManagers[] = (array)$row;
            // }
            // InvestmentManager::insert($toAddInvestmentManagers);
            // $investmentManagersWholeSalersData = \DB::table('investment_manager_wholesalers_street')->get();
            // foreach ($investmentManagersWholeSalersData as $row) {
            //     $row->UserId = User::where('street_user_id', '=', $row->UserId)->first()->Id;
            //     $row->InvestmentManagerId = User::where('street_user_id', '=', $row->InvestmentManagerId)->first()->Id;
            //     $toAddInvestmentManagersWholeSalers[] = (array)$row;
            // }
            // WholesalerIM::insert($toAddInvestmentManagersWholeSalers);

//            dd('step-5');

            /**
             * -----------------------------------------------------------------------------------------------------------
             *                       END
             * -----------------------------------------------------------------------------------------------------------
             *
             */


            /** Insert investment managers wholesalers
             * ------------------------------------------------------------------------------------------------
             * ------------------------------------------------------------------------------------------------
             */

            // $data = \DB::table('users_street')->where('AccountType', '=', 'IM Wholesaler')->get(); //todo need to enable this
            // foreach ($data as $row) {
            //     $id = \DB::table('users')->where('street_user_id', '=', $row->Id)->first()->Id;
            //     \DB::table('investment_manager_wholesaler_zips_street')->where('WholesalerId', '=', $row->Id)->update(['WholesalerId' => $id]); //todo need to enable this
            // }
            // $imWholeSalersOldData = \DB::table('investment_manager_wholesaler_zips_street')->get();
            // foreach ($imWholeSalersOldData as $row) {
            //     unset($row->Id);
            //     IMWholesalerZips::insert((array)$row);
            // }
//            dd('step-6-complete');

            /**
             * -----------------------------------------------------------------------------------------------------------
             *                       END
             * -----------------------------------------------------------------------------------------------------------
             *
             */


            /** start work on  investment line ups
             *--------------------------------------------------------------------------------------------------------
             *--------------------------------------------------------------------------------------------------------
             *--------------------------------------------------------------------------------------------------------
             */
            // add new column street id (fundcategory table). I think need to delete fundcategory and investmentoption table first

            $streetFundCategories = \DB::table('fund_categories_street')
                                    ->whereIn('UserType', ['Provider', 'provider', 'advisor_company'])
                                    ->get();
            $toAddStreetFundCategories = [];
            foreach ($streetFundCategories as $row) {
                if ($row->UserType == "Provider" || $row->UserType == "provider") {
                    if ( \DB::table('providers_street')->where('UserId', '=', $row->UserId)->exists() ) {
                        $firmName = \DB::table('providers_street')->where('UserId', '=', $row->UserId)->first()->FirmName;
                        if (\DB::table('providers')->where('FirmName', '=', $firmName)->exists()) {
                            $row->UserId = \DB::table('providers')->where('FirmName', '=', $firmName)->first()->UserId;
                        }
                        
                    }
                } else {
                    if (\DB::table('advisor_companies')->where('street_company_id', '=', $row->UserId)->exists()) {
                        $row->UserId = \DB::table('advisor_companies')->where('street_company_id', '=', $row->UserId)->first()->Id;
                    }
                }
                
                $row->street_user_id = $row->Id;
                unset($row->Id);
                $toAddStreetFundCategories[] = (array)$row;
            }
            FundCategory::insert($toAddStreetFundCategories);
            $newInsertedFundCategories = FundCategory::where('street_user_id', '>', 0)->whereIn('UserType', ['Provider', 'provider', 'advisor_company'])->get()->toArray();
            $fundCategoryIdsByIndex = array_column($newInsertedFundCategories, 'Id', 'street_user_id');
            $fundCategoryIds = array_column($newInsertedFundCategories, 'street_user_id');
            $streetInvestmentOptions = \DB::table('investment_options_street')->whereIN('FundCategoryId', $fundCategoryIds)->whereNull('IsDeleted')->get();
            $streetInvestmentOptions = json_decode(json_encode($streetInvestmentOptions), true);
            //todo need to check about rjflag
            array_walk($streetInvestmentOptions, function (&$key) use ($fundCategoryIdsByIndex) {
                $key['FundCategoryId'] = $fundCategoryIdsByIndex[$key['FundCategoryId']];
                unset($key['Id']);
            });

            foreach ($streetInvestmentOptions as $row) {
                \App\Models\InvestmentOption::create($row);
            }

            echo 'step-7';
//            dd('step-7c-omplete');


            /**
             * Insert providers fee grid pricing and fee grid assets
             *
             * --------------------------------------STEP-8
             *
             */

            $data = \DB::table('users')->where('street_user_id', '>', 0)->where('AccountType', '=', 'Provider')->get();
            $rjProviders = \DB::table('providers_rj')->get();
            $rjProvidersFirmNames = array_column($rjProviders, 'FirmName');
            $streetProviders = $toAddProviders = [];
            foreach ($data as $row) {
                $streetProviders[$row->Id] = \DB::table('providers_street')->where('UserId', '=', $row->street_user_id)->first();
            }
            // get array difference
            foreach ($streetProviders as $key => $row) {
                if (!in_array($row->FirmName, $rjProvidersFirmNames)) {
                    $row->new_id = $key; // replace new key
                    $toAddProviders[] = (array)$row;
                }
            }

            if ($toAddProviders) {
                foreach ($toAddProviders as $row) {
                    $providerFeeAssets = \DB::table('provider_fee_assets_street')->where('ProviderId', '=', $row['UserId'])->get();
                    if ($providerFeeAssets) {
                        $providerFeeAssets = json_decode(json_encode($providerFeeAssets), true);
                        array_walk($providerFeeAssets, function (&$key) use ($row) {
                            $key['ProviderId'] = $row['new_id'];
                            unset($key['Id']);
                        });
                        FeeFormula::insert($providerFeeAssets);
                    }
                    $providerFeePricing = \DB::table('provider_fee_pricing_rules_street')->where('ProviderId', '=', $row['UserId'])->get();
                    if ($providerFeePricing) {
                        $providerFeePricing = json_decode(json_encode($providerFeePricing), true);
                        array_walk($providerFeePricing, function (&$key) use ($row) {
                            $key['ProviderId'] = $row['new_id'];
                            unset($key['Id']);
                        });
                        FeePricingRule::insert($providerFeePricing);
                    }
                }
            }
            echo 'step-8';
            // dd('testing');exit();

            // END of script
            DB::commit();
            echo 'sucess';
        } catch (Exception $e) {
            DB::rollback();
            echo $e;
            echo 'error';
            echo "The exception was created on line: " . $e->getLine();
        }
    }
}
