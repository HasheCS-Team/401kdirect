<?php

namespace App\Console\Commands;

use \Illuminate\Support\Facades\DB;

use Illuminate\Console\Command;
use App\Models\User;

use Event;
use App\Events\OpportunityReminderEvent;


class OpportunityPageReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opportunity_emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing Opportunity Emails Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = DB::select("CALL sp_advisors_opportunities_cron_data()");

        foreach ($companies as $value) {
            $value->PlanUrl = env('ADVISOR_OPPORTUNITY_REMINDER_PATH');
            $event = Event::fire(new OpportunityReminderEvent($value));
            if ($event) {
                \Log::info('Opportunity Reminder Email sent: Advisor Id:'.$value->AdvisorId.' Comany Id:'.$value->CompanyId);
            } else {
                \Log::info('Opportunity Reminder Email not sent: Advisor Id:'.$value->AdvisorId.' Comany Id:'.$value->CompanyId);
            }
        }
    }
}
