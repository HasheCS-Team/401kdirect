<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\InvestmentManager\InvestmentManagerWholesalerController as imwc;
use App\Models\User;



class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing Emails Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sendReminderEmail = app(\App\Helpers\Classes\HelperFunctions::class);

        $advisors = User::select(['Id','InvitedBy'])
                    ->leftJoin('user_unsubscribe' ,'users.Email', '=' ,'user_unsubscribe.Email' )
                    ->whereNotNull('users.InvitationToken')
                    ->where(['users.IsVerified'=>0])
                    ->where('user_unsubscribe.Email',NULL)
                    ->get();

        foreach ($advisors as $key => $value) {
            if ($value->InvitedBy != null) {
                $check = $sendReminderEmail->sendReminderEmail($value->Id,$value->InvitedBy);
                if ($check) {
                    \Log::info('Email send Reminder Advisor Id:'.$value->Id.' IMW id:'.$value->InvitedBy);
                } else {
                    \Log::info('Email not send Reminder Advisor Id:'.$value->Id.' IMW id:'.$value->InvitedBy);
                }
            } else {
                \Log::info('Email not send Reminder because IMW id is not present');
            }
        }
    }
}
