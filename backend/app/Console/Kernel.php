<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\SendEmails::class,
        Commands\OpportunityPageReminder::class,
        Commands\AdvisorsInvitation::class,
        Commands\MergeStreetDataInRJ::class,
        Commands\CopyProductData::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('emails:send')
        ->saturdays()
        ->timezone('America/New_York')
        ->at('02:00');

        $schedule->command('opportunity_emails:send')
        ->daily()
        ->timezone('America/New_York')
        ->at('02:00');
    }
}
