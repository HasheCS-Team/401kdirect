<?php

namespace App\Http\Controllers\Direct;

use App\Http\Controllers\Controller;
use App\Http\Requests\BenchmarkPostRequest;
use Illuminate\Http\Request;
use Validator;
use DB;

use App\Models\Collection\DolCompany;
use App\Models\FundCategory;
use App\Models\ProviderFeeSchedules;
use App\Models\User;
use App\Models\Collection\ProviderCollection;
use App\Models\Collection\ThirdPartyAdministrator;


class BenchmarkController extends Controller
{

    public function index(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'company_data.FullPlanAssets' => 'required|numeric|min:1|max:20000000|in_between_required_length:1,10',
            'company_data.ParticipentAccountBalance' => 'required|integer|in_between_required_length:1,10|min:1',
            'company_data.TotalIncomeAmount' => 'required|integer|in_between_required_length:1,10',
            'company_data.PlanName' => 'required|min:6|max:250',
            'company_data.PhoneNumber' => 'required',
            'company_data.Address1' => 'required|min:6|max:250',
            'company_data.City' => 'required|exists:usa_zips,City',
            'company_data.State' => 'required|exists:usa_zips,State',
            'company_data.ZipCode' => 'required|in_usa_zip_exits',

            'annual_compensation.year' => 'required|integer', 
            'annual_compensation.percent' => 'required|numeric|between:0.00,999'
        ]);
        if ($validator->passes()) {
            $company = $this->makeCompany($data);
            $company = $this->applyCompensation($data, $company);
            $benchmark = $this->benchmark($company);
            return response()->json($benchmark, 200);           
        }
        return response()->json(['errors' => $validator->errors()->all()], 422);   
    }

    public function dolCompanies($search)
    {
        return response()->json(DolCompany::select(DB::Raw('sf_sponsor_name as name, Id as id'))
            ->where('sf_sponsor_name', 'like', $search . '%')
            ->where('sf_partcp_account_bal_cnt', '>', 0)
            ->where('sf_net_assets_eoy_amt', '>', 0)
            ->where('sf_tot_income_amt', '>', 0)
            ->get());
    }

    public function dolCompany($id)
    {
        return response()->json(DolCompany::where('Id', $id)->first());
    }

    public function myPlanProviderAndTpa()
    {
        return response()->json([
            'providers' => ProviderCollection::select(DB::Raw('Id as id, FirmName as name'))->take(10)->get(),
            'tpa' => ThirdPartyAdministrator::select(DB::Raw('Id as id, tpa_master_name as name'))->take(10)->get()
        ]);
    }

    private function makeCompany($data)
    {
        $company = new DolCompany;
        $company->sf_plan_name = $data['company_data']['PlanName'];
        $company->sf_sponsor_name = $data['company_data']['SponsorName'];
        $company->sf_spons_us_address1 = $data['company_data']['Address1'];
        $company->sf_spons_us_city = $data['company_data']['City'];
        $company->sf_spons_us_state = $data['company_data']['State'];
        $company->sf_spons_us_zip = $data['company_data']['ZipCode'];
        $company->sf_spons_phone_num = $data['company_data']['PhoneNumber'];
        $company->sf_partcp_account_bal_cnt = $data['company_data']['ParticipentAccountBalance'];
        $company->sf_net_assets_eoy_amt = $data['company_data']['FullPlanAssets'];
        $company->sf_tot_income_amt = $data['company_data']['TotalIncomeAmount'];
        $company->sf_admin_signed_name = $data['company_data']['AdminSignedName'];
        $company->years = $data['annual_compensation']['year'];
        $company->percentage = $data['annual_compensation']['percent'];
        $company->tpaFlag = $data['company_data']['TpaFlag'];
        return $company;
    }

    private function applyCompensation($data, $company)
    {
        if($company->sf_net_assets_eoy_amt == 0)
        {
            $company->compensation = (( $data['annual_compensation']['percent'] *  $company->sf_tot_income_amt ) / 100) ;
        }
        else{
            $company->compensation = (( $data['annual_compensation']['percent'] *  $company->sf_net_assets_eoy_amt ) / 100) ;
        }
        return $company;
    }

    public function myPlan(Request $request)
    {
        $totalCostAmount = $request->TotalCost;
        $netAssetsEoyAmount = $request->NetAssetsEoyAmount;
        $hardDollarAmount = $request->HardDollarAmount;
        $participentAccountBalance = $request->ParticipentAccountBalance;

        // Total Cost
        $totalCost = round((($totalCostAmount*$netAssetsEoyAmount)/100) + $hardDollarAmount, 2);
        $totalCostPercentage = round((($totalCost/$netAssetsEoyAmount)*100), 2);
        
        // Other Cost
        $otherTotalCost = round($hardDollarAmount, 2);
        $otherTotalCostPercentage = round((($hardDollarAmount/$netAssetsEoyAmount)*100), 2);

        // Investment Cost
        $totalWeightedExpense = round($totalCost - $hardDollarAmount, 2);
        $totalWeightedExpensePercentage =  round($totalCostPercentage - $otherTotalCostPercentage, 2);

        // Per Participant
        $perParticipants = round($totalCost / $participentAccountBalance, 2);

        return response()->json([
            'totalCostAmount' => $totalCost,
            'totalCostPercentage' => $totalCostPercentage,

            'perParticipant' => $perParticipants,

            'otherCostAmount' => $otherTotalCost,
            'otherCostPercentage' => $otherTotalCostPercentage,

            'InvestmentCostAmount' => $totalWeightedExpense,
            'InvestmentCostPercentage' => $totalWeightedExpensePercentage
        ]);
    }

    private function benchmark($company)
    {
        // Variables
        $FiduciaryServiceCharges = 0.0 ;
        $ProviderFeeCostAmount = 0.0 ;
        $InvestmentCost = 0.0 ;
        $ProviderFeeCost = 0.0 ;
        $ServiceCost = 0.0 ;
        $TotalCost = 0.0 ;
        $InvestmentCostAmount = 0.0 ;
        $TotalCostPerPart = 0.0 ;
        $CompanyFundsExist = 0; // taken as hardcoded 0

        // Assignments
        $totalIncomeAmount = $company->sf_tot_income_amt;
        $NetAssetsAmount = $company->sf_net_assets_eoy_amt == 0 ? $company->sf_tot_income_amt : $company->sf_net_assets_eoy_amt;
        $ParticipentCount = $company->sf_partcp_account_bal_cnt;
        $Compensation = $company->compensation;
        $Years = $company->years;
        $Percentage = (float) $company->percentage;
        $TotalPlanAssets = $company->sf_net_assets_eoy_amt;
        $FullPlanAssets = $company->sf_net_assets_eoy_amt;
        $AverageAccountBalancce = $ParticipentCount ? $NetAssetsAmount + $totalIncomeAmount : ($NetAssetsAmount + $totalIncomeAmount) / $ParticipentCount;
        $BrokerCompensationAmount = ($NetAssetsAmount * ($Percentage) / 100);
        $OldCurrentProvider = 0;
        $ProcessId = date('Y-m-d H:i:s');
        if($company->tpaFlag == 'TPA')
        {
            // To be calculated, dependency required (advisorId, advisorCompanyId)
            $EstimatedTPAFee = 0;
            $EstimatedTPAPercentage = 0;
            $EstimatedThreeSixteenFee = 0;
            $EstimatedThreeSixteenPercentage = 0;
            $TpaFeeGrid = 0;
            $ThreeSixteenFeeGrid = 0;
        }else{
            $EstimatedTPAFee = 0;
            $EstimatedTPAPercentage = 0;
            $EstimatedThreeSixteenFee = 0;
            $EstimatedThreeSixteenPercentage = 0;
            $TpaFeeGrid = 0;
            $ThreeSixteenFeeGrid = 0;
        }

        $users = User::select(DB::Raw('providers.UserId as ProviderId,
                                    providers.FirmName as ProviderName,
                                    providers.IncludeAnnualContribution as IncludeAnnualContribution
                                    '))
            ->join('providers', 'providers.UserId', '=', 'users.Id')
            ->get();
        $benchMarkResults = [];
        $benchMarkResultCounter = 0;
        foreach($users as $user)
        {
            //dd($user->ProviderId);
            $PerformanceData = $this->CalculateInvestmentCostPerformanceBenchMark($user->ProviderId);
            //dd($PerformanceData);
            $feeGridNetAssets = 1;
            $feeGridAverageAccountBalance = 1;
            if($user->IncludeAnnualContribution == 1)
            {
                $feeGridNetAssets = $TotalPlanAssets + $totalIncomeAmount;
            }elseif($TotalPlanAssets == 0){
                $feeGridNetAssets = 1;
            }else{
                $feeGridNetAssets = $TotalPlanAssets;
            }
            if($user->IncludeAnnualContribution == 1)
            {
                $feeGridAverageAccountBalance = $AverageAccountBalancce;
            }elseif($TotalPlanAssets == 0){
                $feeGridAverageAccountBalance = 1/$ParticipentCount;
            }else{
                $feeGridAverageAccountBalance = $TotalPlanAssets/$ParticipentCount;
            }
            $GridPricing = $this->FeeGridPricing($feeGridNetAssets, $feeGridAverageAccountBalance, $ParticipentCount, $user->ProviderId, 0, $TotalPlanAssets, $this->getFeeScheduleId($user->ProviderId, $company->tpaFlag));
            //dd($GridPricing);
            $ServicePlanFee = ($GridPricing == 'NO MATCH' || $GridPricing == 'CUSTOMED' ? 0 : $GridPricing['PlanFees']);
            $ServiceParticipantFee = ($GridPricing == 'NO MATCH' || $GridPricing == 'CUSTOMED' ? 0 : $GridPricing['ParticipantFee']);
            $ServiceAssetBasedFee = ($GridPricing == 'NO MATCH' || $GridPricing == 'CUSTOMED' ? 0 : $GridPricing['AssetBaseFees']);
            $ProviderFeeCost = ($GridPricing == 'NO MATCH' || $GridPricing == 'CUSTOMED' ? 0 : $GridPricing['AssetBaseFees'] + (($ServicePlanFee + ($ServiceParticipantFee * $ParticipentCount))/$NetAssetsAmount * 100));
            $ProviderFeeCostAmount = ($ProviderFeeCost * $NetAssetsAmount) / 100;
            $ServiceCost = ($ProviderFeeCostAmount + $BrokerCompensationAmount + $FiduciaryServiceCharges);
            $InvestmentCost = (float) $PerformanceData['Expense'];
            //dd($PerformanceData['Expense']);
            $InvestmentCostAmount = (($InvestmentCost / 100) * $NetAssetsAmount);
            $TargetDateInvestmentCost = 0.0; // function required
            $TotalCost = ($InvestmentCostAmount? $InvestmentCostAmount : 0) + ($ServiceCost ? $ServiceCost : 0);
            $TotalCostPerPart = ($TotalCost / $ParticipentCount);
            $IsPriceHidden = $this->isCustomPriceHidden($TotalPlanAssets, 0, $user->ProviderId);
            $FeeScheduleId = $this->getFeeScheduleId($user->ProviderId, $company->tpaFlag);
            $FeeScheduleType = $this->getFeeScheduleType($FeeScheduleId, $company->tpaFlag);
            $CostingHistory = $PerformanceData['OneYear'] . ',' . $PerformanceData['ThreeYear'] . ',' . $PerformanceData['FiveYear'] . ',' . $PerformanceData['TenYear'] . ',' . $FiduciaryServiceCharges . ',' . $InvestmentCost . ',' . $InvestmentCostAmount . ',' . $TargetDateInvestmentCost . ',' . $TotalCost . ',' . $TotalCostPerPart;
            //dd($FeeScheduleType);
            
            // Virtually creating benchmarkResults table
            if($GridPricing != 'NO MATCH')
            {
                $benchMarkResults[$benchMarkResultCounter]['Id'] = NULL;
                $benchMarkResults[$benchMarkResultCounter]['ProcessId'] = $ProcessId;
                $benchMarkResults[$benchMarkResultCounter]['AdvisorId'] = 0;
                $benchMarkResults[$benchMarkResultCounter]['ProviderId'] = $user->ProviderId;
                $benchMarkResults[$benchMarkResultCounter]['CompanyId'] = 0;
                $benchMarkResults[$benchMarkResultCounter]['ProviderName'] = $user->ProviderName;
                $benchMarkResults[$benchMarkResultCounter]['TotalIncomeAmount'] = $totalIncomeAmount;
                $benchMarkResults[$benchMarkResultCounter]['NetAssetsAmount'] = $NetAssetsAmount;
                $benchMarkResults[$benchMarkResultCounter]['ParticipentCount'] = $ParticipentCount;
                $benchMarkResults[$benchMarkResultCounter]['Compensation'] = $Compensation;
                $benchMarkResults[$benchMarkResultCounter]['Years'] = $Years;
                $benchMarkResults[$benchMarkResultCounter]['Percentage'] = $Percentage;
                $benchMarkResults[$benchMarkResultCounter]['OneYear'] = $PerformanceData['OneYear'];
                $benchMarkResults[$benchMarkResultCounter]['ThreeYear'] = $PerformanceData['ThreeYear'];
                $benchMarkResults[$benchMarkResultCounter]['FiveYear'] = $PerformanceData['FiveYear'];
                $benchMarkResults[$benchMarkResultCounter]['TenYear'] = $PerformanceData['TenYear'];
                $benchMarkResults[$benchMarkResultCounter]['GridPricing'] = is_array($GridPricing) ? $GridPricing['orignal'] : $GridPricing;
                $benchMarkResults[$benchMarkResultCounter]['ServicePlanFee'] = $ServicePlanFee;
                $benchMarkResults[$benchMarkResultCounter]['ServiceParticipantFee'] = $ServiceParticipantFee;
                $benchMarkResults[$benchMarkResultCounter]['ServiceAssetBasedFee'] = $ServiceAssetBasedFee;
                $benchMarkResults[$benchMarkResultCounter]['PlanAssets'] = $TotalPlanAssets;
                $benchMarkResults[$benchMarkResultCounter]['FullPlanAssets'] = $FullPlanAssets;
                $benchMarkResults[$benchMarkResultCounter]['TotalPlanAssets'] = $TotalPlanAssets;
                $benchMarkResults[$benchMarkResultCounter]['AverageAccountBalancce'] = $AverageAccountBalancce;
                $benchMarkResults[$benchMarkResultCounter]['BrokerCompensationAmount'] = $BrokerCompensationAmount;
                $benchMarkResults[$benchMarkResultCounter]['ProviderFeeCost'] = $ProviderFeeCost;
                $benchMarkResults[$benchMarkResultCounter]['ProviderFeeCostAmount'] = $ProviderFeeCostAmount;
                $benchMarkResults[$benchMarkResultCounter]['FiduciaryServiceCharges'] = $FiduciaryServiceCharges;
                $benchMarkResults[$benchMarkResultCounter]['TotalServiceCost'] = $ServiceCost;
                $benchMarkResults[$benchMarkResultCounter]['InvestmentCost'] = $InvestmentCost;
                $benchMarkResults[$benchMarkResultCounter]['InvestmentCostAmount'] = $InvestmentCostAmount;
                $benchMarkResults[$benchMarkResultCounter]['TargetDateInvestmentCost'] = $TargetDateInvestmentCost;
                $benchMarkResults[$benchMarkResultCounter]['TotalCost'] = $TotalCost;
                $benchMarkResults[$benchMarkResultCounter]['TotalCostPerPart'] = $TotalCostPerPart;
                $benchMarkResults[$benchMarkResultCounter]['IsPriceHidden'] = $IsPriceHidden;
                $benchMarkResults[$benchMarkResultCounter]['FeeScheduleId'] = $FeeScheduleId;
                $benchMarkResults[$benchMarkResultCounter]['FeeScheduleType'] = $FeeScheduleType;
                $benchMarkResults[$benchMarkResultCounter]['ProductId'] = 0; 
                //$benchMarkResults[$benchMarkResultCounter]['CostingHistory'] = $CostingHistory; // removed by farrukh
                $benchMarkResults[$benchMarkResultCounter]['EstimatedTPAFee'] = $EstimatedTPAFee;
                $benchMarkResults[$benchMarkResultCounter]['EstimatedTPAPercentage'] = $EstimatedTPAPercentage;
                $benchMarkResults[$benchMarkResultCounter]['EstimatedThreeSixteenFee'] = $EstimatedThreeSixteenFee;
                $benchMarkResults[$benchMarkResultCounter]['EstimatedThreeSixteenPercentage'] = $EstimatedThreeSixteenPercentage;
                $benchMarkResults[$benchMarkResultCounter]['TpaFeeGrid'] = $TpaFeeGrid;
                $benchMarkResults[$benchMarkResultCounter]['ThreeSixteenFeeGrid'] = $ThreeSixteenFeeGrid;

                $benchMarkResultCounter++;
            }            
        }

        //dd($benchMarkResults[0]);

        $benchmark = [];
        $benchmarkResultCollection = collect($benchMarkResults);

        //dd($benchmarkResultCollection);

        $max = $benchmarkResultCollection->sortByDesc('TotalCost')->first(function($key, $row){
            return $row['TotalCost'] && $row['GridPricing'] != 'CUSTOMED' && $row['GridPricing'] != 'NO MATCH' && $row['InvestmentCostAmount']  > 0;
        });
        $totalPercentage = $max['ProviderFeeCost'] + $max['InvestmentCost']+ $max['Percentage'];    

        $benchmark['max'] = [
            'ProviderFeeCost' => $this->format($max['ProviderFeeCost'], 'percentage'),
            'ProviderFeeCostAmount' => $this->format($max['ProviderFeeCostAmount'], 'currency'),

            'InvestmentCost' => $this->format($max['InvestmentCost'], 'percentage'),
            'InvestmentCostAmount' => $this->format($max['InvestmentCostAmount'], 'currency'),

            'Percentage' => $this->format($max['Percentage'], 'percentage'),
            'BrokerCompensationAmount' => $this->format($max['BrokerCompensationAmount'], 'currency'),
            'TotalCostPercentage' => $this->format($totalPercentage, 'percentage'), 
            'TotalCost' => $this->format($max['TotalCost'], 'currency'),
            'TotalCostParticipant' => $this->format($max['TotalCost']/$max['ParticipentCount'], 'currency')
        ];

        $min = $benchmarkResultCollection->filter(function($row, $key){
            return $row['TotalCost'] && $row['GridPricing'] != 'CUSTOMED' && $row['GridPricing'] != 'NO MATCH' && $row['InvestmentCostAmount']  > 0;
        });
        $min = $min->sortBy('TotalCost')->first();
        //return ($min->toArray()); 
        $totalPercentage = $min['ProviderFeeCost'] + $min['InvestmentCost']+ $min['Percentage'];       
        $benchmark['min'] = [
            'ProviderFeeCost' => $this->format($min['ProviderFeeCost'], 'percentage'),
            'ProviderFeeCostAmount' => $this->format($min['ProviderFeeCostAmount'], 'currency'),

            'InvestmentCost' => $this->format($min['InvestmentCost'], 'percentage'),
            'InvestmentCostAmount' => $this->format($min['InvestmentCostAmount'], 'currency'),

            'Percentage' => $this->format($min['Percentage'], 'percentage'),
            'BrokerCompensationAmount' => $this->format($min['BrokerCompensationAmount'], 'currency'),
            'TotalCostPercentage' => $this->format($totalPercentage, 'percentage'),    
            'TotalCost' => $this->format($min['TotalCost'], 'currency'),
            
            'TotalCostParticipant' => $this->format($min['TotalCost']/$min['ParticipentCount'], 'currency')
        ];

        $dataSetForAverage = $benchmarkResultCollection->filter(function($row, $key){
            return $row['TotalCost'] && $row['GridPricing'] != 'CUSTOMED' && $row['GridPricing'] != 'NO MATCH' && $row['InvestmentCostAmount']  > 0;
        });
        $totalPercentage = $dataSetForAverage->avg('ProviderFeeCost') + $dataSetForAverage->avg('InvestmentCost') + $dataSetForAverage->avg('Percentage');
        $benchmark['avg'] = [
            'ProviderFeeCost' => $this->format($dataSetForAverage->avg('ProviderFeeCost'), 'percentage'),
            'ProviderFeeCostAmount' => $this->format($dataSetForAverage->avg('ProviderFeeCostAmount'), 'currency'),

            'InvestmentCost' => $this->format($dataSetForAverage->avg('InvestmentCost'), 'percentage'),
            'InvestmentCostAmount' => $this->format($dataSetForAverage->avg('InvestmentCostAmount'), 'currency'),

            'Percentage' => $this->format($dataSetForAverage->avg('Percentage'), 'percentage'),
            'BrokerCompensationAmount' => $this->format($dataSetForAverage->avg('BrokerCompensationAmount'), 'currency'),

            'TotalCostPercentage' => $this->format($totalPercentage, 'percentage'),
            'TotalCost' => $this->format($dataSetForAverage->avg('TotalCost'), 'currency'),
            'TotalCostParticipant' => $this->format($dataSetForAverage->avg('TotalCost')/$dataSetForAverage->avg('ParticipentCount'), 'currency')
        ];

        return $benchmark;
    }

    private function format($value, $type)
    {
        return $value;
        if($type == 'percentage')
        {
            return number_format((float)$value , 2, '.', '') . '%';
        }elseif($type == 'currency'){
            return '$' . number_format(round($value));
        }
    }

    private function isCustomPriceHidden($totalPlanAssets, $oldCurrentProvider, $providerId)
    {
        $result = DB::select("SELECT IS_CUSTOMED_PRICED($totalPlanAssets, $oldCurrentProvider, $providerId) as isCustomPriced")[0];
        if($result && $result->isCustomPriced == 1)
        {
            return true;
        }
        return false;
    }

    public function CalculateInvestmentCostPerformanceBenchMark($providerId)
    {
        $result = DB::select("SELECT CALCULATE_INVESTMENT_COST_PERFORMANCE_BENCHMARK(0, $providerId, 'without_data') as cost")[0];
        $arr = explode(",", $result->cost);
        $mulArray = [];
        $mulArray['OneYear'] = $arr[0];
        $mulArray['ThreeYear'] = $arr[1];
        $mulArray['FiveYear'] = $arr[2];
        $mulArray['TenYear'] = $arr[3];
        $mulArray['Expense'] = $arr[4];
        return $mulArray;
    }

    /* @dependant benchmark
    * @params providerId (Int)
    * @return providerFeeScheduleId (Int)
    */
    public function FeeGridPricing($netAssets, $averageAccountBalance, $participentCount, $providerId, $currentProviderId, $totalPlanAssets, $feeScheduleId)
    {
        $result = DB::select("SELECT FEE_GRID_PRICING($netAssets, $averageAccountBalance, $participentCount, $providerId, $currentProviderId, $totalPlanAssets, $feeScheduleId) as fee_grid_pricing")[0];
        if($result->fee_grid_pricing == 'NO MATCH' || $result->fee_grid_pricing == 'CUSTOMED')
        {
            return $result->fee_grid_pricing;
        }
        $gridArray = explode(',', $result->fee_grid_pricing);
        $gridResult = [];
        foreach($gridArray as $row)
        {
            $item = explode(':', $row);
            $gridResult[$item[0]] = $item[1];
        }
        $gridResult['orignal'] = $result->fee_grid_pricing;
        return $gridResult;
    }

    /* @dependant benchmark
     * @params providerId (Int)
     * @return providerFeeScheduleId (Int)
     */
    private function getFeeScheduleId($providerId, $tpaFlag)
    {
        $providerFeeSchedule = ProviderFeeSchedules::select(DB::Raw('provider_fee_schedules.Id as id, provider_fee_schedules.ServiceModel, IF(provider_fee_schedules.ServiceModel = "' . $tpaFlag . '", 1, 0) AS MatchStatus'))
            ->where('ProviderId', $providerId)
            ->orderBy('MatchStatus', 'DESC')
            ->first();
        if($providerFeeSchedule)
        {
            return $providerFeeSchedule->id;
        }
        return 1;
    }

    private function getFeeScheduleType($providerFeeScheduleId)
    {
        $providerFeeSchedule = ProviderFeeSchedules::select('ServiceModel')
            ->where('Id', $providerFeeScheduleId)
            ->where('TYPE', 'Provider')
            ->first();
        if($providerFeeSchedule)
        {
            return $providerFeeSchedule->ServiceModel;
        }
        return 'TPA'; // Default
    }

}