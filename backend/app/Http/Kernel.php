<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            //\Illuminate\Session\Middleware\StartSession::class,
            //\Illuminate\View\Middleware\ShareErrorsFromSession::class,
            // \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'acl' => \App\Http\Middleware\CheckPermission::class,
        'test' => \App\Http\Middleware\Test::class,
        'jwt.auth' => \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
        'jwt.refresh' => \TymonJWTAuth\Middleware\RefreshToken::class,
        'jwt-auth' => \App\Http\Middleware\AuthJWT::class,
        'page-exists' => \App\Http\Middleware\CheckPage::class,
        'im-wholesaler' => \App\Http\Middleware\InvestmentManagerWholesaler::class,
        'LoginAllowedCors' => \App\Http\Middleware\AdminCors::class,
        'LoginAllowedCorsFrontend' => \App\Http\Middleware\FrontendCors::class,
        'AllowCors'=>\Barryvdh\Cors\HandleCors::class,
        'advisor-opp' => \App\Http\Middleware\CheckAdvisorOpportunity::class,
        'admin-page-permission' => \App\Http\Middleware\CheckAdminPermission::class,
        'advisor-new-opp' => \App\Http\Middleware\CheckUpdatedOpportunity::class,
        'campaign' => \App\Http\Middleware\ConfirmationPage::class,
        'saml2' => \App\Http\Middleware\saml2::class,
        'check-pw-permission' => \App\Http\Middleware\CheckWholesalerPermission::class,
        'check-imw-permission' => \App\Http\Middleware\CheckIMWholesalerPermission::class,
        'current-primary-imw' => \App\Http\Middleware\CurrentPrimaryIMW::class,
        'route-mapping' => \App\Http\Middleware\RouteMapping::class,
        'IfProposalHold' => \App\Http\Middleware\IfProposalHold::class,
    ];
}