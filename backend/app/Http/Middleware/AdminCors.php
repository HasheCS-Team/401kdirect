<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class AdminCors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedLoginPath = [
            'Admin'=> env('APPLICATION_URL').'/401k-admin/auth/login',
            'Provider'=> env('APPLICATION_URL').'/401k-admin/auth/login',
            'Investment Manager'=> env('APPLICATION_URL').'/401k-admin/auth/login',
            'Sub Provider Admin'=> env('APPLICATION_URL').'/401k-admin/auth/login',
            'broker-dealer'=> env('APPLICATION_URL').'/401k-admin/auth/login'
        ];

        $user = User::select('AccountType')->where('Email', $request->get('Email'))->first();
        if ( $user ) {
            if ( array_key_exists($user->AccountType, $allowedLoginPath) ) {
                if ( $allowedLoginPath[$user->AccountType] == $request->headers->get('referer') ) {
                    return $next($request);
                } else {
                    return response()->json([
                        'status'=>'fail',
                        'type'=>'unauthorized',
                        'errorMessage' => 'Unauthorized access.'
                    ], 403);
                }
            } else {
                return response()->json([
                    'status'=>'fail',
                    'type'=>'unauthorized',
                    'errorMessage' => 'Unauthorized access.'
                ], 403);
            }
        } else {
            return response()->json([
                'status'=>'fail',
                'errorMessage' => 'Either email is wrong or password.'
            ]);
        }
    }
}
