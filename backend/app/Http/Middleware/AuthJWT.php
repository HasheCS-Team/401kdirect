<?php 
namespace App\Http\Middleware;

use Closure;

use Exception;
use \Tymon\JWTAuth\Exceptions\TokenInvalidException;
use \Tymon\JWTAuth\Exceptions\TokenExpiredException;

use JWTAuth;
use Response;

class AuthJWT
{
    public function handle($request, Closure $next)
    {
        if ( !$token = JWTAuth::setRequest($request)->getToken() ) {
            return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans('api.tokenNotProvided')], 400);
        }
        
        try
        {
            $user = JWTAuth::authenticate($token);
        }
        catch (Exception $e)
        {
            if ( $e instanceof TokenInvalidException )
            {
                return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans('api.tokenExpired')], 401);
            }
            else if ( $e instanceof TokenExpiredException )
            {
                return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans('api.tokenExpired')], 400);
            }
            else
            {
                //fault server error so, forbid the user here...
                return Response::json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans('api.someProblem')], 500);
            }
        }

        if ( !$user )
        {
            return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans('api.UserNotFound')], 404);
        }
        return $next($request);
    }
}

?>