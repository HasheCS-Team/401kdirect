<?php

namespace App\Http\Middleware;

use App\Models\User;

use Closure;
use Auth;
use DB;

class CheckAdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param_id = (Integer) $request->route('id');

        $login_user = Auth()->user();
        
        if($login_user && $login_user->provider)
        {

            if( $login_user->AccountType == 'Provider' && $param_id > 0 )
            {
                return response()->json(['status' => 'fail', 'type' => 'pro', 'errorMessage' => trans('api.accessDenied')], 403);     
            }
        }
        elseif ($login_user && $login_user->investmentManager)
        {

            if( $login_user->AccountType == 'Investment Manager' && $param_id > 0 )
            {
                return response()->json(['status' => 'fail', 'type' => 'pro', 'errorMessage' => trans('api.accessDenied')], 403);     
            }
        }

        return $next($request);

    }
}
