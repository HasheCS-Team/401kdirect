<?php

namespace App\Http\Middleware;

use App\Models\AdvisorCompany;
use App\Models\User;

use Closure;
use Auth;
use DB;

class CheckAdvisorOpportunity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $advisor_id = Auth()->user()->Id;
        $user = User::select('IsNewSession')->where('Id', $advisor_id)->first();
        if(empty(Auth()->user()->BrokerDealer) && empty(Auth()->user()->ZipCode)){
            return response()->json(['status' => 'fail', 'type' => 'bro', 'errorMessage' => trans('api.ZipAndBrokerSelection')], 403);
        }else if(empty(Auth()->user()->ZipCode)){
            return response()->json(['status' => 'fail', 'type' => 'bro', 'errorMessage' => trans('api.ZipEmptySelection')], 403);
        }else if(empty(Auth()->user()->BrokerDealer)){
            return response()->json(['status' => 'fail', 'type' => 'bro', 'errorMessage' => trans('api.BrokerDealerSelection')], 403);
        }

        if ($user && $user->IsNewSession == 1) {
            // $opportunity_count = DB::select("CALL sp_advisor_opportunities_count($advisor_id)");
            $opportunity_count = DB::select("CALL sp_advisor_opportunities_count_new($advisor_id)");

            if ( $opportunity_count[0]->OpportunityCount > 0 )
            {
                return response()->json(['status' => 'fail', 'type' => 'opp', 'errorMessage' => trans('api.advisorOpportunityCheck')], 403);
            }
        }

        return $next($request);

    }
}
