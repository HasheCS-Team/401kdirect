<?php

namespace App\Http\Middleware;

use App\Models\AdvisorCompany;
use App\Models\User;

use Closure;
use Auth;
use DB;

class CheckIMWholesalerPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $wholesaler_id = Auth()->user()->Id;

        $where = [];
        if ($request->route()->parameter('campaign_id')) {
            $where = ['imo.CampaignId'=>$request->route()->parameter('campaign_id'), 'imo.WholesalerId'=>$wholesaler_id];
        } else if($request->get('CampaignId')) {
            $where = ['imo.CampaignId'=>$request->get('CampaignId'), 'imo.WholesalerId'=>$wholesaler_id];
        }

        $result = DB::table('investment_manager_opportunities as imo')
                    ->select('imo.AccessType')
                    ->where($where)
                    ->first();

        if ($result) {
            if ($result->AccessType == 'R') {
                return response()->json(['status' => 'fail', 'type' => 'imw-perm', 'errorMessage' => 'You dont have permission to make this change.'], 403);
            }
        } else {
            return response()->json([
                'status'=>'fail',
                'errorMessage' => 'Something went wrong.'
            ]);
        }

        return $next($request);
    }
}
