<?php

namespace App\Http\Middleware;

use Closure;
use \App\Models\User;
use \App\Models\Permission;
use Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        //adding custom key from one middleware
        //$request->merge(['key' => 'value']);
        //and getting it from other middleware, passed from first middleware...
        //$request->input('test');

        //this permission paramter can be set on route without the involvement of middleware...
        $extra_param = @$request->route()->getAction()['permission'];

        if ( $this->isAllowed($permission) )
        {
            return $next($request);
        }
        else
        {
            return response()->json(['status' => 'fail', 'type' => 'pro', 'errorMessage' => trans('api.actionPrivilege')], 403);
        }
    }

    private function isAllowed($permission)
    {
        $flag = false;
        $user = Auth::user();

        if(isset($_COOKIE['is_login']) )
        {
            $str = $_COOKIE['is_login'];
            $isLogin = explode(":",$str);

            if($isLogin[1])
            {
                $user = User::find($isLogin[1]);
            }
        }


        if($user['ParentId']) {

            $flag = true;
            $permissions[] = array_column($user->permissions->toArray(), Permission::PERMISSION_NAME);

            if ( $flag )
            {
                return in_array($permission, array_flatten($permissions));
            }
        } else {
            foreach ( $user->roles as $role )
            {
                $flag = true;
                $permissions[] = array_column($role->permissions->toArray(), Permission::PERMISSION_NAME);
            }

            if ( $flag )
            {
                return in_array($permission, array_flatten($permissions));
            }
        }

        return true;
    }
}