<?php

namespace App\Http\Middleware;

use App\Models\AdvisorCompany;
use App\Models\User;

use Closure;
use Auth;
use DB;

class CheckUpdatedOpportunity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $advisor_id = Auth()->user()->Id;
        // $opportunity_count = DB::select("CALL sp_advisor_opportunities_count($advisor_id)");

        // if ( $opportunity_count[0]->OpportunityCount == 0 )
        // {
        //     return response()->json(['status' => 'fail', 'type' => 'noNewOpp', 'errorMessage' => trans('api.advisorNewOppCheck')], 403);
        // }

        return $next($request);

    }
}
