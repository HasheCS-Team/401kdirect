<?php

namespace App\Http\Middleware;

use App\Models\AdvisorCompany;
use App\Models\User;

use Closure;
use Auth;
use DB;

class CheckWholesalerPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $wholesaler_id = Auth()->user()->Id;

        $where = [];
        if ($request->route()->parameter('campaign_id')) {
            $where = ['ps.CampaignId'=>$request->route()->parameter('campaign_id'), 'ps.WholesalerId'=>$wholesaler_id];
        } else if($request->route()->parameter('company_id')) {
            $where = ['ps.CompanyId'=>$request->route()->parameter('company_id'), 'ps.WholesalerId'=>$wholesaler_id];
        } else if($request->get('CompanyId')) {
            $where = ['ps.CompanyId'=>$request->get('CompanyId'), 'ps.WholesalerId'=>$wholesaler_id];
        }

        $result = DB::table('provider_stats as ps')
                    ->select('ps.AccessType')
                    ->where($where)
                    ->first();
// return response()->json(['data'=>$where,'status' => 'fail', 'type' => 'pw-perm', 'errorMessage' => 'You dont have permission to make this change.'], 403);
        if ($result) {
            if ($result->AccessType == 'R') {
                return response()->json(['status' => 'fail', 'type' => 'pw-perm', 'errorMessage' => 'You dont have permission to make this change.'], 403);
            }
        } else {
            return response()->json([
                'status'=>'fail',
                'errorMessage' => 'Something went wrong.'
            ]);
        }

        return $next($request);
    }
}
