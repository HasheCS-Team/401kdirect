<?php

namespace App\Http\Middleware;

use App\Models\AdvisorCompany;
use App\Models\User;
use App\Models\Proposal\Campaigns;
use App\Models\Proposal\ProviderStats;

use Closure;
use Auth;
use DB;

class ConfirmationPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $advisor_id = Auth()->user()->Id;

        $company_id = (int) $request->route('company_id');
        $provider_id = (int) $request->route('provider_id');

        if ($advisor_id) 
        {
            $stats = ProviderStats::where(['AdvisorId' => $advisor_id, 'CompanyId' => $company_id, 'Status' => 'Current']);

            $campaign = $stats->count();
            $providerCampaign = $stats->where('ProviderId', $provider_id)->count();

            if ( $campaign == 0 )
            {
                return response()->json(['status' => 'fail', 'type' => 'campaign', 'errorMessage' => trans('api.campaignNotFound')], 403);
            }
            elseif ( $providerCampaign == 0) 
            {
                return response()->json(['status' => 'fail', 'type' => 'providerCampaign', 'errorMessage' => trans('api.providerCampaignNotFound')], 403);
            }
        }
        return $next($request);

    }
}
