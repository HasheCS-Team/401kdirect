<?php

namespace App\Http\Middleware;

use App\Models\AdvisorCompany;
use App\Models\User;

use Closure;
use Auth;
use DB;

class CurrentPrimaryIMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $wholesaler_id = Auth()->user()->Id;
        
        $result = DB::table('investment_manager_wholesalers as imw')
                        ->join('investment_managers as im', 'imw.InvestmentManagerId', '=','im.UserId')
                        ->select('imw.Status', 'im.WholesalerPriority', DB::raw("IF( (imw.`Status` = 'External' AND im.`WholesalerPriority` = 'e'), 'Accepted',IF( (imw.Status = 'Internal' AND im.`WholesalerPriority` = 'i'),'Accepted','Denied')) AS Permission"))
                        ->where('imw.UserId', $wholesaler_id)
                        ->first();
        
        if ($result) {
            if ($result->Permission != 'Accepted') {
                return response()->json(['status' => 'fail', 'type' => 'imw-primary', 'errorMessage' => 'You dont have permission to make this change.'], 403);
            }
        } else {
            return response()->json([
                'status'=>'fail',
                'errorMessage' => 'Something went wrong.'
            ]);
        }

        return $next($request);
    }
}
