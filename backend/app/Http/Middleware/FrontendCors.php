<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class FrontendCors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedLoginPath = [
            'Advisor'=>env('ANGULAR_APP_PATH').'login',
            'Provider Wholesaler'=>env('ANGULAR_APP_PATH').'login',
            'IM Wholesaler'=>env('ANGULAR_APP_PATH').'login',
            'sales-desk'=>env('ANGULAR_APP_PATH').'login',
        ];

// print_r($allowedLoginPath);die;
        $user = User::select('AccountType')->where('Email', $request->get('Email'))->first();
        if ( $user ) {
            if ( array_key_exists($user->AccountType, $allowedLoginPath) ) {
                if ( $allowedLoginPath[$user->AccountType] == $request->headers->get('referer') ) {
                    return $next($request);
                } else {
                    return response()->json([
                        'status'=>'fail',
                        'type'=>'unauthorized',
                        'errorMessage' => 'Unauthorized access.'
                    ], 403);
                }
            } else {
                return response()->json([
                    'status'=>'fail',
                    'type'=>'unauthorized',
                    'errorMessage' => 'Unauthorized access.'
                ], 403);
            }
        } else {
            return response()->json([
                'status'=>'fail',
                'errorMessage' => 'Either email is wrong or password.'
            ]);
        }
    }
}
