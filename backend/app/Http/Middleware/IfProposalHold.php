<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Proposal\Campaigns;

class IfProposalHold
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isOnHold = 0;
        $campaignId = $request->route()->parameter('campaign_id');
        if (!empty($campaignId)) {
            $obj = Campaigns::find($campaignId);
        } else {
            $companyId = $request->route()->parameter('company_id');
            $obj = Campaigns::where(['CompanyId' => $companyId])->first();
        }
        if ($obj) {
            $isOnHold = $obj->OnHold;
        }
        if (!empty($isOnHold)) {
            return response()->json(['status' => 'fail', 'type' => 'on-hold', 'errorMessage' => 'Proposal is on hold.'], 403);
        }

        return $next($request);
    }
}
