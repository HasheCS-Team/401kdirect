<?php

namespace App\Http\Middleware;

use Closure;
use \App\Models\User;
use Auth;

class InvestmentManagerWholesaler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return 'blsabfhsafbs';
        if (Auth::check() && Auth::user()->investmentManagerWholesaler()->exists() && Auth::user()->userType() == 'IM Wholesaler')
        {
            return $next($request);
        }

        return response()->json(['status' => 'fail', 'type' => 'auth', 'errorMessage' => trans('api.privilege')], 403);
    }
}
