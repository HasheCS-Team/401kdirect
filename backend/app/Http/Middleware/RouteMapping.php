<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use App\Models\RoutesMapping;
use App\Models\RoutesMappingHistory as RMH;
use DB;
class RouteMapping
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        DB::enableQueryLog();
        $url = $request->url();
        $route = explode("api/",$url);
        $routeResult = DB::select('select * from routes_mapping rm where IsDeleted = "0" and "'.$route[1].'" like CONCAT(rm.Route,"%")');
        if(!empty($routeResult))
        {
            
            /*
            if($routeResult[0]->Route == "user/advisor/company/add/"){
                $nextResponse = $next($request);
                $response = json_decode($nextResponse->content());
                if($response->status == "success"){
                    $companyId = $response->responseData->Id;
                    $saleDeskAdvisorId = $request->header('SAuthorization');
                    $routeMappingId = $routeResult[0]->Id;
                    RMH::create([
                        'SaleDeskAdvisorSessionId' => $saleDeskAdvisorId,
                        'CompanyId' => $companyId,
                        'RouteMappingId' => $routeMappingIda
                    ]);
                    return $nextResponse;
                }else{
                    return $next($request);
                }
            }else{
                return $next($request);
            } 
            */
            $nextResponse = $next($request);
            $response = json_decode($nextResponse->content());
            if(isset($response) && isset($response->status) && $response->status == "success"){
                $companyId = $request->route('company_id');
                if(is_null($companyId)){
                    $companyId = $request->get('company_id');
                }
                if(is_null($companyId)){
                    $companyId = $request->get('companyId');
                }
                if(is_null($companyId)){
                    $referer = explode('/', $request->header('Referer'));
                    $companyId = $referer[count($referer) - 1];
                }
                $saleDeskAdvisorId = $request->header('SAuthorization');
                if($saleDeskAdvisorId && !empty($saleDeskAdvisorId) && $saleDeskAdvisorId != '' && $saleDeskAdvisorId !='null'){
                    $routeMappingId = $routeResult[0]->Id;
                    RMH::create([
                        'SaleDeskAdvisorSessionId' => $saleDeskAdvisorId,
                        'CompanyId' => $companyId,
                        'RouteMappingId' => $routeMappingId
                    ]);
                }
                return $nextResponse;
            }else{
                return $next($request);
            }
            return $next($request);
        }
        else{
            return $next($request);
        }
    }
}
