<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    // protected $except = [
    //     'login', 'register', 'confirm-mail', 'forgot-password-request', 'forgot-password-token-verify', 'reset-password', 'logout', 'excel-file-upload', 'getExcelSheetData',
    // ];

    protected $except = [
        '*'
    ];
}
