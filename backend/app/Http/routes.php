<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// 401k Direct Routes
Route::group(['prefix' => '401k-direct'], function(){
	require_once app_path('Routes/401k-direct-routes.php');
});