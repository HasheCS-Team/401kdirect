<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

//models used in this Job
use App\Models\ExcelSheet;
use Config;

class ExcelSheetDataImporter extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $path;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // \Log::info('called before sleep');
        // sleep(10);
        // \Log::info("this is job responsibility {$this->path}");
        // echo "this is job responsibility {$this->path}";

        // set execution time to unlimited...
        set_time_limit(0);

        $filePath = Config::get('constants.READ_WRITE_TMP_FILES') . 'f_5500_sf_2015_all.csv';

        //read excel file
        function getLines($file)
        {
            $f = fopen($file, 'r');
            
            try
            {
                while ($line = fgets($f))
                {
                    yield $line;
                }
            }
            finally
            {
                fclose($f);
            }
        }

        $requiredColumns = (new ExcelSheet)->getTableColumnsV2();
        $columns = [];

        foreach (getLines($filePath) as $n => $line)
        {
            \Log::info('called before sleep '. $n);
            
            //first row contains the columns 
            if ( $n == 0 )
            {
                $columns = array_flip(array_map('strtolower', explode(',', $line)));
                continue;
            }

            $lineArray = str_getcsv($line, ",", '"');

            if ( intval($lineArray[$columns[ExcelSheet::$amount]]) > 10000000 )
            {
                continue;
            }

            $sheet = ExcelSheet::where('ack_id', $lineArray[$columns['ack_id']])->first();  

            //if no record found just create new record ...         
            if ( is_null($sheet) )
            {
                $sheet = new ExcelSheet();    
            }
            else
            {
                continue;
            }

            foreach ( $requiredColumns as $col )
            {
                $sheet->$col = $lineArray[$columns[$col]];

                if ( in_array($col, ExcelSheet::$dateColumns) )
                {
                    $sheet->$col = date('Y-m-d', strtotime($lineArray[$columns[$col]]));
                }
            }

            $sheet->save();
        }

        echo "job done";
    }
}
