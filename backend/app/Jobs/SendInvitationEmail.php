<?php

namespace App\Jobs;

use App\Helpers\Classes\HelperFunctions;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Collection\AdvisorList;
use App\Models\LogEmailData;
use App\Models\Setting;
use App\Models\EmailTemplate;

use Mail;
use DB;
use Validator;

class SendInvitationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $emailFromAddress;
    private $emailBodyMessage;
    private $emailSubject;
    private $bccEmailAddress;
    private $ccEmailAddress;
    private $emailFromName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        if ( $setting = Setting::where('key', 'Fromaddress')->first(['Value']) )
        {
            $this->emailFromAddress = $setting->toArray()['Value'];
        }

        if( $setting = Setting::where('key', 'from_name')->first(['Value']) )
        {
            $this->emailFromName = $setting->toArray()['Value'];   
        }

        if( $setting = Setting::where('key', 'bcc_email_address')->first(['Value'])){
            $this->bccEmailAddress = $setting->toArray()['Value'];
        }

        if($setting = Setting::where('key', 'cc_email_address')->first(['Value'])){
           
           $this->ccEmailAddress = $setting->toArray()['Value'];
        }

        if ( $template = EmailTemplate::where(['Title' => 'Advisors-Invitation-Template', 'IsActive' => 1])->first(['Title', 'Subject', 'Body']) )
        {
            $this->emailSubject = $template->Subject;
            $this->emailBodyMessage = $template->Body;
        }


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $advisors = DB::table('advisor_collection as ac')
                        ->select('ac.Id', 'ac.FirstName', 'ac.LastName', 'ac.Email', 'ac.IsInvited', 'ac.InvitationToken', 'ac.InvitationKey')
                        ->where('InvitationKey','Yes')
                        ->where('IsInvited',0)
                        ->where('InvitationToken',NULL)
                        ->get();

        $helperFunctions = app(\App\Helpers\Classes\HelperFunctions::class);

        foreach ($advisors as $value) {

            $advisor = AdvisorList::where('Id',$value->Id)->first();

            $rules = ['Email'=>'required|email'];
            $validator = Validator::make(['Email' => trim($value->Email)], $rules);
            
            if ( $validator->fails() )
            {
                $advisor->InvitationKey = 'No';
                // $advisor->InvitationToken = json_encode(['responseData' => $validator->getMessageBag()->toArray() ]);;
                $advisor->save();
                continue;
            }

            $randomPassword = str_random(8);
            $token = $helperFunctions->encrypt($value->Email);
            $value->InvitationLink = env('ANGULAR_APP_PATH').'invitation-signup/'.$advisor->Id.'/'.str_replace('/', '', $token);

            $temp_template = $this->emailBodyMessage;

            $email_data['name'] = $value->FirstName . " " . $value->LastName;
            $email_data['to'] = trim($value->Email);
            $date = getdate (date("U"));
            $date = ( $date['month']." ".$date['mday'].", ".$date['year'] );

            $temp_template = str_replace("[user_name]", ucwords(strtolower($email_data['name']))  , $temp_template);
            $temp_template  = str_replace("[advisor_invite_url]", $value->InvitationLink , $temp_template);
            
            $temp_template = str_replace("[COMPANY]", "Hashe Computer", $temp_template); 
            $temp_template  = str_replace("[date]", $date , $temp_template);

            $link = env('ANGULAR_APP_PATH').'unsubscribe/'.$email_data['to'];
            $unsubscribeUser = 'Unsubscribe '.$email_data['to'];

            $returnHTML = view('email-template.advisor-invitation-admin.email')->with(['body'=>$temp_template, 'date' => $date, 'link' => $link, 'unsubscribeUser' => $unsubscribeUser, 'sender' => '', 'receiver' => ''])->render();

            $super_email = User::select("email")->where('AccountType', 'Admin')->first();

            $helper = new HelperFunctions;
            $mail = $helper->sendEmail($email_data['to'], $returnHTML, strtolower($this->emailSubject), 'cc', $email_data['name']);

            if ( $mail == 'fail' ) {
                $advisor->IsInvited = 1;
                $advisor->InvitationKey = 'Sent';
                $advisor->InvitationToken = $token;

                \Log::info('Opportunity Reminder Email sent: Advisor Id:'.$value->Id);
            } else {
                $advisor->InvitationKey = 'No';
                \Log::info('Opportunity Reminder Email not sent: Advisor Id:'.$value->Id);
            }

            $advisor->save();
            $helper->logEmail($email_data['to'], $this->emailFromAddress, $this->emailSubject, $this->emailBodyMessage);


        }
    }
}
