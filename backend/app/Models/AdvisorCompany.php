<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;
use App\Models\Question\QuestionAnswer;
use App\Models\AdvisorCompanyMetaData;
use App\Models\User;
use App\Models\Users\Provider;
use DB;
use Config;
use App\Models\PdfRefrences;
use App\Models\Proposal\ProviderStats;
use Carbon\Carbon;
use App\Models\Question\QuestionAnswer as QA;
use App\Models\TpaFlag;
use App\Models\Collection\TpaFee;
use App\Models\Collection\CollectionThreeSixteen;
use App\Models\Collection\ThirdPartyAdministrator;

class AdvisorCompany extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advisor_companies'; 
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted', 'CreatedDate', 'UpdatedDate'];

    /**
     * Morph relationship type...
     *
     * @var string
     */
    const TYPE_RELATION_VALUE = 'advisor_company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['AdvisorId',
                            'street_company_id',
                           'ProviderId',
                           'ParentId',
                           'OldCurrentProvider',
                           'BusinessCode',
                           'TaxPeriod',
                           'SponsEIN',
                           'PlanName',
                           'Email',
                           'SponsorName',
                           'Address1',
                           'Address2',
                           'City',
                           'State',
                           'ZipCode',
                           'PhoneNumber',
                           'AdminSignedName',
                           'TotalIncomeAmount',
                           'NetAssetsEoyAmount',
                           'FullPlanAssets',
                           'PlanAssets',
                           'ParticipentAccountBalance',
                           'SponsorDfeDbaName',
                           'Status',
                           'StatusData',
                           'CurrentBroker',
                           'TotalPaid',
                           'TotalCost',
                           'AcceptableRange',
                           'HardDollarAmount',
                           'Compensation',
                           'TpaName',
                           'TransitionDate',
                           'Year',
                           'Percentage',
                           'Comments',
                           'CreatedBy',
                           'UpdatedBy',
                           'CurrentProviderName',
                           'IsSync',
                           'IsSyncCopyPaste',
                           'IsNewForRM'
                        ];

    /**
     * Validation Rules: Add Advisor company's compensation value...
     */
    public static $compensationRules = ['year' => 'required|integer', 'percent' => 'required|numeric|between:0.00,999'];

    /**
     * Validation Rules: Update Advisor company's  Total Cost...
     */
    public static $companyProviderCreate = [
                // 'PlanName' => 'max:100',
                 'TotalCost' => 'required|numeric',
                // 'TotalPaid' => 'required|numeric',
                //'HardDollarAmount' => 'required|integer',
                'PlanName' => 'required|max:250',
                'NetAssetsEoyAmount' => 'required|integer',
                'TotalIncomeAmount' => 'required|integer',
                'ParticipentAccountBalance' => 'required|integer'];

    /*
     * Frontend Company List 
     * related by particular advisor
     */
    public static function companyList($advisorId, $offset, $limit)
    {
        return AdvisorCompany::select('advisor_companies.Id', 
          'advisor_companies.AdvisorId',
          'advisor_companies.BusinessCode',
          'advisor_companies.TaxPeriod',
          'advisor_companies.SponsEIN',
          'advisor_companies.PlanName',
          'advisor_companies.SponsorName',
          'advisor_companies.Address1',
          'advisor_companies.Address2',
          'advisor_companies.Email',
          'advisor_companies.City',
          'advisor_companies.CurrentBroker',
          'advisor_companies.State',
          'advisor_companies.ZipCode',
          'advisor_companies.TotalPaid',
          'advisor_companies.TotalCost',
          'advisor_companies.AcceptableRange',
          'advisor_companies.PhoneNumber',
          'advisor_companies.AdminSignedName',
          'advisor_companies.TotalIncomeAmount',
          'advisor_companies.NetAssetsEoyAmount',
          'advisor_companies.ParticipentAccountBalance',
          'advisor_companies.SponsorDfeDbaName',
          'advisor_companies.Compensation',
          'advisor_companies.Year',
          'advisor_companies.ProviderId',
          'advisor_companies.OldCurrentProvider',
          'advisor_companies.Percentage', 'ps.OpportunityKey', DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'))
        // DB::raw('OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue) AS OpportunityValue')
        
            ->leftJoin('campaigns as c', 'c.CompanyId', '=', 'advisor_companies.Id')
            ->leftJoin('provider_stats as ps', 'c.Id', '=', 'ps.CampaignId')
            ->whereRaw("IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 )")
            ->where('advisor_companies.AdvisorId', $advisorId)
            ->where("advisor_companies.Status", 1)
            ->groupBy('c.Id', 'advisor_companies.Id')
            ->orderBy('advisor_companies.Id','Desc')
            ->offset($offset)
            ->limit($limit)
            ->get();

// SELECT ac.SponsorName, 
//  ps.OpportunityKey
// FROM advisor_companies ac
// LEFT JOIN campaigns c
// ON c.CompanyId = ac.Id 
// LEFT JOIN provider_stats ps ON c.Id = ps.CampaignId
// WHERE ac.AdvisorId = 191
// AND ( IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 ))
// GROUP BY c.Id

                                 
    }

    public static function companyListPagination($advisorId)
    {
        $advisorCompanies = AdvisorCompany::select(
          'tpaf.Flag as TpaFlag',
          'advisor_companies.Id', 
          'advisor_companies.AdvisorId',
          'advisor_companies.BusinessCode',
          'advisor_companies.TaxPeriod',
          'advisor_companies.SponsEIN',
          'advisor_companies.PlanName',
          'advisor_companies.SponsorName',
          'advisor_companies.Address1',
          'advisor_companies.Address2',
          'advisor_companies.IsSync',
          'advisor_companies.Email',
          'advisor_companies.City',
          'advisor_companies.CurrentBroker',
          'advisor_companies.State',
          'advisor_companies.ZipCode',
          'advisor_companies.TotalPaid',
          'advisor_companies.TotalCost',
          'advisor_companies.AcceptableRange',
          'advisor_companies.PhoneNumber',
          'advisor_companies.AdminSignedName',
          'advisor_companies.TotalIncomeAmount',
          'advisor_companies.NetAssetsEoyAmount',
          'advisor_companies.FullPlanAssets',
          'advisor_companies.ParticipentAccountBalance',
          'advisor_companies.SponsorDfeDbaName',
          'advisor_companies.Compensation',
          'advisor_companies.Year',
          'advisor_companies.ProviderId',
          'advisor_companies.StableValue',
          'advisor_companies.OldCurrentProvider',     
          'advisor_companies.IsSync',
          'advisor_companies.Percentage', 'ps.OpportunityKey', DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'),
          'advisor_companies.OldCurrentProvider',
          DB::raw('GET_EXCLUDED_ASSETS(advisor_companies.Id) AS ExcludedAssets'),
          'advisor_companies.Percentage',
          'ps.OpportunityKey',
          DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'))
        // DB::raw('OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue) AS OpportunityValue')
        
            ->leftJoin('campaigns as c', 'c.CompanyId', '=', 'advisor_companies.Id')
            ->leftJoin('provider_stats as ps', 'c.Id', '=', 'ps.CampaignId')
            ->leftJoin('tpa_flag as tpaf', function($join){
              $join->on('tpaf.AdvisorId', '=', 'advisor_companies.AdvisorId');
              $join->on('tpaf.CompanyId', '=', 'advisor_companies.Id');
            })
            ->whereRaw("IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won' AND (AccessType IS NULL OR AccessType = 'W')) = 1 , ps.OpportunityKey = 'won', 1 )")
            ->where('advisor_companies.AdvisorId', $advisorId)
            ->where("advisor_companies.Status", 1)
            ->groupBy('c.Id', 'advisor_companies.Id')
            ->orderBy('advisor_companies.Id','Desc')
            ->paginate(10);
            
            $tpaFlags = [];
            foreach($advisorCompanies as $row){
              if($row->TpaFlag === null || $row->TpaFlag === ""){
                $flag = "Bundled";
                  if($row->NetAssetsEoyAmount <= 10000000)
                      $flag = "TPA";
                  array_push(
                    $tpaFlags,
                    [
                      'CompanyId' => $row['Id'],
                      'AdvisorId' => $row['AdvisorId'],
                      'Flag' => $flag,
                      'CreatedDate'=>date('Y-m-d H:i:s'),
                      'UpdatedDate'=> date('Y-m-d H:i:s')
                    ]
                  );
                  $row->TpaFlag = $flag;
              }
            }
            if(count($tpaFlags) > 0)
              TpaFlag::insert($tpaFlags);
            return $advisorCompanies;

            // ->offset($offset)
            // ->limit($limit)
            //->get();

// SELECT ac.SponsorName, 
//  ps.OpportunityKey
// FROM advisor_companies ac
// LEFT JOIN campaigns c
// ON c.CompanyId = ac.Id 
// LEFT JOIN provider_stats ps ON c.Id = ps.CampaignId
// WHERE ac.AdvisorId = 191
// AND ( IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 ))
// GROUP BY c.Id

                                 
    }

    public static function getAdvisorCompanyList($advisorId)
    {
        return AdvisorCompany::select('advisor_companies.Id', 
          'advisor_companies.AdvisorId',
          'advisor_companies.BusinessCode',
          'advisor_companies.TaxPeriod',
          'advisor_companies.SponsEIN',
          'advisor_companies.PlanName',
          'advisor_companies.SponsorName',
          'advisor_companies.Address1',
          'advisor_companies.Address2',
          'advisor_companies.Email',
          'advisor_companies.City',
          'advisor_companies.CurrentBroker',
          'advisor_companies.State',
          'advisor_companies.ZipCode',
          'advisor_companies.TotalPaid',
          'advisor_companies.TotalCost',
          'advisor_companies.AcceptableRange',
          'advisor_companies.PhoneNumber',
          'advisor_companies.AdminSignedName',
          'advisor_companies.TotalIncomeAmount',
          'advisor_companies.NetAssetsEoyAmount',
          'advisor_companies.FullPlanAssets',
          'advisor_companies.ParticipentAccountBalance',
          'advisor_companies.SponsorDfeDbaName',
          'advisor_companies.Compensation',
          'advisor_companies.Year',
          'advisor_companies.ProviderId',
          'advisor_companies.OldCurrentProvider',
          'advisor_companies.Percentage', 'ps.OpportunityKey', 
          DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'),
          DB::raw('(SELECT count(*) FROM sale_desk_advisor_history WHERE CompanyId=advisor_companies.Id) as historyExist'))
          // DB::raw('OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue) AS OpportunityValue')
        
            ->leftJoin('campaigns as c', 'c.CompanyId', '=', 'advisor_companies.Id')
            ->leftJoin('provider_stats as ps', 'c.Id', '=', 'ps.CampaignId')
            ->whereRaw("IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won' AND (AccessType IS NULL OR AccessType = 'W')) = 1 , ps.OpportunityKey = 'won', 1 )")
            ->where('advisor_companies.AdvisorId', $advisorId)
            ->where("advisor_companies.Status", 1)
            ->groupBy('c.Id', 'advisor_companies.Id')
            ->orderBy('advisor_companies.Id','Desc')
            ->paginate(10);
            // ->offset($offset)
            // ->limit($limit)
            //->get();

// SELECT ac.SponsorName, 
//  ps.OpportunityKey
// FROM advisor_companies ac
// LEFT JOIN campaigns c
// ON c.CompanyId = ac.Id 
// LEFT JOIN provider_stats ps ON c.Id = ps.CampaignId
// WHERE ac.AdvisorId = 191
// AND ( IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 ))
// GROUP BY c.Id

                                 
    }

    public static function companyListPaginationForSearch($advisorId, $planName , $date)
    {
//        dd($advisorId, $planName , $date);
      $advisorCompanies = null;
        if(($planName != '' || $planName != 'undefined') && ($date == '' || $date == 'undefined')) {
            $advisorCompanies = AdvisorCompany::select('advisor_companies.Id',
                'tpaf.Flag as TpaFlag',
                'advisor_companies.AdvisorId',
                'advisor_companies.BusinessCode',
                'advisor_companies.TaxPeriod',
                'advisor_companies.SponsEIN',
                'advisor_companies.PlanName',
                'advisor_companies.SponsorName',
                'advisor_companies.Address1',
                'advisor_companies.Address2',
                'advisor_companies.Email',
                'advisor_companies.City',
                'advisor_companies.CurrentBroker',
                'advisor_companies.State',
                'advisor_companies.ZipCode',
                'advisor_companies.TotalPaid',
                'advisor_companies.TotalCost',
                'advisor_companies.AcceptableRange',
                'advisor_companies.PhoneNumber',
                'advisor_companies.AdminSignedName',
                'advisor_companies.TotalIncomeAmount',
                'advisor_companies.NetAssetsEoyAmount',
                'advisor_companies.FullPlanAssets',
                'advisor_companies.ParticipentAccountBalance',
                'advisor_companies.SponsorDfeDbaName',
                'advisor_companies.Compensation',
                'advisor_companies.Year',
                'advisor_companies.ProviderId',
                'advisor_companies.StableValue',
                'advisor_companies.OldCurrentProvider',
                'advisor_companies.IsSync',
                'advisor_companies.Percentage', 'ps.OpportunityKey', DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'))
                ->leftJoin('campaigns as c', 'c.CompanyId', '=', 'advisor_companies.Id')
                ->leftJoin('provider_stats as ps', 'c.Id', '=', 'ps.CampaignId')
                ->leftJoin('tpa_flag as tpaf', function($join){
                  $join->on('tpaf.AdvisorId', '=', 'advisor_companies.AdvisorId');
                  $join->on('tpaf.CompanyId', '=', 'advisor_companies.Id');
                })
                ->whereRaw("IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 )")
                ->where('advisor_companies.AdvisorId', $advisorId)
                ->where('advisor_companies.PlanName', 'LIKE', '%'.$planName.'%')
                ->where("advisor_companies.Status", 1)
                ->groupBy('c.Id', 'advisor_companies.Id')
                ->orderBy('advisor_companies.Id','Desc')
                ->paginate(10);
        } elseif (($planName == '' || $planName == 'undefined') && ($date != '' || $date != 'undefined')) {
          $advisorCompanies = AdvisorCompany::select('advisor_companies.Id',
                'tpaf.Flag as TpaFlag',
                'advisor_companies.AdvisorId',
                'advisor_companies.BusinessCode',
                'advisor_companies.TaxPeriod',
                'advisor_companies.SponsEIN',
                'advisor_companies.PlanName',
                'advisor_companies.SponsorName',
                'advisor_companies.Address1',
                'advisor_companies.Address2',
                'advisor_companies.Email',
                'advisor_companies.City',
                'advisor_companies.CurrentBroker',
                'advisor_companies.State',
                'advisor_companies.ZipCode',
                'advisor_companies.TotalPaid',
                'advisor_companies.TotalCost',
                'advisor_companies.AcceptableRange',
                'advisor_companies.PhoneNumber',
                'advisor_companies.AdminSignedName',
                'advisor_companies.TotalIncomeAmount',
                'advisor_companies.NetAssetsEoyAmount',
                'advisor_companies.ParticipentAccountBalance',
                'advisor_companies.SponsorDfeDbaName',
                'advisor_companies.Compensation',
                'advisor_companies.Year',
                'advisor_companies.ProviderId',
                'advisor_companies.OldCurrentProvider',
                'advisor_companies.IsSync',
                'advisor_companies.Percentage', 'ps.OpportunityKey', DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'))
                ->leftJoin('campaigns as c', 'c.CompanyId', '=', 'advisor_companies.Id')
                ->leftJoin('provider_stats as ps', 'c.Id', '=', 'ps.CampaignId')
                ->leftJoin('tpa_flag as tpaf', function($join){
                  $join->on('tpaf.AdvisorId', '=', 'advisor_companies.AdvisorId');
                  $join->on('tpaf.CompanyId', '=', 'advisor_companies.Id');
                })
                ->whereRaw("IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 )")
                ->where('advisor_companies.AdvisorId', $advisorId)
                ->whereBetween('advisor_companies.CreatedDate',[$date.' 00:00:00', $date.' 23:59:59'])
                ->where("advisor_companies.Status", 1)
                ->groupBy('c.Id', 'advisor_companies.Id')
                ->orderBy('advisor_companies.Id','Desc')
                ->paginate(10);
        } elseif(($planName != '' || $planName != 'undefined') && ($date != '' || $date != 'undefined')) {
          $advisorCompanies = AdvisorCompany::select('advisor_companies.Id',
                'tpaf.Flag as TpaFlag',
                'advisor_companies.AdvisorId',
                'advisor_companies.BusinessCode',
                'advisor_companies.TaxPeriod',
                'advisor_companies.SponsEIN',
                'advisor_companies.PlanName',
                'advisor_companies.SponsorName',
                'advisor_companies.Address1',
                'advisor_companies.Address2',
                'advisor_companies.Email',
                'advisor_companies.City',
                'advisor_companies.CurrentBroker',
                'advisor_companies.State',
                'advisor_companies.ZipCode',
                'advisor_companies.TotalPaid',
                'advisor_companies.TotalCost',
                'advisor_companies.AcceptableRange',
                'advisor_companies.PhoneNumber',
                'advisor_companies.AdminSignedName',
                'advisor_companies.TotalIncomeAmount',
                'advisor_companies.NetAssetsEoyAmount',
                'advisor_companies.ParticipentAccountBalance',
                'advisor_companies.SponsorDfeDbaName',
                'advisor_companies.Compensation',
                'advisor_companies.Year',
                'advisor_companies.ProviderId',
                'advisor_companies.OldCurrentProvider',
                'advisor_companies.IsSync',
                'advisor_companies.Percentage', 'ps.OpportunityKey', DB::raw('IF(ps.OpportunityKey = "open",OPPORTUNITY_OPEN_DAYS(ps.OpportunityValue), ps.OpportunityValue ) as OpportunityValue'))
                ->leftJoin('campaigns as c', 'c.CompanyId', '=', 'advisor_companies.Id')
                ->leftJoin('provider_stats as ps', 'c.Id', '=', 'ps.CampaignId')
                ->leftJoin('tpa_flag as tpaf', function($join){
                  $join->on('tpaf.AdvisorId', '=', 'advisor_companies.AdvisorId');
                  $join->on('tpaf.CompanyId', '=', 'advisor_companies.Id');
                })
                ->whereRaw("IF( (SELECT COUNT(pss.Id) FROM provider_stats pss WHERE pss.CampaignId = c.Id AND pss.OpportunityKey = 'won') = 1 , ps.OpportunityKey = 'won', 1 )")
                ->where('advisor_companies.AdvisorId', $advisorId)
                ->where('advisor_companies.PlanName', 'LIKE', '%'.$planName.'%')
                ->whereBetween('advisor_companies.CreatedDate',[$date.' 00:00:00', $date.' 23:59:59'])
                ->where("advisor_companies.Status", 1)
                ->groupBy('c.Id', 'advisor_companies.Id')
                ->orderBy('advisor_companies.Id','Desc')
                ->paginate(10);
        } else {
          $advisorCompanies = [];
        }

        $tpaFlags = [];
            foreach($advisorCompanies as $row){
              if(!$row->TpaFlag || $row->TpaFlag === null || $row->TpaFlag === ""){
                  $flag = getServiceModelwhenNotProvider($row->NetAssetsEoyAmount);
                  array_push(
                    $tpaFlags,
                    [
                      'CompanyId' => $row['Id'],
                      'AdvisorId' => $row['AdvisorId'],
                      'Flag' => $flag,
                      'CreatedDate'=>date('Y-m-d H:i:s'),
                      'UpdatedDate'=> date('Y-m-d H:i:s')
                    ]
                  );
                  $row->TpaFlag = $flag;
              }
            }
            if(count($tpaFlags) > 0)
              TpaFlag::insert($tpaFlags);
            return $advisorCompanies;
    }

    public static function companyNamesList($advisorId)
    {
        return AdvisorCompany::select('Id','SponsorName')
                              ->where('AdvisorId', $advisorId)
                              ->where("Status", 1)
                              ->get();
                                 
    }

    public static function companyNamesData($advisorId,$companyId)
    {
      return AdvisorCompany::select('Id','SponsorName','SponsorName','NetAssetsEoyAmount','TotalCost','AcceptableRange','ProviderId')
                              ->where('AdvisorId', $advisorId)
                              ->where('Id', $companyId)
                              ->where("Status", 1)
                              ->get();
        // return DB::table('advisor_companies AS ac')
        //             ->select('ac.Id','ac.ProviderId','ac.SponsorName','ac.NetAssetsEoyAmount','ac.TotalCost','ac.AcceptableRange', 'acm.PercentageDifference',
        //                                            'acm.CostStatus',
        //                                            'acm.AcceptableRangeStatus',
        //                                            'acm.BenchmarkingAverage')
        //             ->join('advisor_company_metadata AS acm', 'ac.Id', '=', 'acm.CompanyId')
        //             ->where('ac.Id', $companyId)
        //             ->where('ac.AdvisorId', $advisorId);
                                 
    }

    
    public function fundCategories()
    {
        return $this->morphMany('App\Models\FundCategory', 'UserId', 'UserType', 'UserId')
                    ->select(['Id', 'Name','WeightedExpense' ,'CreatedDate', 'UpdatedDate']);
    }

    public function fundCategoriesCopyPaste()
    {
        return $this->morphMany('App\Models\FundCategoryCopyPaste', 'UserId', 'UserType', 'UserId');
    }

    public function fundCategoriesProposal()
    {
        return $this->hasMany('App\Models\FundCategoryProposal', 'CompanyId','Id')
                    ->select(['Id', 'Name', 'AssetsValue', 'WeightedExpense','PlanPercentage' ,'CreatedDate', 'UpdatedDate']);
    }

     /**
     * The Advisor that belong to the User.
     */
    public function advisor()
    {
        return $this->belongsTo('App\Models\Users\Advisor', 'AdvisorId', 'UserId');
    }

    /**
     * The Advisor that belong to the User.
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider', 'ProviderId', 'UserId');
    } 

     /**
     * The Advisor company has one company provider.
     */
    public function companyProvider()
    {
        return $this->hasOne('App\Models\CompanyProvider', 'AdvisorCompanyId', 'Id');
    }

    // /**
    //  * The Advisor company has one provider
    //  */
    // public function provider()
    // {
    //     return $this->hasOne('App\Models\Users\Provider', 'UserId', 'ProviderId');
    // } 


      /**
     * The Advisor company has many Question Answers.
     */
    public function questionAnswer()
    {
        return $this->hasMany('App\Models\Question\QuestionAnswer', 'CompanyId', 'Id');
    } 

     /**
     * The Advisor company has many Company Steps.
     */
    public function companyStep()
    {
        return $this->hasMany('App\Models\CompanySteps', 'AdvisorCompanyId', 'Id');
    } 

    /**
     * The Advisor company has many Company Steps.
     */
    public function providerStats()
    {
        return $this->hasMany('App\Models\Provider\ProviderStats', 'AdvisorId', 'Id');
    }

    /**
     * The Advisor has many relation to the Campaigns Mdoel
     */
    public function benchmarkingResults()
    {
        return $this->hasMany('App\Models\BenchmarkingResults', 'CompanyId', 'Id');
    }

    /**
     * The AdvisorCompany has many relation to the Benchmarks Model
     */
    public function benchmarks()
    {
        return $this->hasMany('App\Models\Benchmark', 'CompanyId', 'Id');
    }

    /**
     * The Advisor has many relation to the Campaigns Mdoel
     */
    public function campaigns()
    {
        return $this->hasMany('App\Models\Proposal\Campaigns', 'CompanyId', 'Id');
    }

    /**
     * The Advisor has many relation to the Campaigns Mdoel
     * use this for future as there is One to One relation between company and campaign
     */
    public function campaign()
    {
        return $this->hasOne('App\Models\Proposal\Campaigns', 'CompanyId', 'Id');
    }

    /**
     * The Advisor company has many Meta Data.
     */
    public function advisorCompanyMetaData()
    {
        return $this->hasOne('App\Models\AdvisorCompanyMetaData', 'CompanyId', 'Id')->select('Id','CompanyId','OpportunityKey','OpportunityValue');
    } 



    public function scopeColumns($query, $advisor)
    {
        return $query->select(['Id',
                               'BusinessCode',
                               'TaxPeriod',
                               'SponsEIN',
                               'PlanName',
                               'SponsorName',
                               'Address1',
                               'Address2',
                               'City',
                               'Compensation',
                               'Year',
                               'Percentage',
                               'State',
                               'ZipCode',
                               'PhoneNumber',
                               'AdminSignedName',
                               'TotalIncomeAmount',
                               'NetAssetsEoyAmount',
                               'ParticipentAccountBalance',
                               'SponsorDfeDbaName'
                           ])->where('AdvisorId', $advisor)->orderBy('Id','Desc');
    }

    public static function companyProviderExists($company_id, $provider_id)
    {
      return static::where(['Id' => $company_id, 'ProviderId' => $provider_id])->exists();
    }

    /*
     * Front End
     * Check Opportunities again AdvisorCompanyId
     */
    public static function getOpportunitiesInfo($advisor_id, $company_ids, $opportunity_day_limit)
    {
        // return $query->select(['Id','PlanName','NetAssetsEoyAmount','TotalIncomeAmount','ParticipentAccountBalance'])->with(['advisorCompanyMetaData' => function($query){
        //     $query->select('Id','CompanyId','OpportunityKey','OpportunityValue');
        // }])->where('AdvisorId',$advisor_id)->where('Status',1)->whereIn('Id',$company_ids)->get();

        return DB::table('users as u')
                  ->join('advisors as a', 'u.Id', '=', 'a.UserId')
                  ->join('advisor_companies as ac', 'u.Id', '=', 'ac.AdvisorId')
                  ->join('benchmarks as b', 'ac.Id', '=', 'b.CompanyId')
                  ->join('advisor_company_metadata as acm', 'ac.Id', '=', 'acm.CompanyId')
                  // ->join('campaigns as cam', 'ac.AdvisorId', '=', 'cam.AdvisorId')
                  ->join('campaigns as cam', 'ac.Id', '=', 'cam.CompanyId')
                  ->join('provider_stats as ps', 'cam.Id', '=', 'ps.CampaignId')
                  // ->select('cam.CreatedDate','cam.Id as CampaignId','ac.Id', 'cam.CompanyId', 'cam.AdvisorId', 'ac.PlanName', 'ac.NetAssetsEoyAmount', 'ac.TotalIncomeAmount', 'ac.ParticipentAccountBalance', 'ps.OpportunityKey', 'ps.OpportunityValue', DB::Raw('IF(cam.IsStatusSet = 0, DATEDIFF(CURRENT_DATE(),cam.CreatedDate), DATEDIFF("2018-06-01",SUBSTRING_INDEX(ps.OpportunityValue, "|", -1))) AS DaysDifference'), 'cam.IsStatusSet',)
                  ->select('cam.CreatedDate','cam.Id as CampaignId','ac.Id', 'cam.CompanyId', 'cam.AdvisorId', 'ac.PlanName', 'ac.NetAssetsEoyAmount','b.PlanAssets','b.FullPlanAssets', 'b.TotalIncomeAmount', 'b.ParticipentCount as ParticipentAccountBalance', 'ps.OpportunityKey', 'ps.OpportunityValue', DB::Raw('IF(cam.IsStatusSet = 0, ('.$opportunity_day_limit.' - DATEDIFF(CURRENT_DATE(),cam.CreatedDate)), DATEDIFF(CURRENT_DATE(),SUBSTRING_INDEX(ps.OpportunityValue, "|", -1))) AS DaysDifference'), 'cam.IsStatusSet', DB::Raw('IF(cam.IsStatusSet = 0, If(('.$opportunity_day_limit.' - DATEDIFF(CURRENT_DATE(),cam.CreatedDate)) < 0, TRUE, FALSE), If(DATEDIFF(CURRENT_DATE(),SUBSTRING_INDEX(ps.OpportunityValue, "|", -1))>0,TRUE,FALSE)) AS IsDaysExceeded'))
                  ->whereIn('cam.CompanyId', $company_ids)
                  // ->where('cam.IsStatusSet', 0)
                  ->whereRaw("( ps.OpportunityKey = 'open' )")                  
                  // ->whereRaw("cam.IsStatusSet = 0 || ( ps.OpportunityKey = 'open' AND IS_OPPORTUNITY_OPEN_DATE_REACHED(ps.OpportunityValue) ) ")                  
                  ->where('cam.AdvisorId', $advisor_id)
                  ->where('ps.Status', 'Current')
                  ->groupBy('cam.CompanyId', 'cam.AdvisorId')
                  ->get();
    } 

    /**
     * Private Functions
     * get provider name and ids from company log table and benchmark table
     */
    private static function proposalProviders($advisor_id, $company_id)
    {
        return DB::table('company_proposal_log as CPL')->join('benchmarking_results as BR', function($join){
              $join->on(DB::raw("find_in_set(BR.Id, CPL.provider_increamental_ids)"), DB::raw(''), DB::raw(''));
            })  
            ->select('BR.ProviderId', 'BR.ProviderName', 'CPL.currentProvider')
            ->where('CPL.advisor_id','=',$advisor_id)
            ->where('CPL.company_id','=',$company_id)
            ->get();
    }

    /**
     * Front-End
     * get company proposal providers without Current Provider for opportunity page
     */
    public static function getProposalProviders($advisor_id, $company_id)
    {   
        $providerslist = self::proposalProviders($advisor_id, $company_id);

        $providerId = self::select('OldCurrentProvider')->where('Id', $company_id)->first();

         return ['providerslist' => $providerslist, 'currentProvider' => $providerId];
    }

    /**
     * Front-End
     * get company proposal providers with Current Provider for opportunity page
     */
    public static function getProposalWithCurrentProvider($advisor_id, $company_id, $provider_id)
    {
      $proposalProvider = self::proposalProviders($advisor_id, $company_id);

      //Get proposalProvider Id's
      foreach ($proposalProvider as $key => $value) {
        $providerIds[] = $value->ProviderId;
      }

      $match_provider  = array_filter( $proposalProvider, function ($e) use($provider_id) { return $e->ProviderId == $provider_id; } );

      if( count($match_provider) > 0 )
      {
        $currentProviderResult = BenchmarkingResults::select('ProviderId', 'ProviderName')
          ->where('AdvisorId','=',$advisor_id)
          ->where('CompanyId','=',$company_id)
          ->where("ProcessId", '=', DB::raw("(SELECT bb.ProcessId FROM benchmarking_results bb WHERE bb.AdvisorId = '$advisor_id' AND bb.CompanyId = '$company_id' ORDER BY bb.ProcessId DESC LIMIT 1)"))
          ->whereIn('ProviderId', $providerIds)
          ->get()
          ->toArray();
 
        return ['providerslist' => $currentProviderResult  , 'currentProvider' => $provider_id ];
      }
      else
      {
          $currentProviderResult = BenchmarkingResults::select('ProviderId', 'ProviderName')
          ->where('AdvisorId','=',$advisor_id)
          ->where('CompanyId','=',$company_id)
          ->where("ProcessId", '=', DB::raw("(SELECT bb.ProcessId FROM benchmarking_results bb WHERE bb.AdvisorId = '$advisor_id' AND bb.CompanyId = '$company_id' ORDER BY bb.ProcessId DESC LIMIT 1)"))
          ->where('ProviderId', $provider_id)
          ->get()
          ->toArray();

          $providerslist = array_merge($proposalProvider, $currentProviderResult);

          return ['providerslist' => $providerslist  , 'currentProvider' => $provider_id ];
      }
    }

    /**
     * Opportunity Page
     * check provider id exists in won case
     */
    public static function checkOpportunityProviderId($advisor_id, $company_id, $provider_id)
    {
      $response = self::getProposalProviders($advisor_id, $company_id);

      // dd($response);

      foreach ($response as $key => $value) {

        if( (int)$provider_id == $value->ProviderId)
        {
          return true;
        }
        else
        {
          return false;
        }
      }

      //return $response;
    }

    /*
     * Front End
     * Check Advisor Id & Step Id for Progress Bar
     */
    public static function checkAdvisorCompanyStep($advisor,$advisorCompanyId)
    {
      return self::find($advisorCompanyId)->companyStep()->where("AdvisorCompanyId", $advisorCompanyId)->first();
    }

    /*
     * Front End
     * Check Advisor Id & Advisor Id for the Progress Bar
     */
    public static function checkAdvisorCompany($advisor,$advisorCompanyId)
    {
      return self::where("Id", $advisorCompanyId)->where('AdvisorId', $advisor)->exists();
    }

    /**
     * [advisorCompanyData plan summmary page and 4 boxes]
     * @param  [type] $advisorCompanyId [advisor_id]
     * @return [type]                   [company id]
     */
    public static function planInfo($advisorCompanyId)
    {
      $response = self::find($advisorCompanyId, ['Id', 'PlanName', 'TotalCost', 'NetAssetsEoyAmount', 'HardDollarAmount', 'AcceptableRange', 'ParticipentAccountBalance']);
      
      $fundCategories = $response->fundCategories->toArray();
      //return $fundCategories;

      if( !is_null( $fundCategories) )
      {
        
        //Total Cost & Percentage..
        $response->totalCost = (($response->NetAssetsEoyAmount * $response->TotalCost)/100) + $response->HardDollarAmount;

        //Investment Cost & percentage..
        $totalWeightedExpensePercentage = array_sum(array_map(function($elm){ return $elm['WeightedExpense']; }, $fundCategories));
        
        $response->TotalWeightedExpensePercentage = Round($totalWeightedExpensePercentage,2);

        $totalWeightedExpense = (($response->TotalWeightedExpensePercentage * $response->NetAssetsEoyAmount)/100);
        $response->TotalWeightedExpense = Round($totalWeightedExpense,2);
        
        // Other Cost & Percentage
        $response->otherCostPercentage =  round(($response->HardDollarAmount * $response->TotalCost) / $response->TotalWeightedExpense ,2 ) ;
        $response->TotalCost = round($response->TotalCost + $response->otherCostPercentage,2) ;
        $response->otherTotalCost = $response->totalCost - $response->TotalWeightedExpense;
      }

      // return round(($response->TotalWeightedExpensePercentage * $response->NetAssetsEoyAmount)/100 ,0);
      
      if( $response->ParticipentAccountBalance != 0 )
      {
        $perParticipants = $response->totalCost / $response->ParticipentAccountBalance;

        $response->perParticipants = round($perParticipants);
      }
      else
      {
        $response->perParticipants = 0;
      }

      //$response->FundsCollected = count($fundCategories)? true : false;
      $response->CheckQuesAns = (boolean) QuestionAnswer::where('CompanyId', $advisorCompanyId)->exists();
      return $response;
    

    }

    public static function calculateTotalCost($company_id)
    {
      return self::select('TotalCost', 'HardDollarAmount', 'ParticipentAccountBalance', 'NetAssetsEoyAmount')->where('Id', $company_id)->first();
    }

    public static function updateFeeMonitorStatus($companyId, $advisorId, $isFeeMonitor)
    {
       $advisorCompany = self::where(['AdvisorId' => $advisorId,'Id' => $companyId])->first();       
       
       $advisorCompany->IsMonitor = $isFeeMonitor;

       $advisorCompany->update();

       return $advisorCompany;
    }

    /*
     * Front-End
     * Auto Fill Data for Manually enter the data from the 408(b)(2) and add the plan’s investments and plan info
     */
    public static function companyPlanInfo($company_id, $advisor_id)
    {
      $response = self::select('Id',
                               'PlanName',
                               'TotalCost',
                               'TotalPaid',
                               'HardDollarAmount',
                               DB::raw('ROUND(NetAssetsEoyAmount) as NetAssetsEoyAmount'),
                               'Address1',
                               'City',
                               'State',
                               'ZipCode',
                               'AdminSignedName',
                               'SponsorName',
                               'PhoneNumber',
                               'TotalIncomeAmount',
                               'FullPlanAssets',
                               'ParticipentAccountBalance',
                               'UpdatedDate',
                               'IsSync')->where("Id", $company_id)->where('AdvisorId', $advisor_id)->first();
      
      $fundCategories = $response->fundCategories->toArray();

      $response->FundsCollected = count($fundCategories) ? true : false;     
      
      return $response;
    }

    /**
     * [selectProviderInfo Data]
     * @param  [type] $company_id [id]
     * @param  [type] $advisor_id [id]
     * @return [type]             [description]
     */
    public static function selectProviderInfo($company_id, $advisor_id, $provider_id)
    {
      $response = self::select(
        'Id', 
        'PlanName', 
        'SponsorName', 
        'IsMonitor',
        DB::raw('(select TpaName from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" and Status = "Current" AND (AccessType IS NULL OR AccessType = "W")) as TpaName'),
        DB::raw('(select ConfirmPageStatus from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" and Status = "Current" AND (AccessType IS NULL OR AccessType = "W")) as ConfirmPageStatus'),
        DB::raw('(select TRIM(FirmName) from `providers` WHERE UserId = "'.$provider_id.'") AS ProviderName'),
        DB::raw('(select ProviderStatus from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" and Status = "Current" AND (AccessType IS NULL OR AccessType = "W")) as ProviderStatus'),
        DB::raw('(select TransitionDate from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" and Status = "Current" AND (AccessType IS NULL OR AccessType = "W")) as TransitionDateStatus')
      )
      ->where(["Id" => $company_id, 'AdvisorId' => $advisor_id])
      ->first();

      $response->Advisor = User::select(\DB::raw("CONCAT(FirstName, ' ', LastName) AS Name"))->where('Id', $advisor_id)->first();
      $provider = Provider::find($provider_id);
      
      $response->option  = QA::getMappingOrTargetStatusBenchmarked($company_id);


      $response->ProviderStartDays = $provider->StartDays;
      $addDay = Carbon::today()->addDays($response->ProviderStartDays);

      if ($addDay->day != 1){
        $addMonth = Carbon::createFromFormat( 'Y-m-d H:i:s', $addDay->toDateTimeString())->addMonth()->firstOfMonth();
        $date = $addMonth->isWeekend()?$addMonth->addWeekday():$addMonth;
      } else {
        $date = $addDay->isWeekend()?$addDay->addWeekday():$addDay;
      }
      

      $response->date = date('M d, Y', strtotime($date->toDateTimeString()));
      return $response;
    }

    public static function checkProductUsedOrNot($company_id, $advisor_id, $provider_id)
    {
      $response = \DB::table('advisor_companies as ac')
          ->select('br.ProductId','br.ProcessId', 'br.EstimatedTPAFee', 'br.EstimatedThreeSixteenFee', 'br.TpaFeeGrid', 'br.ThreeSixteenFeeGrid')
          ->join('company_proposal_log as cpl', 'ac.Id', '=', 'cpl.company_id')
          ->join('benchmarking_results as br', 'cpl.process_id', '=', 'br.ProcessId')
          ->where('ac.AdvisorId', $advisor_id)
          ->where('ac.Id', $company_id)
          ->where('br.ProviderId', $provider_id)                 
          ->first();
      $response->TpaFeeGrid = json_decode($response->TpaFeeGrid);
      $response->ThreeSixteenFeeGrid = json_decode($response->ThreeSixteenFeeGrid);
      return $response;

    }

    /**
     * [get fiduciaryDueInfo Data]
     * @param  [type] $company_id [id]
     * @param  [type] $advisor_id [id]
     * @return [type]             [description]
     */
    
    public static function fiduciaryDueInfo($company_id, $advisor_id, $provider_id)
    { 
      // print_r($provider_id);
      // die();
      // $response = \DB::table('advisor_companies as ac')
      //           ->select('ac.Id', 'ac.ProviderId', 'ac.Email','ac.PlanName', 'ac.SponsorName', DB::raw('(select TransitionDate from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'") as TransitionDate') , 'ac.IsMonitor', DB::raw('(select TpaName from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'") as TpaName'),'br.Percentage',
      //             'br.InvestmentCost', 'br.ProviderFeeCost', 'ac.NetAssetsEoyAmount',
      //              'ac.TotalCost', 'ac.HardDollarAmount', 'ac.ParticipentAccountBalance', 'cpl.company_id', 'cpl.process_id', 'br.ProcessId as BrProcessId', 'br.TotalCost as BenchmarkingTotalCost', 'br.TotalCostPerPart')
      //           ->join('advisor_company_metadata as acm', 'ac.Id', '=', 'acm.CompanyId')
      //           ->join('company_proposal_log as cpl', 'ac.Id', '=', 'cpl.company_id')
      //           ->join('benchmarking_results as br', 'cpl.process_id', '=', 'br.ProcessId')
      //           ->whereRaw('ac.ProviderId = br.ProviderId')
      //           ->where('ac.AdvisorId', $advisor_id)
      //           ->where('ac.Id', $company_id)                
      //           ->first();

      $response = \DB::table('advisor_companies as ac')
          ->select('ac.Id', 'ac.ProviderId', 'ac.Email','ac.PlanName', 'ac.SponsorName', DB::raw('(select TransitionDate from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" AND (AccessType IS NULL OR AccessType = "W")) as TransitionDate') , 'ac.IsMonitor', DB::raw('(select TpaName from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" AND (AccessType IS NULL OR AccessType = "W")) as TpaName'),  DB::raw('(select ProviderStatus from provider_stats where AdvisorId = "'.$advisor_id.'" and CompanyId = "'.$company_id.'" and ProviderId = "'.$provider_id.'" AND (AccessType IS NULL OR AccessType = "W")) as ProviderStatus') ,'br.Percentage',
            'br.InvestmentCost', 'br.ProviderFeeCost', 'ac.NetAssetsEoyAmount',
             'ac.TotalCost', 'ac.HardDollarAmount', 'ac.ParticipentAccountBalance', 'cpl.company_id', 'cpl.process_id', 'br.ProcessId as BrProcessId', 'br.TotalCost as BenchmarkingTotalCost', 'br.TotalCostPerPart')
          // ->join('advisor_company_metadata as acm', 'ac.Id', '=', 'acm.CompanyId')
          ->join('company_proposal_log as cpl', 'ac.Id', '=', 'cpl.company_id')
          ->join('benchmarking_results as br', 'cpl.process_id', '=', 'br.ProcessId')
          // ->whereRaw('ac.ProviderId = ps.ProviderId')
          ->where('ac.AdvisorId', $advisor_id)
          ->where('ac.Id', $company_id)
          ->where('br.ProviderId', $provider_id)                 
          ->first();
    
      // $date = date_create($response->TransitionDate);
      
      // $response->TransitionDate = date_format($date, 'Y-m-d');

      $tpaCollectionName ='';
      $threeSixteenCollectionName ='';
      $EstimatedTPAFee ='';
      $tpaThreeSixteenFee = '';
      $EstimatedThreeSixteenFee ='';

      $company_tpa = TpaFee::getCompanyTpaFee($company_id, $advisor_id);
      
      
      if($company_tpa && $company_tpa->ProductId){

          $productTPA = TpaFee::getProductTpaFee($company_tpa->ProductId, $provider_id);
            
          if($productTPA && ($productTPA->CollectionTpaId > 0 || $productTPA->CollectionThreeSixteenId > 0) ){
              $tpaThreeSixteenFee = $productTPA;
          }
          else{
              $tpaThreeSixteenFee = $company_tpa;
          }
      }
      else{
          $tpaThreeSixteenFee = $company_tpa;
      }

      if(count($tpaThreeSixteenFee) > 0)
      {
          if($tpaThreeSixteenFee['CollectionTpaId'] > 0)
          {
              $tpaName = ThirdPartyAdministrator::select('tpa_master_name')->where('Id',$tpaThreeSixteenFee['CollectionTpaId'])->first();
              if($tpaName)
              {
                  $tpaCollectionName = $tpaName->tpa_master_name;
                  $EstimatedTPAFee = $tpaThreeSixteenFee->EstimatedTPAFee;
              }
          }
          if($tpaThreeSixteenFee['CollectionThreeSixteenId'] > 0)
          {
              $threeSixteenName = CollectionThreeSixteen::select('Name')->where('Id',$tpaThreeSixteenFee['CollectionThreeSixteenId'])->first();
              if($threeSixteenName)
              {
                  $threeSixteenCollectionName = $threeSixteenName->Name;
                  $EstimatedThreeSixteenFee = $tpaThreeSixteenFee->EstimatedThreeSixteenFee;
              }
          }
      }
      $response->tpaCollectionName = $tpaCollectionName;
      $response->threeSixteenCollectionName = $threeSixteenCollectionName;
      $response->EstimatedTPAFee = $EstimatedTPAFee;
      $response->EstimatedThreeSixteenFee = $EstimatedThreeSixteenFee;

      $response->companyFlag = TpaFlag::getCompanyFlag($company_id, $advisor_id);

      $response->TransitionDate = date('F j, Y', strtotime( $response->TransitionDate) );
      $response->option = QA::getMappingOrTargetStatusBenchmarked($company_id);
      $CalculatedTotalCost =  ROUND( (($response->NetAssetsEoyAmount * $response->TotalCost) / 100) + $response->HardDollarAmount , 0);
      
      if( $response->ParticipentAccountBalance != 0 )
      {
        //Plan per participant..
        $response->perParticipants = round( $CalculatedTotalCost / $response->ParticipentAccountBalance );

        $response->savingPerParticipants = (ROUND($response->TotalCostPerPart - ($CalculatedTotalCost / $response->ParticipentAccountBalance), 0));
      }
      else
      {
        $response->perParticipants = 0;
        $response->savingPerParticipants = 0;
      }

      $yourSaving =  $response->BenchmarkingTotalCost - $CalculatedTotalCost;
      
      $response->yourSaving = (int) $yourSaving;
      
      $response->TotalCost =  (float) $response->TotalCost;

      // $response->ProviderName = User::select(\DB::raw("CONCAT(FirstName, ' ', LastName) AS Name"))->where('Id', $response->ProviderId)->first();
      $response->ProviderName = Provider::select(\DB::raw("FirmName AS Name"))->where('UserId', $response->ProviderId)->first();

      // For fiduciary fee information on final confirmation page.
      $question_info = self::getFiduciaryFeeQuestionInfo($company_id);
      if ($question_info && $question_info->OptionId == 3) {
        $fiduciary_info = DB::table('providers as p')
                ->select('p.UserId', 'p.FiduciaryFee', 'p.FiduciaryFirmName')
                ->where('p.UserId', $provider_id)
                ->first();
        $response->IsQuestionTwo = true;
        $response->FiduciaryFee = $fiduciary_info->FiduciaryFee;
        $response->FiduciaryFirmName = ($fiduciary_info->FiduciaryFirmName) ? $fiduciary_info->FiduciaryFirmName : '';
      }
      else
      {
        $response->IsQuestionTwo = false;
        $response->FiduciaryFee = 0;
        $response->FiduciaryFirmName = '';
      }
      
      return $response;
    }

    public static function getFiduciaryFeeQuestionInfo($company_id)
    {
      $response = DB::table('company_question_answers as cqa')
                ->select('cqa.OptionId')
                ->where('cqa.CompanyId', $company_id)
                ->where('cqa.QuestionId', 2)
                ->first();
      return $response;
    }

    public static function fiduciaryDueDocInfo($company_id, $advisor_id)
    {
      $baseUrl = \Config::get('constants.APP_BASE_URL');

      $pdfRefInstance = new PdfRefrences();
      $pdfRefInstance->advisorId = $advisor_id;
      $pdfRefInstance->companyId = $company_id;
      $pdfRefInstance->save();

      $ref = str_pad($pdfRefInstance->Id, 10, '0', STR_PAD_LEFT);
      $hashKey = sha1($ref . $advisor_id . $company_id);

      $pdfRefInstance->refrenceNumber = $hashKey;
      $pdfRefInstance->save();    
      

      $response =  self::select('Id', 'ProviderId', 'Email','PlanName', 'SponsorName')->where('AdvisorId', $advisor_id)->where('Id', $company_id)->first();

      $response->Advisor = User::select(\DB::raw("CONCAT(FirstName, ' ', LastName) AS Name"))->where('Id', $advisor_id)->first();

      
      $response->hashKey = $hashKey;

      $response->url =  $baseUrl. "/public/test/pdf";

      return $response;
    }

    /**
     * [getCompanyEmail Email Address]
     * @param  [int] $advisorId [id]
     * @param  [int] $companyId [id]
     * @return [string] Email   [email addresss]
     */
    public static function getCompanyInfo($advisorId, $companyId)
    {
      return self::select('Email', 'SponsorName', 'OldCurrentProvider', 'SelectedYear', 'ChangeProbability', 'TotalCost as CurrentCost')
                  ->where('Id', $companyId)
                  ->where('AdvisorId', $advisorId)
                  ->first();
    }

    public static function saveEmailAddress($advisorId, $companyId, $email)
    {
      $advisorCompany = self::where(['AdvisorId' => $advisorId,'Id' => $companyId])->first();

      if ( !$advisorCompany )
      {
        return false;
      }

      $advisorCompany->Email = $email;

      if ( $advisorCompany->update() )
      {
        return true;
      }
      
      return false;
    }

    public static function updateSelectedYear($advisorId,$companyId,$selected_year){

      $advisorCompany = self::where(['AdvisorId' => $advisorId,'Id' => $companyId])->first();

      if(!$advisorCompany){
        return false;
      }

      $advisorCompany->SelectedYear = $selected_year;

      if($advisorCompany->update())
      {
        return true;
      }
      
      return false;
      
    }

    public static function updateChangeProbability($advisor_id, $company_id, $change_probability)
    {
      $advisor_company = self::where(['AdvisorId' => $advisor_id,'Id' => $company_id])->first();

      if ( !$advisor_company )
        return false;

      $advisor_company->ChangeProbability = $change_probability;

      if ( $advisor_company->update() )
        return true;
      
      return false;
    }

    public static function getCurrentProvider($advisorId,$companyId)
    {
      $CurrentProviderId = self::select('OldCurrentProvider','CurrentProviderName')       
                                 ->where(['AdvisorId' => $advisorId,'Id' => $companyId])
                                 ->first();
      return $CurrentProviderId;

    }

        /*
     * Front End
     * Get Company Ids from company proposal log  
     */
    public function scopeCheckStepNumber($query, $advisor_id)
    {
        return \DB::table('company_proposal_log as cpl')
                ->select('cpl.provider_increamental_ids','cpl.advisor_id', 'cpl.company_id')
                ->where('cpl.advisor_id', $advisor_id);
    }

    public static function checkCurrenProvider($company_id, $advisor_id)
    {
      return self::select("ProviderId")->where(['AdvisorId' => $advisor_id, 'Id' => $company_id])->first();
      // return $p && $p->ProviderId ? true : false;
    }

    /* Check Changes for advisor and wholesaler */
    public static function checkChanges($company_id, $provider_id, $status)
    {
        return Db::table('provider_stats as ps')
                        ->join('proposal_categories_history as pch', 'ps.Id', '=', 'pch.ProposalConnectsId')
                        ->where('ps.CompanyId', $company_id)
                        ->where('ps.ProviderId', $provider_id)
                        ->where('pch.ChangeBy', $status)
                        ->count();
    }

    /* identify wholesaler with respect to advisor zip code */
    public static function getWholesalerInfo($advisor_id, $company_id, $provider_id)
    {
        $wholesaler_id = [];
        $response = [];

        $CurrentProvideresult=AdvisorCompany::select('OldCurrentProvider')->where('Id',$company_id)->first();
        $CurrProvideresult = $CurrentProvideresult->OldCurrentProvider;
        if($CurrProvideresult == $provider_id){
          $wholesaler =  ProviderStats::getRMOrDefaultbyProviderID($advisor_id, $company_id, $provider_id);
          $isRM=1;
          if(!empty($wholesaler)) {
            array_push($wholesaler_id, $wholesaler[0]->WholesalerId);
            
            if(!empty($wholesaler[1])){
              array_push($wholesaler_id, $wholesaler[1]->WholesalerId);
            }
          }
        }
        else{
           $wholesaler = DB::select("CALL sp_proposal_wholesaler_assignment($advisor_id, $company_id, $provider_id)");
           $isRM=0;
            if (count($wholesaler) > 0) {
              array_push($wholesaler_id, $wholesaler[0]->WholesalerId);
              $wholesaler_status = $wholesaler[0]->STATUS;

              $other_matched_wholesaler = DB::select("CALL sp_wholesaler_internal_external_match($advisor_id, $company_id, $provider_id, '$wholesaler_status')");

              if (count($other_matched_wholesaler) > 0) {
                  array_push($wholesaler_id, $other_matched_wholesaler[0]->WholesalerId);
              }
            }
        }
        
        if( count($wholesaler_id) > 0 )
        {
          $response = DB::table('providers_wholesaler as pw')
                      ->join('users as u', 'pw.UserId', '=', 'u.Id')
                      ->select('pw.Status as WholesalerStatus', DB::raw('CONCAT(u.FirstName, " ", u.LastName) AS Name'), 'u.CellPhone', 'u.Email' , 'u.IsVerified',DB::raw(" '$isRM' AS isRM"))
                      ->whereIn('pw.UserId', $wholesaler_id)
                      ->orderByRaw("FIELD(WholesalerStatus , 'External', 'Internal') ASC")
                      ->get();
        }
        
        return $response;
        // return DB::table('users as u')
        //         ->join('advisors as a', 'a.UserId', '=', 'u.Id')
        //         ->join('advisor_companies as ac', 'ac.AdvisorId', '=', 'a.UserId')
        //         ->join('providers_wholesaler_zips as pz', 'u.ZipCode', '=', 'pz.ZipCode')
        //         ->join('providers_wholesaler as pw', 'pz.WholesalerId', '=', 'pw.UserId')
        //         ->join('providers as p', 'p.UserId', '=', 'pw.ProviderId')
        //         ->select('ac.Id as CompanyId', 'pw.UserId as WholesalerId', 'a.UserId as AdvisorId','u.ZipCode as AdvisorZipCode', 'pz.ZipCode as WholesalerZipCode', 'pw.Status as WholesalerStatus', DB::raw('(select CONCAT(FirstName, " ", LastName) from `users` WHERE Id = pw.UserId) AS Name'), DB::raw('(select email from `users` WHERE Id = pw.UserId) AS Email'), DB::raw('(select CellPhone from `users` WHERE Id = pw.UserId) AS CellPhone'))
        //         ->where('u.Id', $advisor_id)
        //         ->where('ac.Id', $company_id)
        //         ->where('pw.ProviderId', $provider_id)
        //         ->where('ac.Status', 1)
        //         ->first();
        //         // ->toSql();
    }
    public static function getCurrentFunds($advisorId,$companyId)
    {
      return Db::table('advisor_companies as ac')
                        ->join('fund_categories as fc', 'ac.Id', '=', 'fc.UserId')
                        ->where('ac.Id', $companyId)
                        ->where('fc.UserType', 'advisor_company')
                        ->where('ac.AdvisorId', $advisorId)
                        ->whereNull('fc.isDeleted')
                        ->count('fc.Id');

    }
}
