<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;
use App\Models\Question\QuestionAnswer;
use App\Models\User;

class AdvisorCompanyMetaData extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advisor_company_metadata'; 
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['AdvisorId',
                           'CompanyId',
                           'OpportunityKey',
                           'OpportunityValue',
                           'TotalFeeEstimate',
                           'AnnualSavings',
                           'SavingPerParticipant',
                           'PercentageDifference',
                           'CostStatus',
                           'AcceptableRangeStatus',
                           'BenchmarkingAverage',        
                           'CreatedBy',
                           'UpdatedBy'
                        ];

    /**
     * Validation Rules..
     */
    public static $companyCreate = [
                'CostStatus' => 'required',
                'PercentageDifference' => 'required',
                //'AcceptableRangeStatus' => 'required',
                'BenchmarkingAverage' => 'required|integer'
            ];


     /**
     * The Advisor company relationship.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'CompanyId', 'Id');
    }

    public static function updateReadKey($ids)
    {
        AdvisorCompanyMetaData::whereIn('Id',$ids)->update(['ReadStatus' => 1]);
    }

}