<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

use App\Models\TpaFlag;
use App\Models\AdvisorCompany;

use App\Traits\ModelCommon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Benchmark extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'benchmarks'; 
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted', 'CreatedDate', 'UpdatedDate'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $fillable = ['CompanyId', 'ProcessId', 'TotalIncomeAmount', 'NetAssetsAmount', 'PlanAssets', 'ParticipentCount', 'Compensation', 'Years', 'Percentage', 'AverageAccountBalancce', 'BrokerCompensationAmount', 'FiduciaryServiceCharges', 'ProductId', 'CostingInfo','ServiceModel', 'QuestionAnswers', 'FullPlanAssets'];

    /**
     * Benchmarks belongs to Company.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'CompanyId','Id');
    }

    /**
     * Benchmarks belongs to Campaigns .
     */
    public function campaigns()
    {
        return $this->belongsTo('App\Models\Proposal\Campaigns', 'CompanyId','CompanyId');
    }

    /**
     * Benchmarks has many benchmarkingResults against a ProcessId
    */
    public function benchmarkingResult()
    {
        return $this->hasMany('App\Models\BenchmarkingResults', 'CompanyId','CompanyId');
    }

    public function benchmarkDefaultResult()
    {
        return $this->hasMany('App\Models\BenchmarkDefaultResult', 'CompanyId', 'Id');
    }

    public static function insertBenchmarkingInfo($benchmark_data, $br_common_data)
    {
        $obj = AdvisorCompany::find($br_common_data->CompanyId);
        $arrQuestionAnswers = prepareQuestionAnswers($obj->questionAnswer()->get());
        $br_common_data->QuestionAnswers = serialize($arrQuestionAnswers);

        $benchmark_data->average_total_cost = $benchmark_data->avg;
        $br_common_data->CostingInfo = json_encode($benchmark_data);
        $tpaFlag = null;
        $objTpa = TpaFlag::where(['CompanyId' => $br_common_data->CompanyId])->first();
        if ($objTpa) {
            $tpaFlag = $objTpa->Flag;
        }
        $br_common_data->ServiceModel = $tpaFlag;

        self::create((array)$br_common_data);
    }

}
