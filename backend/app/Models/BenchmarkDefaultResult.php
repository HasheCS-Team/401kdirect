<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;

class BenchmarkDefaultResult extends Model
{
    //traits this model using...
    use ModelCommon;

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'benchmark_default_results';
    public $timestamps = false;
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider', 'ProviderId', 'Id');
    }

    /**
     * Benchmarking Results belongs to Company.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'CompanyId','Id');
    }

    /**
     * Benchmarking Results belongs to Campaigns .
     */
    // public function campaigns()
    // {
    //     return $this->belongsTo('App\Models\Proposal\Campaigns', 'ProcessId','Id');
    // }

    /**
     * Benchmarking Results belongs to Benchmarks .
     */
    public function benchmarks()
    {
        return $this->belongsTo('App\Models\Benchmark', 'CompanyId','CompanyId');
    }

    public static function insertBenchmarkingDefaultInfo($br_provider_data)
    {
        DB::table('benchmark_default_results')->insert( json_decode(json_encode($br_provider_data), true) );
    }

 }