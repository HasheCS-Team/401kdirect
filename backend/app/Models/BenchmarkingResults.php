<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\AdvisorCompany;
use App\Models\FundCategory;
use App\Models\InvestmentOption;

//traits
use App\Traits\ModelCommon;
use DB;

class BenchmarkingResults extends Model
{
    //traits this model using...
    //use SoftDeletes, ModelCommon;
    use ModelCommon;

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'benchmarking_results';
    public $timestamps = false;
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider', 'ProviderId', 'Id');
    }

    public static function getProviderIds($increamentalIds)
    {
       $providerIds = self::whereIn('id', $increamentalIds)
                             ->pluck('ProviderId')
                             ->toArray();
        return  $providerIds;                    

    }

    /**
     * Benchmarking Results belongs to Company.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'CompanyId','Id');
    }

    /**
     * Benchmarking Results belongs to Campaigns .
     */
    public function campaigns()
    {
        return $this->belongsTo('App\Models\Proposal\Campaigns', 'ProcessId','Id');
    }

    /**
     * Benchmarking Results belongs to Benchmarks .
     */
    public function benchmarks()
    {
        return $this->belongsTo('App\Models\Benchmark', 'CompanyId','CompanyId');
    }

    /**
     * returns broker product used against a specific provider and company
     *
     * @param $advisor_id
     * @param $company_id
     * @param $provider_id
     * @return int
     */
    public static function getUsedBrokerProduct($advisor_id, $company_id, $provider_id)
    {
        $bench_res = DB::table('benchmarking_results as br')
                        ->select('br.ProductId')
                        ->join('company_proposal_log as cpl', 'cpl.process_id', '=', 'br.ProcessId')
                        ->join('products as p', 'p.id', '=', 'br.ProductId')
                        ->where('br.AdvisorId', '=', $advisor_id)
                        ->where('br.CompanyId', '=', $company_id)
                        ->where('br.ProviderId', '=', $provider_id)
                        ->where('p.is_completed', '=', 1)
                        ->first();
        return ($bench_res) ? $bench_res->ProductId : 0;
    }

    public static function getUsedBrokerProductFee($advisor_id, $company_id)
    {
        $bench_res = DB::table('benchmarking_results as br')
                        ->select('br.FiduciaryServiceCharges')
                        ->join('company_proposal_log as cpl', 'cpl.process_id', '=', 'br.ProcessId')
                        ->where('br.AdvisorId', '=', $advisor_id)
                        ->where('br.CompanyId', '=', $company_id)
                        ->where('br.ProductId', '>', 0)
                        ->first();
        return ($bench_res) ? $bench_res->FiduciaryServiceCharges : 0;
    }

    public static function createBenchmarkResults($advisor_id, $company_id)
    {
        $company = AdvisorCompany::find($company_id);
        // Below is used to delete investment options forcefully
        $storedFundCategories = $company->fundCategories()->get();
        if ($storedFundCategories) { // This is used to delete store investment option
            $storedFundCategories = $storedFundCategories->toArray();
            foreach ($storedFundCategories as $row) {
              $objFundCategory = FundCategory::find($row['Id'])->investmentOptions();
              if ($objFundCategory)
                $objFundCategory->forceDelete();
            }
        }

        $company->fundCategories()->forceDelete();
        $fundcategoryData = $company->fundCategoriesCopyPaste()->with('investmentOptionsCopyPaste')->get();
        $arrFundCategories = $arrInvestmentOptions = $preparedInvestmentOptions = $arrUniqueIds = [];
        if ($fundcategoryData) {
          foreach ($fundcategoryData->toArray() as $key => $row) {
            $arrInvestmentOptionsTemp = $row['investment_options_copy_paste'];
            unset($row['investment_options_copy_paste']);
            $uniqueId = $row['Id'];
            unset($row['Id']);
            $row['temp_id'] = $uniqueId;
            $arrFundCategories[] = $row;
            $arrInvestmentOptions[$uniqueId] = $arrInvestmentOptionsTemp;
            $arrUniqueIds[] = $uniqueId;
          }
        }
        if (!empty($arrFundCategories)) {
            $company->fundCategories()->insert($arrFundCategories);
            $storedFundCategoriesData = FundCategory::whereIn('temp_id', $arrUniqueIds)->get()->toArray();
            foreach ($storedFundCategoriesData as $key => $row) {
                if (!empty($row['temp_id'])) {
                    $tempInvestmentOptions = $arrInvestmentOptions[$row['temp_id']];
                    $newFundCategoryId = $row['Id'];
                    array_walk($tempInvestmentOptions, function (&$t) use ($newFundCategoryId, &$preparedInvestmentOptions) {
                        unset($t['Id']);
                        $t['FundCategoryId'] = $newFundCategoryId;
                        $preparedInvestmentOptions[] = $t;
                    });
                }
            }
            if (!empty($preparedInvestmentOptions)) {
                InvestmentOption::insert($preparedInvestmentOptions);
            }
        }

        DB::statement("CALL sp_benchmarking($advisor_id, $company_id, @process_id, @total_max, @total_avg, @total_min, @provider_fee_cost_max, @provider_fee_cost_min, @provider_fee_cost_avg, @investment_cost_min, @investment_cost_max, @investment_cost_avg, @compensation_min, @compensation_max, @compensation_avg, @average, @minimum, @maximum, @pp_average, @pp_minimum, @pp_maximum)");

        $result = DB::select("SELECT @process_id as ProcessId, @total_max as total_max, @total_avg as total_avg, @total_min as total_min, @provider_fee_cost_max AS pfc_max, @provider_fee_cost_min AS pfc_min, @provider_fee_cost_avg AS pfc_avg, @investment_cost_min AS ic_min, @investment_cost_max AS ic_max, @investment_cost_avg AS ic_avg, @compensation_min AS c_min, @compensation_max AS c_max, @compensation_avg AS c_avg, @average AS avg, @minimum AS min, @maximum AS max, @pp_average AS pp_avg, @pp_minimum AS pp_min, @pp_maximum AS pp_max")[0];

        return $result;
    }

    public static function getBenchmarkData($company_id, $process_id)
    {
        return DB::table('benchmarking_results as br')
                    ->where('br.ProcessId', '=', $process_id)
                    ->where('br.CompanyId', '=', $company_id)
                    ->get();
    }

    public static function getBenchmarkCommonData($company_id, $process_id)
    {
        return DB::table('benchmarking_results as br')
                    ->select('br.CompanyId', 'br.ProcessId', 'br.TotalIncomeAmount', 'br.NetAssetsAmount', 'br.PlanAssets', 'br.ParticipentCount', 'br.Compensation', 'br.Years', 'br.Percentage', 'br.AverageAccountBalancce', 'br.BrokerCompensationAmount', 'br.FiduciaryServiceCharges', 'br.ProductId', 'br.FullPlanAssets')
                    ->where('br.ProcessId', '=', $process_id)
                    ->where('br.CompanyId', '=', $company_id)
                    ->first();
    }

    public static function getBenchmarkProviderData($company_id, $process_id)
    {
        return DB::table('benchmarking_results as br')
                    ->select("br.CompanyId" ,"br.ProcessId" ,"br.ProviderId" ,"br.ProviderName" ,"br.OneYear" ,"br.ThreeYear" ,"br.FiveYear" ,"br.TenYear" ,"br.GridPricing" ,"br.ServicePlanFee" ,"br.ServiceParticipantFee" ,"br.ServiceAssetBasedFee" ,"br.ProviderFeeCost" ,"br.ProviderFeeCostAmount" ,"br.TotalServiceCost" ,"br.InvestmentCost" ,"br.InvestmentCostAmount" ,"br.TargetDateInvestmentCost" ,"br.TotalCost" ,"br.TotalCostPerPart" ,"br.IsPriceHidden" ,"br.FeeScheduleId" ,"br.FeeScheduleType", "br.EstimatedTPAFee", "br.EstimatedTPAPercentage", "br.EstimatedThreeSixteenFee", "br.EstimatedThreeSixteenPercentage", "br.TpaFeeGrid", "br.ThreeSixteenFeeGrid" )
                    ->where('br.ProcessId', '=', $process_id)
                    ->where('br.CompanyId', '=', $company_id)
                    ->get();
    }

    /**
     * This is used to get benchmarking data without calling sp benchmarking
     *
     * @param $processId
     * @return mixed
     */
    public static function tempResetBenchmarking($processId)
    {
        $sql = "SELECT 
                  temptable.ProcessId,
                  temptable.TotalMax as total_max,
                  temptable.TotalAvg as total_avg,
                  temptable.TotalMin as total_min,
                  temptable.ProviderFeeCostMax as pfc_max,
                  temptable.ProviderFeeCostMin as pfc_min,
                  temptable.ProviderFeeCostAvg as pfc_avg,
                  temptable.InvestmentCostMax as ic_max,
                  temptable.InvestmentCostMin as ic_min,
                  temptable.InvestmentCostAvg as ic_avg,
                  temptable.CompensationMin as c_min,
                  temptable.CompensationMax as c_max,
                  temptable.CompensationAvg as c_avg,
                  temptable.atc as avg,
                  temptable.ap as pp_avg,
                  temptable.mitc as min,
                  temptable.mip as pp_min,
                  temptable.mxtc as max,
                  temptable.mxp as pp_max
                FROM
                (SELECT 
                    '$processId' as ProcessId,
                    (pfcMAX + icMAX + pMAX) AS TotalMax,
                    (pfcAVG + icAVG + pAVG) AS TotalAvg,
                    (pfcMIN + icMIN + pMIN) AS TotalMin,
                    ProviderFeeCostMax,
                    ProviderFeeCostMin,
                    ProviderFeeCostAvg,
                    InvestmentCostMax,
                    InvestmentCostMin,
                    InvestmentCostAvg,
                    CompensationMin,
                    CompensationMax,
                    CompensationAvg,
                    atc,
                    ap,
                    mitc,
                    mip,
                    mxtc,
                    mxp
                FROM
                    (SELECT 
                      ProviderFeeCost AS pfcMAX,
                      CONCAT(
                        ROUND(ProviderFeeCost, 2),
                        '% ($',
                        FORMAT(ProviderFeeCostAmount, 0),
                        ')'
                      ) AS ProviderFeeCostMax,
                      InvestmentCost AS icMAX,
                      CONCAT(
                        ROUND(InvestmentCost, 2),
                        '% ($',
                        FORMAT(InvestmentCostAmount, 0),
                        ')'
                      ) AS InvestmentCostMax,
                      Percentage AS pMAX,
                      CONCAT(
                        ROUND(Percentage, 2),
                        '% ($',
                        FORMAT(BrokerCompensationAmount, 0),
                        ')'
                      ) AS CompensationMax 
                    FROM
                      benchmarking_results 
                    WHERE TotalCost IS NOT NULL 
                      AND ProcessId = '$processId' 
                      AND GridPricing <> 'CUSTOMED' 
                      AND GridPricing <> 'NO MATCH' 
                      AND InvestmentCostAmount > 0 
                    ORDER BY TotalCost DESC 
                    LIMIT 1) AS t1 
                    
                    CROSS JOIN
                    
                    (SELECT 
                      ProviderFeeCost AS pfcMIN,
                      CONCAT(
                        ROUND(ProviderFeeCost, 2),
                        '% ($',
                        FORMAT(ProviderFeeCostAmount, 0),
                        ')'
                      ) AS ProviderFeeCostMin,
                      InvestmentCost AS icMIN,
                      CONCAT(
                        ROUND(InvestmentCost, 2),
                        '% ($',
                        FORMAT(InvestmentCostAmount, 0),
                        ')'
                      ) AS InvestmentCostMin,
                      Percentage AS pMin,
                      CONCAT(
                        ROUND(Percentage, 2),
                        '% ($',
                        FORMAT(BrokerCompensationAmount, 0),
                        ')'
                      ) AS CompensationMin 
                    FROM
                      benchmarking_results 
                    WHERE TotalCost IS NOT NULL 
                      AND ProcessId = '$processId' 
                      AND GridPricing <> 'CUSTOMED' 
                      AND InvestmentCostAmount > 0 
                      AND GridPricing <> 'NO MATCH' 
                    ORDER BY TotalCost ASC 
                    LIMIT 1) AS t2 
                    
                    CROSS JOIN
                    
                    (SELECT 
                      AVG(TotalCost),
                      AVG(ProviderFeeCost) AS pfcAVG,
                      CONCAT(
                        ROUND(AVG(ProviderFeeCost), 2),
                        '% ($',
                        FORMAT(AVG(ProviderFeeCostAmount), 0),
                        ')'
                      ) AS ProviderFeeCostAvg,
                      AVG(InvestmentCost) AS icAVG,
                      CONCAT(
                        ROUND(AVG(InvestmentCost), 2),
                        '% ($',
                        FORMAT(AVG(InvestmentCostAmount), 0),
                        ')'
                      ) AS InvestmentCostAvg,
                      AVG(Percentage) AS pAVG,
                      CONCAT(
                        ROUND(AVG(Percentage), 2),
                        '% ($',
                        FORMAT(AVG(BrokerCompensationAmount), 0),
                        ')'
                      ) AS CompensationAvg 
                    FROM
                      benchmarking_results 
                    WHERE TotalCost IS NOT NULL 
                      AND ProcessId = '$processId' 
                      AND InvestmentCostAmount > 0 
                      AND GridPricing <> 'CUSTOMED' 
                      AND GridPricing <> 'NO MATCH') AS t3 
                    
                    CROSS JOIN
                    
                    (SELECT 
                      AVG(TotalCost) atc,
                      AVG(TotalCost) / ParticipentCount AS ap,
                      MIN(TotalCost) AS mitc,
                      MIN(TotalCost) / ParticipentCount AS mip,
                      MAX(TotalCost) AS mxtc,
                      MAX(TotalCost) / ParticipentCount AS mxp 
                    FROM
                      benchmarking_results 
                    WHERE ProcessId = '$processId' 
                      AND GridPricing <> 'CUSTOMED' 
                      AND InvestmentCostAmount > 0 
                      AND GridPricing <> 'NO MATCH') AS t4
               ) as temptable";

        $data = DB::select(DB::raw($sql));

        return $data;
    }

 }