<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Broker extends Model
{
    protected $table = 'brokers';

    protected $fillable = ['user_id', 'firm_name', 'logo', 'rpag_score'];

    /**
     * This is used to get broker dealers for drop down
     *
     * @param $search
     * @return mixed
     */
    public static function getBrokerDealers($search)
    {
        $search = '%'.$search.'%';

        return self::select('Id','firm_name as Name')->where('firm_name', 'Like', $search)->groupBy('firm_name')->get();
    }

    /**
     * This is broker providers relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function providers()
    {
        return $this->belongsToMany('App\Models\Users\Provider','broker_provider', 'BrokerDealerId', 'ProviderId')->withPivot('BrokerDealerId', 'ProviderId');
    }

    /**
     * This is used to get stats for broker dealer
     *
     * @param $params
     *     userId
     *     brokerDealerId
     * @return bool|mixed
     *
     * array:3 [
            "countProducts" => 1
            "countSalesDesk" => 3
            "countAssignProviders" => 3
        ]
     */
    public static function getBrokerStats($params)
    {
        $userId = $params['userId'];
        $brokerDealerId = $params['brokerDealerId'];
        $sql = <<<SQL
                    SELECT
                    (SELECT COUNT(p.id) FROM products AS p WHERE broker_dealer_id = ? AND is_completed = 1 AND deleted_at IS NULL) AS countProducts,
                    (SELECT COUNT(s.id) FROM sale_desks AS s WHERE s.broker_id = ?) AS countSalesDesk,
                    (SELECT COUNT(bp.Id) FROM broker_provider AS bp WHERE bp.BrokerDealerId = ? AND IsUser = 1) AS countAssignProviders
SQL;

        $db = getDB();

        return $db->fetchArray($sql, [$userId, $brokerDealerId, $brokerDealerId]);

    }

}
