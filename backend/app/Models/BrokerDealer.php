<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;

class BrokerDealer extends Model
{
    //traits this model using...
    //use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'collection_broker_dealer'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    //const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    //const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    //const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['IsDeleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name'];

    /**
     * The Fund Category Belongs Provider.
     */
    public $timestamps = false;


    //show broker values
    public function scopeDropdown($query, $search)
    {
        $query->select(['Id','Name'])->where('Name', 'LIKE', "%$search%");
    }

    /**
     * This is used to get firm names
     *
     * @param $params
     * @return mixed
     */
    public static function getFirmNames($params)
    {
        $search = $params['search'];

        return DB::table("collection_broker_dealer as cbd")
            ->selectRaw('cbd.Id, cbd.Name, (SELECT count(*) FROM users WHERE BrokerDealer LIKE cbd.Name AND AccountType="broker-dealer") AS brokerCount')
            ->where('cbd.Name', 'LIKE', "%$search%")
            ->having('brokerCount', '=', 0);
    }
    //show brokers which already not selected
    public function scopedropdownBrokerDealer($query, $search)
    {
        $query->from("collection_broker_dealer as cbd")->selectRaw('cbd.Id, cbd.Name, (SELECT count(*) FROM users WHERE BrokerDealer=cbd.Name AND AccountType="broker-dealer") AS brokerCount')->where('cbd.Name', 'LIKE', "%$search%")->having('brokerCount', '=', 0);
    }

}