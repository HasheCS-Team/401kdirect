<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;


class BrokerProvider extends Model
{
    protected $table = 'broker_provider'; 

    protected $primaryKey = 'Id';
     
    protected $fillable = ['BrokerDealerId','ProviderId','IsUser'];

    const CREATED_AT = 'CreatedDate';

    const UPDATED_AT = 'UpdatedDate';

}