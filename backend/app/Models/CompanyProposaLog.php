<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\FundCategoryProposal;
use App\Models\AdvisorCompany;

class CompanyProposaLog extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_proposal_log';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';


    public static function clearCompanyProposalLog($company_id, $advisor_id)
    {
        $log = self::where(['advisor_id' => $advisor_id,'company_id' => $company_id])->first(); 
       
        if ( !$log )
        {
            return ["status" => "error","message" => "companyProposalNotExits" ];  
        }

        if ( self::where(['advisor_id' => $advisor_id,'company_id' => $company_id])->delete() )
        {
            AdvisorCompany::find($company_id)->update(['ProviderId' => null]);
            
            return ["status" => "success","message" => "companyProposalDeletedSuccess" ];  
        }

        return ["status" => "error","message" => "companyProposalDeletedError" ];
    }

    public static function saveCompanyLog($company_id,$advisor_id,$providersIncreamentalIds){
                   
        $providersIncreamentalIds = implode(",", $providersIncreamentalIds);

        $log = self::where(['advisor_id' => $advisor_id,'company_id' => $company_id])->first();

        if ( !$log )
        {

            $log = new CompanyProposaLog();
            $log->provider_increamental_ids = $providersIncreamentalIds;
            $log->advisor_id = $advisor_id;
            $log->company_id = $company_id;

            if ( $log->save() )
            {
                return ["status" => "success","message" => "companyProposalLogSaveSucces" ];

            }
            return ["status" => "error","message" => "companyProposalLogSaveError" ];
        }
        
        $log->provider_increamental_ids = $providersIncreamentalIds;
        
        if ( $log->update() )
        {
            return ["status" => "success","message" => "companyProposalLogUpdateSucces" ];
        }

        return ["status" => "error","message" => "someProblem"];
    }

    public static function updateProposalLog($company_id, $advisor_id, $incremental_id,$provider_id)
    {
        $log = self::where(['advisor_id' => $advisor_id, 'company_id' => $company_id])->first();

        if ( $log )
        {
            
            $log_value = implode(',', array_diff(explode(',', $log->provider_increamental_ids), [$incremental_id]));
            $log_draggedIds = implode(',', array_diff(explode(',', $log->draggedProviderIds), [$provider_id]));

            $log->provider_increamental_ids = $log_value;
            $log->draggedProviderIds = $log_draggedIds ;
            $log->save();
        }
        return true;
    }


    public static function includeCurrentProvider($company_id, $advisor_id, $current_provider_id,$current_provider_status)
    {
        
        $log = self::where(['advisor_id' => $advisor_id, 'company_id' => $company_id])->first();
        
        if ( $log )
        {
           // dd(explode(',', $log->draggedProviderIds));
            if(trim($log->draggedProviderIds) == '' || $log->draggedProviderIds == null){
                 $log->draggedProviderIds = $current_provider_id.""; 
            }else
            {
                $tempProviderIds = array_diff(explode(',', $log->draggedProviderIds), [$current_provider_id]);
                if(count($tempProviderIds) == 5)
                {
                   $tempProviderIds[4] = $current_provider_id;     
                }else{
                    array_push($tempProviderIds,$current_provider_id);  
                }
                $log->draggedProviderIds = implode(',' , $tempProviderIds );
            }
        }
        else
        {
          $log = new CompanyProposaLog();
          $log->advisor_id  = $advisor_id;
          $log->company_id  = $company_id;
          $log->draggedProviderIds = $current_provider_id."";
        }

        $log->currentProvider = $current_provider_status;
        $log->save();


        return true;
    }


    public static function excludeCurrentProvider($company_id, $advisor_id, $current_provider_id,$current_provider_status)
    {
        
        $log = self::where(['advisor_id' => $advisor_id, 'company_id' => $company_id])->first();

        if ( $log )
        {
           
            $tempProviderIds = array_diff(explode(',', $log->draggedProviderIds), [$current_provider_id]);            
               
            
            if(count($tempProviderIds) == 0)
            {
              $log_draggedIds = null; 
            
            }else{
                       
                $log_draggedIds = implode( $tempProviderIds, ',');
                  
            }

            $log->draggedProviderIds = $log_draggedIds;
            $log->currentProvider = $current_provider_status;
            $log->save();
        }
        return true;
    }

    public static function updateAddedProvider($company_id, $advisor_id, $provider_id)
    {
         $log = self::where(['advisor_id' => $advisor_id, 'company_id' => $company_id])->first();

        if ( $log )
        {
            if ( $log->draggedProviderIds == '' || $log->draggedProviderIds == null){

                $log->draggedProviderIds = "".$provider_id;
            }else{
                
                $log->draggedProviderIds .= ",".$provider_id;
            }

            $log->save();
        }
        return true;
    }

    public static function setIncIdsNull($company_id, $advisor_id)
    {
        self::where(['advisor_id' => $advisor_id,'company_id' => $company_id])->update(['provider_increamental_ids'=>null]);        
        return true;
    }

    public static function getSaveProviderIds($advisorId, $companyId)
    {
       $logProviderIds = self::select('provider_increamental_ids', 'draggedProviderIds', 'sortedScheme', 'currentProvider', 'process_id')
                             ->where(['advisor_id' => $advisorId, 'company_id' => $companyId])
                             ->first();
        return $logProviderIds;
    }

    public static function saveDraggedPids($advisorId,$companyId,$draggedPids,$incremental_id)
    {

       $log = self::where(['advisor_id' => $advisorId,'company_id' => $companyId])->first();
    
        if ( !$log )
        {
            $log = new CompanyProposaLog();
            $log->provider_increamental_ids = $incremental_id;           
            $log->draggedProviderIds = $draggedPids;
            $log->advisor_id = $advisorId;
            $log->company_id = $companyId;

            if ( $log->save() )
            {
                return 1; //Successfully updated
            }
            return -1; // Not succesfully updated
        }

        if($log->where(['advisor_id' => $advisorId,'company_id' => $companyId])->update(['draggedProviderIds'=>$draggedPids,'provider_increamental_ids'=>$incremental_id]))
        {
            return 2; // succesfully updated...
        }
       return 0;

        return -2; // not succesfully updated...
    }

    public static function updateDraggedIds($advisor_id,$company_id,$draggedIds){
        $log = self::where(['advisor_id' => $advisor_id,'company_id' => $company_id])->first();
    
        if ( $log )
        {
             $log->draggedProviderIds = $draggedIds;
             $log->save();
        }
       
    }

    public static function getSaveProviderIncrementalIds($advisorId, $companyId)
    {
       return self::select('provider_increamental_ids')
                    ->where(['advisor_id' => $advisorId, 'company_id' => $companyId])
                    ->first();
    }
}
