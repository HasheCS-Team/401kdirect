<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;

class CompanyProvider extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_provider'; 
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['AdvisorCompanyId',
                           'ProviderName',
                           'TotalCost',
                           'TotalPaid',
                           'HardDollarAmount',
                           'PlanName',
                           'PlanAssets',
                           'AnnualContributions',
                           'PlanParticipantBalance',
                           'CreatedBy',
                           'UpdatedBy'
                        ];

    public static $companyProviderCreate = [
                'ProviderName' => 'max:100',
                'TotalCost' => 'required|numeric',
                'TotalPaid' => 'required|numeric',
                'HardDollarAmount' => 'required|integer',
                'PlanName' => 'required|max:100',
                'PlanAssets' => 'required|integer',
                'AnnualContributions' => 'required|integer',
                'PlanParticipantBalance' => 'required|integer',
            ];

    /**
     * The Advisor that belong to the User.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'AdvisorCompanyId','Id');
    } 
}