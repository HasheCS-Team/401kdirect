<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class CompanySteps extends Model
{
    //traits this model using...
    use ModelCommon;

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_steps';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';


 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'AdvisorCompanyId', 'Status', 'StepNumber', 'CreatedBy', 'UpdatedBy'
                          ];

    
    public static function GenerateSteps()
    {
         $arr = [];
        foreach( range(1,8) as $n )
        {
            array_push($arr, ['StepNumber' => $n, 'Status' => false]);
        }

        return $arr;
    }

    /*
     * Front-End
     * Check Porgress Bar Again Company Steps
     */
    public function scopeProgressBarSteps($query, $advisorCompanyId)
    {
        return $query->select(['StepNumber'])
                    ->where('AdvisorCompanyId',$advisorCompanyId)
                    ->where('Status',1);
    }

    /*
     * Front End
     * Check Step Number and 
     */
    public function scopeCheckStepNumber($query, $advisor_id)
    {
        // return $query->select(['StepNumber','AdvisorCompanyId','Status'])
        //             ->where('StepNumber',5)
        //             ->where('Status',1);
        return \DB::table('advisor_companies as ac')
                ->select('cqa.OptionId','cqa.QuestionId', 'cqa.CompanyId')
                ->join('company_question_answers as cqa', 'ac.Id', '=', 'cqa.CompanyId')
                ->where('ac.AdvisorId', $advisor_id)
                ->where('cqa.QuestionId', 2)
                ->where('cqa.OptionId', 3);;
    }

    /**
     * Company Steps belongs to Advisor Company.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'AdvisorCompanyId','Id');
    }                  
  
}