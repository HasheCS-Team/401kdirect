<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Config;
use File;

//traits
use App\Traits\ModelCommon;

class ContactUs extends Authenticatable
{
    //traits this model using...
    use SoftDeletes, ModelCommon;
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';
    

    //validation rules for contact-us...
    public static $contactUsRules = [
        'Email' => 'required|email|email_verification',
        'Role' => 'required',
        'Message' => 'required',
        'captchaKey' => 'required'
    ];

}
