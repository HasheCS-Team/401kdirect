<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use App\Traits\ModelCommon;

use DB;

class DefaultFundCategory extends Model
{
	use ModelCommon;

    protected $table = 'collection_default_mapping';
    protected $fillable = ['FundCategoryName','AssetPer', 'Status'];

    public static function scopeDataTable($query)
  	{    
  	    $query->select('Id','FundCategoryName', DB::raw("CONCAT(AssetPer,'','%') AS AssetPer"))
  	    ->where('Status','Default')
  	    ->orderBy('FundCategoryName','asc');
  	}

    public function scopeFundCategory($query)
    {
        $query->select(DB::raw('LOWER(FundCategoryName) AS fund_category'))->groupBy('FundCategoryName');
    }

    public static function checkDefaultCategory($fund_category_name)
    {
        return self::where('FundCategoryName', strtolower($fund_category_name))->where('Status', 'Default')->first();
    }

    public static function getDefaultFundsList(){
      return DB::table('collection_default_mapping')->select('Id', DB::raw(('lower(FundCategoryName) as fund_category')))->where('Status', 'Default')->orderBy('fund_category', 'asc')->groupBy('FundCategoryName')->get();
    }

    // public static function getMissingDefaultFundsList(){
    //   return DB::table('collection_default_mapping')->select(DB::raw(('lower(FundCategoryName) as fund_category')))->where('Status', 'Default')->orderBy('fund_category', 'asc')->get();
    // }
    /**
     * This is used to get fund categories data
     *
     * @param $params
     * @return mixed
     */
    public static function getDefaultFundCategories($params)
    {
        $sql = self::select('collection_default_mapping.*');
        if (!empty($params['ids'])) {
            $sql->whereIn('collection_default_mapping.Id', $params['ids']);
        }
        if (!empty($params['productId'])) {
            $sql->addSelect(
                'io.QuarterUpdatedDate',
                'io.DefaultForProvider as DefaultInvestment',
                'io.QuarterUpdatedDate',
                DB::raw('count(io.id) as count'),
                DB::raw("DATE_FORMAT(io.CreatedDate, '%m/%d/%Y') as CreatedDate"),
                DB::raw("DATE_FORMAT(io.UpdatedDate, '%m/%d/%Y') as UpdatedDate"));
            $sql->leftjoin('fund_categories as fc', function ($join) use ($params) {
                $join->on('fc.Name', '=', 'collection_default_mapping.FundCategoryName');
                $join->where('fc.UserId', '=', $params['productId']);
                $join->where('fc.UserType', '=', 'broker_dealer_product');
                $join->where('collection_default_mapping.Status', '=', 'Default');
            });
            $sql->leftjoin('investment_options as io', function ($join) use ($params) {
                $join->on('io.FundCategoryId', '=', 'fc.Id');
                $join->where('io.ProductId', '=', $params['productId']);
                $join->where('io.ProviderId', '=', $params['providerId']);
            });
        }

        return $sql->groupBy('FundCategoryName')->where('Status', '=', 'Default')->get()->toArray();
    }
    
}