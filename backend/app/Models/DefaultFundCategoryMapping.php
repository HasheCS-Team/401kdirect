<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use App\Traits\ModelCommon;

use DB;

class DefaultFundCategoryMapping extends Model
{
	use ModelCommon;

    protected $table = 'collection_default_mapping_funds';
    protected $fillable = ['DefaultId','FundName'];

    public $timestamps = false;

    public static function defaultFundStatus ($id)
    {
        $arr = [ 
            'FundName' => 'required|unique:collection_default_mapping_funds|min:3|max:50',
            'DefaultId' => 'required|exists:collection_default_mapping,Id|default_fund_status:'."$id"
          ];
        return $arr;
    }

    public static function DataTable($id)
  	{ 
  	    $response = self::select('Id','DefaultId','FundName')
        ->where('DefaultId',$id)
        ->orderBy('FundName','asc')
        ->get();
  	    return $response;
  	}

  	public function scopeEdit($query)
    {   
        $query->select('FundName');
    }

    public static function UpdateMappedFund($id,$data)
    {
      $response = DB::table('collection_default_mapping_funds')->where('Id',$id)->update(['FundName' => $data]);
      return $response;
    }

}