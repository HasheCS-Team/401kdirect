<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;

use DB;

class EmailChangeRequest extends Model
{
    use ModelCommon;

    protected $table = 'change_email_history';

    const CREATED_AT = 'CreatedDate';
    const UPDATED_AT = 'UpdatedDate';

    protected $fillable = ['Id','UserID','PreviousEmail','RequestedEmail','Comment','Status','AccountType'];

    public $timestamps = true;

  	public static function scopeDataTable($query)
  	{    
  	    $query->select('Id','UserID', 'PreviousEmail', 'RequestedEmail', 'AccountType')->where('Status' , 'requested');
  	}

    public static function emailChangeRequestIDValidation($id)
    {
        $arr = [
            'Id' => 'required|exists:change_email_history,Id|email_change_request_id:'. "$id"
        ];

        return $arr;
    }
    public static $commentValidation = [ 
        'Comment' => 'max:300',
       ]; 
    
    public static function disapproveEmailChange($id ,$Comment)
    {
        if($Comment == '')
          {
            $Comment = 'N/A';
          }
      
        return DB::table('change_email_history')
                 ->where('Id',$id)
                 ->update(['Comment' => $Comment, 'Status' => 'disapprove']);
      
    }

    public static function getRequestedData($id)
    {
       $requestedData = DB::table('change_email_history')
                       ->select('RequestedEmail','PreviousEmail','UserId')
                       ->where('Id',$id)
                       ->first();

        return $requestedData;
    }

    public static function updateEmailChange($id,$data)
    {
       $response = DB::table('users')->where('Id',$data->UserId)->update(['Email' => $data->RequestedEmail]);

       if($response)
       {
         $requestStatus = DB::table('change_email_history')
                        ->where('Id',$id)
                        ->update(['Status' =>'approve']);

        return $requestStatus;
       }
    }

    // public static function statusApprove($id)
    // {
    //   $requestStatus = DB::table('change_email_history')
    //                     ->where('Id',$id)
    //                     ->update(['Status' =>'approve']);
    //   return $requestStatus;
    // }

    // public static function getRequestedEmail($id)
    // {
    //   $data = DB::table('change_email_history')->select('RequestedEmail')->where('Id',$id)->first();
    //   return $data ? $data->RequestedEmail : false;
    // }

    // public static function getPreviousEmail($id)
    // {
    //   $data = DB::table('change_email_history')->select('PreviousEmail')->where('Id',$id)->first();
    //   return $data ? $data->PreviousEmail : false;
    // }

    public static function getComment($id)
    {
      $data = DB::table('change_email_history')
                  ->select('Comment')
                  ->where('Id',$id)
                  ->first();
      return $data ? $data->Comment : false;
    }

    public static function getRequestStatus($id)
    {
      $data = DB::table('change_email_history')
                  ->select('Status')
                  ->where('UserID',$id)
                  ->orderBy('Id','desc')
                  ->first();

      return $data ? $data->Status : false; 
    }

    public static function getUserName($id)
    {
        $user_id =DB::table('change_email_history')
            ->select('UserId')
            ->where('Id',$id)
            ->first();

        if($user_id)
        {
            $data = DB::table('users')
                ->where('Id',$user_id->UserId)
                ->first();
        }
        return $data ? $data : false;
    }
}