<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class EmailTemplate extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_template'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The name of the "created by" column.
     *
     * @var string
     */
    // /const CREATED_BY = 'CreateBy';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];

    
    public static $emailTemplateRules = [

        'Title' => 'required|unique:email_template,Title|max:100',
        'Subject' => 'required|max:100'
    ];


    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Title', 'Subject', 'Body', 'IsActive' ,'CreatedBy','UpdatedBy','EmailFrom','EmailTo','EmailDescription'];

    
    //Mutators for IsActive Attribute
    public function setIsActiveAttribute($value)
    {
        $this->attributes['IsActive'] = ($value == "true" ? 1 : 0 );
    }

    //datatables for email template
    public static function scopeDataTable($query)
    {
        
        $query->select(['Id', 'Title', 'Subject', 'IsActive' ,'CreatedDate','UpdatedDate','EmailFrom','EmailTo','EmailDescription'])->orderBy('id', 'DESC');
    }

    //edit atttributes
    public function scopeEdit($query)
    {
       
        $query->select(['Id', 'Title','Body','Subject', 'IsActive','EmailFrom','EmailTo','EmailDescription']);
    }
    public static function TemplateUpdatedByAdmin($id = null)
    {
        if ( $id )
        {
            // $arr['Title'] = 'required|min:1|unique:email_template,Title,'. $id;
            $arr['Subject'] = 'required|min:1|max:100';
            return $arr;
        }
    }



    /**
     * The Fund Category Belongs Provider.
     */
    //public $timestamps = false;
   
}