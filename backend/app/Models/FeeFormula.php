<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;

class FeeFormula extends Model
{
    //traits this model using...
    //use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provider_fee_assets';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    //const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    //const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    //const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['IsDeleted'];





    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ProviderId', 'FeeScheduleId',  'AssetsStart', 'AssetsEnd', 'BalanceStart', 'BalanceEnd', 'PercentValue'];


    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider');
    }


    /**
     * The Fund Category Belongs Provider.
     */
    public $timestamps = false;

    public static $ADD_RULE_ROW = [
        'data.ProviderId'       => 'required|integer',
        'data.AssetEndValue'    => 'required',
        'data.AssetStart'       => 'required',
        'data.AssetEnd'         => 'required'
    ];
    public static $ADD_RULE_COLUMN = [
        'data.ProviderId'       => 'required|integer',
        'data.BalanceEndValue'  => 'required',
        'data.AABStart'         => 'required',
        'data.AABEnd'         => 'required'
    ];

    public function next(){
        // get next user
        return DB::table('provider_fee_assets')->where('AssetsStart', '>', $this->AssetsStart)->where('AssetsEnd', '>', $this->AssetsEnd)->where('ProviderId', $this->ProviderId)->where('FeeScheduleId', $this->FeeScheduleId)->orderBy(
            'Id','asc')->first();
    }
    
    public  function previous(){
        // get previous  user
        return DB::table('provider_fee_assets')->where('AssetsStart', '<', $this->AssetsStart)->where('AssetsEnd', '<', $this->AssetsEnd)->where('ProviderId', $this->ProviderId)->where('FeeScheduleId', $this->FeeScheduleId)->orderBy(
            'Id','desc')->first();
        //return DB::table('provider_fee_assets')->where('Id', '<', $this->Id)->orderBy('Id','desc')->first();
    }
}
