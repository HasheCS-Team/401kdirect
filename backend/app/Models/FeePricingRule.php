<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class FeePricingRule extends Model
{
    //traits this model using...
    //use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provider_fee_pricing_rules'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    //const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    //const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    //const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['IsDeleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ProviderId', 'FeeScheduleId', 'Priority', 'AssetsStart', 'AssetsEnd', 'AverageBalanceStart', 'AverageBalanceEnd', 'ParticipantCountStart', 'ParticipantCountEnd', 'AssetBaseFee', 'PlanFee', 'ParticipantFee', 'IsActive', 'isAssetBaseFee', 'isParticipantFee', 'isPlanFee'];

    // Validation rules
    public static function validationRules($requestData, $providerId){
        return $rules = [
            'AssetsStart'           	=> "integer",
            'AssetsEnd'             	=> "integer|min:".($requestData['AssetsStart']!= 0?$requestData['AssetsStart']+1:0),
            'AverageBalanceStart'   	=> "integer",
            'AverageBalanceEnd'     	=> "integer|min:".($requestData['AverageBalanceStart']!=0?$requestData['AverageBalanceStart']+1:0),
            'ParticipantCountStart'  	=> "integer",
            'ParticipantCountEnd'     	=> "integer|min:".($requestData['ParticipantCountStart']!=0?$requestData['ParticipantCountStart']+1:0),
            'ProviderId'            	=> "required|integer|min:0",
            // 'Priority' 					=> 'required|integer|min:0|unique:provider_fee_pricing_rules,Priority,'.$this->request->input('Id').',Id,ProviderId,'.$ProviderId		
        ];
    }

    // Check Duplication row
    public static function checkPricingDuplication($requestData, $providerId, $feeScheduleId) {
        
        return self::where([
            'AssetsStart'=> $requestData['AssetsStart'],
            'AssetsEnd'=> $requestData['AssetsEnd'],
            'AverageBalanceStart'=> $requestData['AverageBalanceStart'],
            'AverageBalanceEnd' => $requestData['AverageBalanceEnd'],
            'ParticipantCountStart'=> $requestData['ParticipantCountStart'],
            'ParticipantCountEnd'=> $requestData['ParticipantCountEnd'],
            'PlanFee'=> $requestData['PlanFee'],
            'ParticipantFee'=> $requestData['ParticipantFee'],
            'AssetBaseFee'=> $requestData['AssetBaseFee']
        ])->where(['ProviderId' => $providerId, 'FeeScheduleId' => $feeScheduleId])->count();

    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider');
    }

    public function setIsAverageBalanceStartAttribute($value)
    {
        $this->attributes['AverageBalanceStart'] = ($value == "" ? null : $value );
    }

    public function setIsAverageBalanceEndAttribute($value)
    {
        $this->attributes['AverageBalanceEnd'] = ($value == "" ? null : $value );
    }

    public $timestamps = false;
    
}