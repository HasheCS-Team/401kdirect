<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;
use DB;

//traits
use App\Traits\ModelCommon;

class FundCategory extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fund_categories'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'UserId', 'AssetsValue', 'PlanPercentage', 'WeightedExpense', 'UserType', 'street_user_id', 'RjFlag', 'PlanIdExcel', 'DefaultCheck', 'CreatedBy', 'UpdatedBy'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     /**
     * Validation for create and update.
     *
     * @var array
     */
      /**
     * Validation for create and update.
     *
     * @var array
     */

    public static function fundCategoryCreate($providerId, $funId = null)
    {
        $arr['UserId'] = 'required';
        $arr['DefaultCheck'] = 'required';
        $arr['Name'] = "required|unique:fund_categories,Name,null,Id,UserId,$providerId,UserType,provider";

        if ( $funId )
        {
            $arr['Name'] = "required|unique:fund_categories,Name,$funId,Id,UserId,$providerId,UserType,provider";
        }

        return $arr; 
    }  

    public static function companyFundCategoryCreate($companyId, $Id = null)
    {
        // $arr['UserId'] = 'required';
        $arr['Name'] = "required|unique:fund_categories,Name,null,Id,UserId,$companyId,UserType,advisor_company";

        // if ( $Id )
        // {
        //     $arr['Name'] = "required|unique:fund_categories,Name,$Id,Id,UserId,$companyId,UserType,advisor_company";
        // }

        return $arr; 
    }  

    //excel file upload validation....
    public static $investmentOptionRules = ['fund_category',
                                            'investment_options',
                                            //'oneyear',
                                            //'threeyear',
                                            //'fiveyear',
                                            //'tenyear',
                                            'expense',
                                            'ticker',
                                            'default_for_provider',
                                        ];

    public function scopePrimaryKey($query, $name, $ProviderId)
    {
        $query->select($this->primaryKey)->where('Name', $name)->where('UserType', 'provider')->where('UserId', $ProviderId);
    }

    public function scopeEdit($query)
    {
        $query->select(['Name']);
    }

    /**
     * Get all of the owning user models.
     */
    public function userType()
    {
        return $this->morphTo(null, 'UserType', 'UserId');
    }

    /**
     * The Fund Category Belongs to Provider.
     */
    // public function provider()
    // {
    //     return $this->belongsTo('App\Models\Users\Provider', 'UserId');
    // }

    /**
     * The Fund Category may have many Investment Options for a Provider.
    */
    public function investmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOption', 'FundCategoryId', 'Id')
                    ->select(['Id', 'FundCategoryId', 'Name', 'OneYear', 'ThreeYear', 'FiveYear', 'Expense', 'TenYear', 'DefaultForProvider', 'QuarterUpdatedDate', 'QuarterVersion', 'Ticker','Score'])->orderBy('Name', 'asc');
    }

    /**
     * The Fund Category may have many Investment Options for a Provider.
    */
    public function frontInvestmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOption', 'FundCategoryId', 'Id')
                    ->select(['Id', 'FundCategoryId', 'Name', 'Expense', 'Assets']);
    }

    /**
     * front end side, will show relevent in dropdown box.. on fundcategory's investment page...
     */
    public function dropdownInvestmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOption', 'FundCategoryId', 'Id')
                    ->select(['Id', 'Name as option_name', 'Score']);
    }


    public static function updateFundcategoriesUserTypeOld($userId)  {
        self::where(['UserId'=>$userId,'UserType'=>'advisor_company'])
        ->update(['UserType' => 'advisor_company_old']);
    }


    public  function fcInvestmentOptions()
    {
        
        return $this->hasMany('App\Models\InvestmentOption', 'investment_options.FundCategoryId', 'Id')
                    ->select([
                              'fund_categories.Id as fcId',
                              'investment_options.Id as ioId',
                              'investment_options.Name as option_name',
                              'fund_categories.Name as fund_category'
                             ]);            
    }

    public static function getProviderFund($fund_id)
    {
        return self::where(['Id' => $fund_id, 'UserType' => 'Provider'])->first();
    }

    public static function getProductFund($fund_id)
    {
        return self::where(['Id' => $fund_id, 'UserType' => 'broker_dealer_product'])->first();
    }

    /**
     * This is used to get fund category id
     *
     * @param $fundCategory
     * @param $id
     * @param int $productId
     * @param string $userType
     * @return int
     */
//    public static function getFundCategory($fundCategory, $id, $productId = 0, $userType = 'Provider')
//    {
//        $sql = self::where('Name', '=', $fundCategory);
//        if (!empty($productId)) {
//            $sql->where('UserId', '=', $productId);
//        } else {
//            $sql->where('UserId', '=', $id);
//        }
//        $sql->where('UserType', '=', $userType);
//
//        $response = $sql->first();
//
//        return $response ? $response->Id : 0;
//    }

    public static function getFundCategory($fundCategory, $id)
    {
        $response = self::where(['UserId' => $id, 'UserType' => 'Provider', 'Name' => trim(strtolower($fundCategory)) ])->first();

        return $response ? $response->Id : 0;
    }
    
    public static function getProviderFundCategory($name, $id)
    {
        return self::where(['UserId' => $id, 'Name' => trim(strtolower($name)), 'UserType' => 'Provider'])->first();
    }

    public static function getProviderFundCategories($id)
    {
        return self::select('Id', 'Name as fund_category')->where(['UserId' => $id, 'UserType' => 'Provider'])->whereNotIn('Name', ['no fund category'])->groupBy('Name')->get();
    }

    /**
     * This is used to get fund category by type
     *
     * @param $params
     * @return mixed
     */
    public static function getFundCategoryByType($params)
    {
        return self::where('Name', $params['name'])->where('UserType', $params['type'])->where('UserId', $params['id'])->first();
    }

    public static function fundCategoryCount($fund_category_id)
    {
        return self::where('FundCategoryId', $fund_category_id)->count();
    }

    public static function fundCategoryStatus($fund_category_id)
    {
        return DB::table('fund_categories')
                ->select('DefaultCheck')
                ->where('Id', $fund_category_id)
                ->first();
    }

    public static function updateFundCategoryStatus($fund_category_id)
    {
        DB::table('fund_categories')
            ->where('Id', $fund_category_id)
            ->update(['DefaultCheck' => '0']);

    }

    public static function getExcludedCategoryAssets($company_id)
    {
        $col_other_fc = DB::table('collection_other_categories as coc')
                            ->select('coc.CategoryName')
                            ->pluck('coc.CategoryName');

        $fc_data = DB::table('fund_category_copy_pastes as fc')
                    ->join('investment_option_copy_pastes as io', 'io.FundCategoryId', '=', 'fc.Id')
                    ->select(DB::raw("SUM(io.Assets) as ExcludedAssets"))
                    ->where('fc.UserType', 'advisor_company')
                    ->where('fc.UserId', $company_id)
                    ->whereIn('fc.Name', $col_other_fc)
                    ->first();
        return $fc_data ? $fc_data->ExcludedAssets : 0;
    }

}