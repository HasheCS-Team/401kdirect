<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;

class FundCategoryCopyPaste extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['Name', 'UserId', 'UserType', 'street_user_id', 'DefaultCheck', 'CreatedBy', 'UpdatedBy'];
    protected $guarded = [];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * Validation for create and update.
     *
     * @var array
     */
    /**
     * Validation for create and update.
     *
     * @var array
     */

    public static function fundCategoryCreate($providerId, $funId = null)
    {
        $arr['UserId'] = 'required';
        $arr['DefaultCheck'] = 'required';

        return $arr;
    }

    public static function companyFundCategoryCreate($companyId, $Id = null)
    {
        $arr['Name'] = "required|unique:fund_category_copy_pastes,Name,null,Id,UserId,$companyId,UserType,advisor_company";

        return $arr;
    }

    public function scopeEdit($query)
    {
        $query->select(['Name']);
    }

    /**
     * Get all of the owning user models.
     */
    public function userType()
    {
        return $this->morphTo(null, 'UserType', 'UserId');
    }

    /**
     * The Fund Category may have many Investment Options for a Provider.
     */
    public function investmentOptionsCopyPaste()
    {
        return $this->hasMany('App\Models\InvestmentOptionCopyPaste', 'FundCategoryId', 'Id')
            ->select(['Id', 'FundCategoryId', 'Name', 'OneYear', 'ThreeYear', 'FiveYear', 'Assets','Expense', 'TenYear', 'DefaultForProvider', 'QuarterUpdatedDate', 'QuarterVersion', 'Score', 'Ticker', 'CreatedDate', 'UpdatedDate'])->orderBy('Name', 'asc');
    }

    /**
     * front end side, will show relevent in dropdown box.. on fundcategory's investment page...
     */
    public function dropdownInvestmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOptionCopyPaste', 'FundCategoryId', 'Id')
            ->select(['Id', 'Name as option_name']);
    }


    public static function updateFundcategoriesUserTypeOld($userId)
    {
        self::where(['UserId' => $userId, 'UserType' => 'advisor_company'])->update(['UserType' => 'advisor_company_old']);
    }


    public function fcInvestmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOptionCopyPaste', 'investment_option_copy_pastes.FundCategoryId', 'Id')
            ->select([
                'fund_category_copy_pastes.Id as fcId',
                'investment_option_copy_pastes.Id as ioId',
                'investment_option_copy_pastes.Name as option_name',
                'fund_category_copy_pastes.Name as fund_category'
            ]);
    }

    /**
     * This is used to get fund category by type
     *
     * @param $params
     * @return mixed
     */
    public static function getFundCategoryByType($params)
    {
        return self::where('Name', $params['name'])->where('UserType', $params['type'])->where('UserId', $params['id'])->first();
    }

    /**
     * This is used to get temporary investment options of category
     *
     * @param $params
     * @return bool|mixed
     */
    public static function getInvestmentOptions($params)
    {
        $sql = <<<SQL
                    SELECT fc.*,io.* FROM fund_category_copy_pastes AS fc
                    JOIN investment_option_copy_pastes AS io ON io.`FundCategoryId` = fc.`Id`
                    WHERE fc.`UserType` = 'advisor_company' 
                    AND fc.`UserId` = ?;
SQL;

        $db = getDB();

        return $db->fetchArrayAll($sql, [$params['companyId']]);
    }
}
