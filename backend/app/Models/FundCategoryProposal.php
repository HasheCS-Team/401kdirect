<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;
use DB;

//traits
use App\Traits\ModelCommon;
use App\Models\Proposal\Campaigns;

class FundCategoryProposal extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fund_categories_proposal'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'ProviderId', 'CompanyId', 'AdvisorId'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     /**
     * Validation for create and update.
     *
     * @var array
     */
      /**
     * Validation for create and update.
     *
     * @var array
    */

    public static $investmentFundsChangeRules = ['options.*.Name' => 'required',
                                                 'options.*.Assets' => 'required|numeric',
                                                 'options.*.Expense' => 'required|numeric',
                                                 'options.*.PlanPercentage' => 'required|numeric'
                                                ];

    public static $investmentFundsChangeMessages = ['options.*.Name.required' => 'Option Name :attribute is required',
                                                
                                                     'options.*.Assets.required' => 'Assets field is required',
                                                     'options.*.Assets.numeric' => 'Assets field must be Numaric',
                                                
                                                     'options.*.Expense.required' => 'Expense field is required',
                                                     'options.*.Expense.numeric' => 'Expense field must be Numaric',

                                                     'options.*.PlanPercentage.required' => 'PlanPercentage field is required',
                                                     'options.*.PlanPercentage.numeric' => 'PlanPercentage field must be Numaric',
                                                    ];

    // public static $providerFundsCreateRules = ['options.*.Name' => 'required',
    //                                            'options.*.Assets' => 'required|numeric',
    //                                            'options.*.Expense' => 'required|numeric'
    //                                           ];

    // public static $providerCreateMessages = ['options.*.Name.required' => 'Option Name :attribute is required',
                                        
    //                                          'options.*.Assets.required' => 'Assets field is required',
    //                                          'options.*.Assets.numeric' => 'Assets field must be Numaric',
                                        
    //                                          'options.*.Expense.required' => 'Expense field is required',
    //                                          'options.*.Expense.numeric' => 'Expense field must be Numaric'
    //                                         ];


    public static function fundCategoryCreate($providerId, $funId = null)
    {
        $arr['UserId'] = 'required';
        $arr['Name'] = "required|unique:fund_categories,Name,null,Id,UserId,$providerId,UserType,provider";

        if ( $funId )
        {
            $arr['Name'] = "required|unique:fund_categories,Name,$funId,Id,UserId,$providerId,UserType,provider";
        }

        return $arr; 
    }  

    public static function companyFundCategoryCreate($companyId, $Id = null)
    {
        // $arr['UserId'] = 'required';
        $arr['Name'] = "required|unique:fund_categories,Name,null,Id,UserId,$companyId,UserType,advisor_company";

        // if ( $Id )
        // {
        //     $arr['Name'] = "required|unique:fund_categories,Name,$Id,Id,UserId,$companyId,UserType,advisor_company";
        // }

        return $arr; 
    }  

    //excel file upload validation....
    public static $investmentOptionRules = ['fund_category',
                                            'investment_options',
                                            'oneyear',
                                            'threeyear',
                                            'fiveyear',
                                            'tenyear',
                                            'expense',
                                            'default_for_provider',
                                        ];

    public function scopePrimaryKey($query, $name, $ProviderId)
    {
        $query->select($this->primaryKey)->where('Name', $name)->where('UserId', $ProviderId);
    }

    public function scopeEdit($query)
    {
        $query->select(['Name']);
    }

    public static function verifyCompaign($advisor_id, $company_id, $wholesaler_id)
    {
        // return DB::table('users as u')
        //         ->join('advisors as a', 'a.UserId', '=', 'u.Id')
        //         ->join('advisor_companies as ac', 'ac.AdvisorId', '=', 'a.UserId')
        //         // ->join('campaigns as c', 'c.AdvisorId', '=', 'a.UserId')
        //         ->join('campaigns as c', 'c.CompanyId', '=', 'ac.Id')
        //         ->join('benchmarking_results as br', 'br.ProcessId', '=', 'c.ProcessId')
        //         ->join('provider_stats as ps', 'ps.CampaignId', '=', 'c.Id')
        //         ->join('providers_wholesaler as pw', 'pw.UserId', '=', 'ps.WholesalerId')
        //         ->join('providers_wholesaler_zips as pz', 'pz.WholesalerId', '=', 'pw.UserId')
        //         ->where('a.UserId', $advisor_id)
        //         ->where('ac.Id', $company_id)
        //         ->where('ps.WholesalerId', $wholesaler_id)
        //         ->where('ps.Status', 'Current')
        //         ->select('u.Id', 'c.Id as CampaignId', 'u.ZipCode', 'ps.ProviderId')
        //         ->whereIn('u.ZipCode', DB::table('providers_wholesaler_zips')->select('ZipCode')->where('WholesalerId', $wholesaler_id)->pluck('ZipCode'))
        //         // ->limit(1)
        //         ->toSql();
        //         // ->pluck('ZipCode', 'Id', 'CampaignId');

        $campaigns = Campaigns::where(['AdvisorId' => $advisor_id, 'CompanyId' => $company_id]);

        return $campaigns ? $campaigns->first()->proposalConnects()->where('WholesalerId',$wholesaler_id)->first() : 0;
    }

    public static function verifyAdvisorCompaign($advisor_id, $company_id, $provider_id)
    {
        // return DB::table('users as u')
        //         ->join('advisors as a', 'a.UserId', '=', 'u.Id')
        //         ->join('advisor_companies as ac', 'ac.AdvisorId', '=', 'a.UserId')
        //         ->join('campaigns as c', 'c.CompanyId', '=', 'ac.Id')
        //         // ->join('benchmarking_results as br', 'br.ProcessId', '=', 'c.ProcessId')
        //         ->join('provider_stats as ps', 'ps.CampaignId', '=', 'c.Id')
        //         ->join('providers_wholesaler as pw', 'pw.UserId', '=', 'ps.WholesalerId')
        //         ->join('providers_wholesaler_zips as pz', 'pz.ZipCode', '=', 'u.ZipCode')
        //         ->where('a.UserId', $advisor_id)
        //         ->where('ac.Id', $company_id)
        //         ->where('ps.ProviderId', $provider_id)
        //         ->where('ps.Status', 'Current')
        //         ->select('c.AdvisorId', 'c.CompanyId', 'c.Id as CampaignId', 'u.ZipCode', 'ps.ProviderId', 'ps.WholesalerId')
        //         ->limit(1)
        //         ->first();
        $campaigns = Campaigns::where(['AdvisorId' => $advisor_id, 'CompanyId' => $company_id])->first();

        return $campaigns != null ? $campaigns->proposalConnects()->where('ProviderId', $provider_id)->where('AccessType', 'W')->first() : 0;
    }

    public static function getAdvisorCategoryBuilder($advisor_id, $company_id, $provider_id, $fund_category_name)
    {
        return FundCategoryProposal::where('Name', $fund_category_name)
                                ->where('CompanyId', $company_id)
                                ->where('ProviderId', $provider_id)
                                ->where('AdvisorId', $advisor_id);
    }

    public static function createChanges($fund_category, $options)
    {
        if ( $fund_category->exists() )
        {
            $comming_options = array_column($options, 'Name');
            $saved_options = $fund_category->first()->frontInvestmentOptions;

            $deleted_options = [];
            $changed_options = [];

            foreach ( $saved_options as $investment_option )
            {
                if ( !in_array($investment_option->Name, $comming_options) )
                {
                    $investment_option->Operation = 'delete';
                    $deleted_options[] = array_diff_key($investment_option->toarray(), array_flip(['Id', 'FundCategoryId']));
                }
                else
                {
                    //matched... 
                    $index = array_search($investment_option->Name, $comming_options);

                    if ( $index >= 0 )
                    {
                        $changed = $options[$index];

                        if ( $changed['Name'] == $investment_option->Name && intval($investment_option->Assets) != intval($changed['Assets']) )
                        {
                            $changed['Operation'] = 'change';
                            $changed_options[] = array_diff_key($changed, array_flip(['Id', 'FundCategoryId', 'CategoryName', 'DefaultForProvider', 'checked', 'PlanPercentage']));
                        }
                    }
                }
            }
            
            $compare_saved_options = array_column($saved_options->toArray(), 'Name');
            
            $new_options = [];
            
            foreach ( $options as $investment_option )
            {
                if ( !in_array($investment_option['Name'], $compare_saved_options) )
                {
                    $investment_option['Operation'] = 'new';
                    $new_options[] = array_diff_key($investment_option, array_flip(['Id', 'FundCategoryId', 'CategoryName', 'DefaultForProvider', 'checked', 'PlanPercentage']));
                }
            }
            return array_merge($deleted_options, $new_options, $changed_options);
        }
        return false;
    }

    public static function createCategory($advisor_id, $provider_id, $company_id, $name)
    {
        // dd($advisor_id, (int) $provider_id, (int) $company_id, $name);
        return self::create(['ProviderId' => (int) $provider_id, 'CompanyId' => (int) $company_id, 'AdvisorId' => $advisor_id, 'Name' => $name]);
    }

    /**
     * Get all of the owning user models.
     */
    // public function userType()
    // {
    //     return $this->morphTo(null, 'UserType', 'UserId');
    // }

    /**
     * Provider Stats belongs to Advisor Company.
     */
    public function advisorCompany()
    {
        return $this->belongsTo('App\Models\AdvisorCompany', 'CompanyId','Id');
    }

    /**
     * The Fund Category Belongs to Provider.
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider', 'UserId');
    }

    /**
     * The Fund Category may have many Investment Options for a Provider.
    */
    public function investmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOptionProposal', 'FundCategoryId', 'Id')
                    ->select(['Id', 'FundCategoryId', 'Name','OneYear', 'ThreeYear', 'FiveYear', 'Score', 'Expense', 'TenYear', 'DefaultForProvider']);
    }


    public function investmentOptionsCalculation()
    {
        return $this->hasMany('App\Models\InvestmentOptionProposal', 'FundCategoryId', 'Id')
                    ->select(['Name','Assets' ,'OneYear', 'ThreeYear', 'FiveYear', 'Expense', 'TenYear', 'DefaultForProvider']);
    }

    /**
     * The Fund Category may have many Investment Options for a Provider.
    */
    public function frontInvestmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOptionProposal', 'FundCategoryId', 'Id')
                    ->select(['Id', 'FundCategoryId', 'Name', 'OneYear', 'ThreeYear', 'FiveYear', 'TenYear','Expense', 'Assets']);
    }

    /**
     * front end side, will show relevent in dropdown box.. on fundcategory's investment page...
     */
    public function dropdownInvestmentOptions()
    {
        return $this->hasMany('App\Models\InvestmentOptionProposal', 'FundCategoryId', 'Id')
                    ->select(['Id', 'Name as option_name']);
    }

    /**
     * [selectProviderFundInvestments check provider have proposed categories]
     * @param  [type] $companyId [Company Id]
     * @return [type]            [description]
     */
    public static function providerFundCategoriesExist($provider_id, $advisor_id, $company_id)
    {   
        return \DB::table('fund_categories_proposal')
                ->join('investment_options_proposal', 'fund_categories_proposal.Id', '=', 'investment_options_proposal.FundCategoryId')
                ->where('ProviderId', $provider_id)
                ->where('AdvisorId', $advisor_id)
                ->where('CompanyId', $company_id)->count();
    }

    /**
     * [get fund investment options on confirmation page]
     * @param  [type] $companyId [Company Id]
     * @return [type]            [description]
     */
    public static function selectFundInvestmentsConfirmation($provider_id, $company_id, $advisor_id)
    {   
        return FundCategoryProposal::select('Id', 
                                            'Name as FundName',
                                            DB::raw('ROUND(AssetsValue,2) as AssetsValue'),
                                            'PlanPercentage',
                                            'WeightedExpense', DB::raw("IF( (SELECT COUNT(*) > 0 FROM collection_other_categories coc WHERE coc.CategoryName = Name) ,0,1) AS orderKey"))
                        ->with(['investmentOptions' => function($query) use ($company_id,$advisor_id,$provider_id) {
                           $query->select('Id','FundCategoryId', 'Name as InvestName', 'OneYear','ThreeYear','FiveYear','TenYear','Assets','Expense', DB::raw("ROUND((SUM( (Assets/(SELECT SUM(io.Assets) FROM fund_categories_proposal fc INNER JOIN investment_options_proposal io ON fc.Id = io.FundCategoryId WHERE fc.CompanyId = ".$company_id." AND fc.ProviderId = ".$provider_id." AND fc.AdvisorId = ".$advisor_id.")) * 100 )), 2) AS AvgAssets"), DB::raw('COUNT(Name) as IoCount'))->groupBy('Id');
                        }])->where('ProviderId', $provider_id)
                        ->where('CompanyId', $company_id)
                        ->where('AdvisorId', $advisor_id)
                        ->groupBy('FundName')
                        ->orderBy('orderKey', 'DESC')
                        ->orderBy('FundName', 'ASC');
    }

    /**
     * This is used to get proposal data
     *
     * @param $params
     * @return mixed
     */
    public static function getProposalData($params)
    {
        return \DB::table('fund_categories_proposal as fcp')
            ->select('fcp.*', 'iop.*')
            ->join('investment_options_proposal as iop', 'fcp.Id', '=', 'iop.FundCategoryId')
            ->where('fcp.AdvisorId', $params['advisorId'])
            ->where('fcp.CompanyId', $params['companyId'])
            ->get();
    }

}