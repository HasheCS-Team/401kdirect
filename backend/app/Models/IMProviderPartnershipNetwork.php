<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelCommon;

/* Models */
use App\Models\Users\Advisor;
use App\Models\User;

use DB;

class IMProviderPartnershipNetwork extends Model
{
    //traits this model using...
    use ModelCommon;

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'im_provider_partnership_network';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    public $incrementing = true;

    protected $fillable = ['CampaignId', 'ProviderWholesalerId', 'IMWholesalerId'];

    public static function getPartnershipData($imw, $im, $dateStart, $dateEnd)
    { 
        return DB::table('im_provider_partnership_network as ippn')
        ->leftJoin('investment_manager_opportunities as imo', function($join){
            $join->on('ippn.CampaignId', '=', 'imo.CampaignId');
            // $join->on('ippn.IMWholesalerId', '=', 'imo.WholesalerId');
        })
        ->join('providers_wholesaler as pw', 'ippn.ProviderWholesalerId', '=', 'pw.UserId')
        ->join('users as u', 'pw.ProviderId', '=', 'u.Id')
        ->join('campaigns as c', 'ippn.CampaignId', '=', 'c.Id')
        ->join('providers as p', 'p.UserId', '=', 'u.Id')
        // ->where('ippn.IMWholesalerId', $imw)
        ->where('imo.WholesalerId', $imw)
        ->whereRaw('imo.CampaignId IS NOT NULL')
        ->whereRaw('ippn.IMWholesalerId = GET_PRIMARY_IMW_ID(imo.CampaignId, '.$im.')')
        ->where(function($query) use ($dateStart, $dateEnd) {
            if ($dateStart != 'All' && $dateEnd == 'All') {
                $query->where('ippn.CreatedDate', '>=', $dateStart);
            } elseif ($dateStart == 'All' && $dateEnd != 'All') {
                $query->where('ippn.CreatedDate', '<=', $dateEnd);
            } elseif ($dateStart != 'All' && $dateEnd != 'All') {
                $query->whereBetween('ippn.CreatedDate', array($dateStart, $dateEnd));
            }
        })
        ->select( [
            'ippn.CampaignId',
            'ippn.ProviderWholesalerId',
            'ippn.IMWholesalerId',
            'imo.OpportunityStatus',
            'imo.WinnerProviderId',
            'pw.ProviderId',
            'p.FirmName AS providerName',
            // DB::raw('CONCAT(u.FirstName, u.LastName) AS providerName'),
            'ippn.CreatedDate',
            DB::raw("IF (
                            imo.OpportunityStatus = 'won',
                            PROPOSED_FUNDS_TOTAL (pw.ProviderId,$im,c.CompanyId),
                            SUBSCRIBED_FUNDS_TOTAL (pw.ProviderId,$im,c.CompanyId)
                        ) AS assets"
                    )  
        ])
        ->get();
    }

    // checks if IMW and PW partnership exists on specific campaign
    public static function checkPartnershipExists($campaign_id, $pw_id, $imw_id) {
        $result = DB::table('im_provider_partnership_network as impn')
                    ->select('impn.CampaignId', 'impn.ProviderWholesalerId', 'impn.IMWholesalerId')
                    ->where(['CampaignId' => $campaign_id,'ProviderWholesalerId' => $pw_id, 'IMWholesalerId' => $imw_id])
                    ->count();

        return $result;
    }

}