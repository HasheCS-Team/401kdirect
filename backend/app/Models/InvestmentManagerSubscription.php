<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;

use Config;
use DB;

class InvestmentManagerSubscription extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'investment_manager_subscriptions';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['InvestmentOptionName', 'CategoryName', 'ProviderId', 'InvestmentManagerId', 'ProviderInvestmentOptionId', 'CreatedDate', 'UpdatedDate', 'ProductId', 'Ticker'];

    public static function getSubscribedInvestmentOptions($investment_manager_id)
    {
        return DB::table('investment_manager_subscriptions as ims')
                ->join('providers as p', 'p.UserId', '=', 'ims.ProviderId')
                ->leftJoin('products as pro', 'pro.id', '=', 'ims.ProductId')
                ->where('ims.InvestmentManagerId', '=', $investment_manager_id)
                ->where('ims.IsDeleted', '=', NULL)
                ->select('ims.InvestmentOptionName', 'ims.ProviderInvestmentOptionId AS InvestmentOptionId', 'ims.ProviderId AS UserId', 'ims.CategoryName AS FundCategoryName', 'p.Logo', 'pro.id', 'pro.name as ProductName', DB::raw('IF(ims.ProductId = 0, "Provider", "broker_dealer_product") as UserType'))
                ->get();
    }

    public static function checkSubscription($investment_manager_id)
    {
        return self::where('InvestmentManagerId', $investment_manager_id)->exists();
    }

    public static function deleteSubscription($investment_manager_id)
    {
        return self::where('InvestmentManagerId', $investment_manager_id)->forceDelete();
    }

}