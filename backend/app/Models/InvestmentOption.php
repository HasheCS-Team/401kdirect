<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use App\Models\Users\Provider;
use DB;
class InvestmentOption extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'investment_options'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'FundCategoryId', 'FundName','ProductId', 'ProviderId', 'PlanIdExcel', 'RjFlag', 'OneYear', 'ThreeYear', 'FiveYear', 'TenYear', 'Expense', 'Score', 'Assets', 'DefaultForProvider', 'DefaultForAdvisor', 'QuarterUpdatedDate', 'QuarterVersion', 'Ticker', 'CreatedBy', 'UpdatedBy'];


    public static $createRules = ['options.*' => 'required',
                                  'options.*.Name' => 'required',
//                                  'options.*.OneYear' => 'required',
//                                  'options.*.ThreeYear' => 'required',
//                                  'options.*.FiveYear' => 'required',
//                                  'options.*.TenYear' => 'required',
//                                  'options.*.Expense' => 'required',
                                ]; 

    public static $createRuleDefaultForProvider = [
                                  'options.*' => 'required',
                                  'options.*.Name' => 'required',
//                                  'options.*.OneYear' => 'required',
//                                  'options.*.ThreeYear' => 'required',
//                                  'options.*.FiveYear' => 'required',
//                                  'options.*.TenYear' => 'required',
//                                  'options.*.Expense' => 'required',
                                  'options.*.DefaultForProvider' => 'required'
                                ]; 
    
    public static $createRuleDefaultForProductDefaultProvider = [
        'options.*.DefaultForProvider' => 'required'
    ];

    public static function investmentProductCreateRules ($catId = null)
    {
        $arr['options.*'] = 'required';
        $arr['options.*.Expense'] = 'required';

        if ( $catId )
        {
            $arr['options.*.Name'] = "required";
        }

        return $arr;
    }
                            

    public static function investmentCreateRules($catId = null)
    {
        $arr['options.*'] = 'required';
//        $arr['options.*.OneYear'] = 'required';
//        $arr['options.*.ThreeYear'] = 'required';
//        $arr['options.*.FiveYear'] = 'required';
//        $arr['options.*.TenYear'] = 'required';
//        $arr['options.*.Expense'] = 'required';

        if ( $catId )
        {
            // $arr['options.*.Name'] = "required|unique:investment_options,Name,$catId";
            $arr['options.*.Name'] = "required";
        }
        // else
        // {
        //     $arr['options.*.Name'] = "required|unique:investment_options,Name,null";
        // }

        return $arr; 
    } 

    public static $advisorCompanyCreateRules = ['options.*.Name' => 'required',
                                                'options.*.Assets' => 'required',
                                                'options.*.Expense' => 'required'
                                            ];

    public static $advisorCompanyCreateMessages = [
                            'options.*.Name.required' => 'Fund Name is required',
                            
                            'options.*.Assets.required' => 'Assets field is required',
                            'options.*.Assets.numeric' => 'Assets field must be Numaric',
                        
                            'options.*.Expense.required' => 'Expense field is required',
                            'options.*.Expense.numeric' => 'Expense field must be Numaric',
    ];

    public static $createMessages = [
                            'options.*.Name.required' => 'Option Name :attribute is required',
                            // 'options.*.Name.unique' => 'Option name has already been taken',
                        
//                            'options.*.OneYear.required' => ' OneYear field is required',
                            'options.*.OneYear.numeric' => 'OneYear field must be Numeric',
                        
//                            'options.*.ThreeYear.required' => 'ThreeYear field is required',
                            'options.*.ThreeYear.numeric' => 'ThreeYear field must be Numeric',
                        
//                            'options.*.FiveYear.required' => ' FiveYear field is required',
                            'options.*.FiveYear.numeric' =>  'FiveYear field must be Numeric',
                        
//                            'options.*.TenYear.required' => 'TenYear field is required',
                            'options.*.TenYear.numeric' => 'TenYear field must be Numeric',
                        
//                            'options.*.Expense.required' => 'Expense field is required',
                            'options.*.Expense.numeric' => 'Expense field must be Numeric',

                            'options.*.DefaultForProvider.required' => 'Default For Provider field is required',
                        ];

    // public function setDefaultForProviderAttribute($value)
    // {
    //     $this->attributes['DefaultForProvider'] = ($value == 1 ? 1 : 0);
    // }

    // public function setDefaultForProviderAttribute($value)
    // {
    //     $this->attributes['DefaultForProvider'] = ($value == 1 ? 1 : 0 );
    // }

    // public function setDefaultForProviderAttribute($value)
    // {
    //     $this->attributes['DefaultForProvider'] = ($value == "Yes" ? 1 : 0 );
    // }

    public function fundCategory()
    {
        return $this->belongsTo('App\Models\FundCategory', 'FundCategoryId', 'Id');
    }

    /**
     * front end side...
     * @param  string $name        category name against search need to be made...
     * @param  object $provider_id  against specific Provider Id...
     * @param  string $search_term search term for category's option's name...
     */
    public static function providerOptions($name, $provider_id, $search_term = '')
    {
        // dd($name, $provider_id, $search_term = '');
        $data['Options'] = \DB::table('investment_options as ios')
            ->join('fund_categories as fcs', 'ios.FundCategoryId', '=', 'fcs.Id')
            ->where('fcs.Name', '=', $name)
            ->where('fcs.UserId', '=', $provider_id)
            ->where('fcs.UserType', '=', 'provider')
            ->where('ios.Name', 'LIKE', "%$search_term%")
            ->select(['ios.Id', 'ios.Name as Name', 'ios.FundCategoryId', 'fcs.Name as CategoryName', 'ios.OneYear', 'ios.ThreeYear', 'ios.FiveYear', 'ios.TenYear', 'ios.Score',  'ios.Expense', 'ios.DefaultForProvider'])
            ->orderBy('ios.Name', 'ASC')
                ->get();

        $data['ProviderLogo'] = Provider::logo($provider_id);

        return $data;
    }

    // options when broker product used against a provider
    public static function providerOptionsProduct($name, $product_id,$provider_id, $search_term = '')
    {
        // dd($name, $provider_id, $search_term = '');
        $data['Options'] = \DB::table('investment_options as ios')
            ->join('fund_categories as fcs', 'ios.FundCategoryId', '=', 'fcs.Id')
            ->where('fcs.Name', '=', $name)
            ->where('fcs.UserId', '=', $product_id)
            ->where('fcs.UserType', '=', 'broker_dealer_product')
            ->where('ios.ProviderId', '=', $provider_id)
            ->where('ios.Name', 'LIKE', "%$search_term%")
            ->select(['ios.Id', 'ios.Name as Name', 'ios.FundCategoryId', 'fcs.Name as CategoryName', 'ios.OneYear', 'ios.ThreeYear', 'ios.FiveYear', 'ios.TenYear', 'ios.Score', 'ios.Expense', 'ios.DefaultForProvider'])
            ->orderBy('ios.Name', 'ASC')
                ->get();

        $data['ProviderLogo'] = Provider::logo($provider_id);

        return $data;
    }
    

        /**
     * front end side...
     * @param  string $name        category name against search need to be made...
     * @param  object $provider_id  against specific Provider Id...
     * @param  string $search_term search term for category's option's name...
     */
    public static function fundNames( $search_term)
    {
        //dd(substr($search_term,0,3));
        $data =
        \DB::table('investment_options')
            ->select(['Id','name'])            
             ->whereRaw("ACRONYM(SUBSTRING(name, TRIM(LOCATE(' ', CONCAT(name, ' '))+1))) LIKE '%".substr($search_term,0,3)."%'")
            // ->whereRaw("CAPITILIZE('hello world', ' ' , TRUE) LIKE '%".substr($search_term,0,3)."%'")
            // ->whereRaw("ACRONYM(SUBSTRING(NAME, TRIM(LOCATE(' ', CONCAT(NAME, ' '))+1))) LIKE '%ACRONYM(SUBSTRING(". $search_term .", TRIM(LOCATE(' ', CONCAT(". $search_term .", ' '))+1)))%' "    
            //     )
            //->whereRaw("ACRONYM(SUBSTRING(NAME, TRIM(LOCATE(' ', CONCAT(NAME, ' '))+1))) LIKE '%RP%'"    
             //   )
            //->whereRaw("ACRONYM(SUBSTRING(name, TRIM(LOCATE(' ', CONCAT(name, ' '))+1))) LIKE '%RP%'")
            ->take(8)
            ->get();
       
        return $data;
        /*  MySql Query 
            
             SELECT 
                NAME ,
                SUBSTRING(NAME, LOCATE(' ', CONCAT(NAME, ' '))+1) AS nextstring,
                ACRONYM(SUBSTRING(NAME, TRIM(LOCATE(' ', CONCAT(NAME, ' '))+1))) AS firstLetters
               FROM investment_options 
             WHERE TRIM(SUBSTRING(NAME,1,3)) LIKE '%ab%'
             AND ACRONYM(SUBSTRING(NAME, TRIM(LOCATE(' ', CONCAT(NAME, ' '))+1))) LIKE 't%'
        */

         
    }

    public static function validateInvestmentExpense($investment_name, $expense, $fund_ids){
        
       return self::where('Name', trim(strtolower($investment_name)))->whereRaw("FORMAT(Expense,6) = FORMAT('$expense',6)")->whereIn('FundCategoryId', $fund_ids)->get();
        // $investment = \DB::select('SELECT * FROM investment_options WHERE `Name` = "'.$investment_name.'" and FORMAT(Expense,2) = FORMAT("'.$expense.'",2) and FundCategoryId IN ("'.$fund_ids.'")');
        // dd($investment_name, $expense, $fund_ids,$investment);
    }


    public static function validateFundCategoryInvestment($investment_name, $expense, $fund_id){
        
        return self::where('Name', trim(strtolower($investment_name)))->where('FundCategoryId', $fund_id)->first();
         // $investment = \DB::select('SELECT * FROM investment_options WHERE `Name` = "'.$investment_name.'" and FORMAT(Expense,2) = FORMAT("'.$expense.'",2) and FundCategoryId IN ("'.$fund_ids.'")');
         // dd($investment_name, $expense, $fund_ids,$investment);
     }

    // public static function validateInvestmentExpense($investment, $provider_id, $fund_id){
        
    //     return \DB::table('fund_categories as fc')
    //             ->join('investment_options as io', 'fc.Id', '=', 'io.FundCategoryId')
    //             ->where('io.Name', trim($investment))
    //             ->where('fc.UserType', 'Provider')
    //             ->where('fc.UserId', $provider_id)
    //             // ->where('io.FundCategoryId', '!=', $fund_id)
    //             ->first();

    //     // $response = self::where('Name', trim($investment))->where('Expense', $expense)->where('FundCategoryId', $fund_category_id)->get();

    //     // return count($response);
    // }

    public static function checkQuarterlyDate($fund_category_id, $date)
    {
        return self::where(['FundCategoryId' => $fund_category_id])->where(function($q) use ($date) {
              $q->where('QuarterUpdatedDate', null)
              ->orWhere('QuarterUpdatedDate' ,'!=', $date);
            })->exists();
    }

        /**
     * This is used to get ivestment options count against all categories of providers
     *
     * @param $params
     * @return array|bool
     */
    public static function getIOAgainstProductCategory($params)
    {
        $providerIds = $params["providerId"];
        $sql = <<<SQL
                      SELECT count(DISTINCT(FundCategoryId)) as count, ProviderId as providerId
                        FROM `investment_options` 
                     JOIN fund_categories ON fund_categories.`Id` = investment_options.`FundCategoryId`   
                      WHERE 
                        ProductId = ?
                      AND 
                        ProviderId IN ($providerIds)
                      GROUP BY ProviderId
SQL;

        $db = getDB();

        return $db->fetchArrayAll($sql, [$params['productId']]);
    }

    /**
     * This is used to check whether same investment option is added against same provider of product and check expense also
     *
     * @param $params
     * @return mixed
     */
    public static function validateProductInvestmentExpense($params, $fund_category_id)
    {
        $sql = self::where(['Name' => trim(strtolower($params['name'])) ])
            ->where('ProductId', '=', $params['productId'])
            ->where('ProviderId', '=', $params['providerId'])
            // ->where('Expense', '=', $params['expense'])
            ->where('FundCategoryId', trim(strtolower($fund_category_id)) );
        if (!empty($params['id'])) {
            $sql->where('Id', '!=', $params['id']);
        }
        return $sql->first();

    }

    public static function fundCategoryCount($fund_category_id)
    {
        return self::where('FundCategoryId', $fund_category_id)->count();
    }
    public static function defaultFundCategoryStatus($fund_category_id)
    {
        $response = DB::table('investment_options')->select('DefaultForProvider')->where('FundCategoryId',$fund_category_id)->get();
        foreach ($response as $key => $value) {
            if($value->DefaultForProvider == 1)
            {
                return $value->DefaultForProvider;
            }
        }
        return 0;
    }

    /**
     * This is used to get ivestment options count against all categories of providers
     *
     * @param $params
     * @return array|bool
     */
    public static function getTickerFunds($params)
    {
        $sql = DB::table('fund_categories as fc')
            ->select(
                'io.Ticker AS System_IOption_Ticker',
                'fc.NAME AS FundCategory',
                'io.Id AS System_IOption_Id',
                'fc.Id AS System_FC_Id',
                'io.NAME AS System_IOption_Name',
                'io.OneYear AS IO_One_Year',
                'io.ThreeYear AS IO_Three_Year',
                'io.FiveYear AS IO_Five_Year',
                'io.TenYear AS IO_Ten_Year',
                'io.Expense AS IO_Expense',
                'io.DefaultForProvider AS IO_Default',
                'io.QuarterUpdatedDate AS IO_QuarterUpdatedDate',
                'io.QuarterVersion AS IO_QuarterVersion'
            );
        $sql->join('investment_options as io', 'io.FundCategoryId', '=', 'fc.Id');
        if (!empty($params['productId'])) {
            $sql->where('fc.UserId', '=', $params['productId']);
            $sql->where('io.ProviderId', '=', $params['providerId']);
        } else {
            $sql->where('fc.UserId', '=', $params['providerId']);
        }
        $sql->where('fc.UserType', $params['userType']);

        return $sql->get();
    }

    public static function matchedTickerFunds($params)
    {
        $sql = DB::table('collection_investment_options as cio')
            ->select(
                'cio.Id',
                'cio.Ticker AS RPAG_Ticker',
                'cio.option_name AS RPAG_IOption_Name',
                'cio.fund_category AS RPAG_FundCategory',
                'cio.1yr AS RPAG_One_Year',
                'cio.3yr AS RPAG_Three_Year',
                'cio.5yr AS RPAG_Five_Year',
                'cio.10yr AS RPAG_Ten_Year',
                'cio.Expense AS RPAG_Expense',
                'cio.QuarterUpdatedDate AS RPAG_QuarterUpdatedDate',
                'cio.QuarterVersion AS RPAG_QuarterVersion',
                'cio.Is_Manual AS Is_Manual'
            );
        if (!empty($params['isTicker'])) {
            $sql->whereIn(DB::raw('LOWER(cio.Ticker)'), $params['tickers']);
        } else {
            if (!empty($params['ids']))
                $sql->whereNotIn('cio.Id', $params['ids']);
            $sql->whereIn(DB::raw('TRIM(LOWER(cio.option_name))'), $params['name']);
        }
        $sql->whereNull('cio.Is_manual');

        return $sql->get();
    }

    /**
     * This is used to provider all funds
     *
     * @param $params
     * @return mixed
     */
    public static function getProvidersAllFunds($params)
    {
        $sql = DB::table('fund_categories as fc')
            ->select(
                'fc.Name AS FundCategory',
                'io.Id AS System_IOption_Id',
                'fc.Id AS System_FC_Id',
                'io.Ticker AS System_IOption_Ticker',
                'io.Name AS System_IOption_Name',
                'io.OneYear AS IO_One_Year',
                'io.ThreeYear AS IO_Three_Year',
                'io.FiveYear AS IO_Five_Year',
                'io.TenYear AS IO_Ten_Year',
                'io.Expense AS IO_Expense',
                'io.DefaultForProvider AS IO_Default',
                'io.QuarterUpdatedDate AS IO_QuarterUpdatedDate',
                'io.QuarterVersion AS IO_QuarterVersion'
            );
        $sql->join('investment_options as io', 'io.FundCategoryId', '=', 'fc.Id');
        if (!empty($params['productId'])) {
            $sql->where('fc.UserId', '=', $params['productId']);
            $sql->where('io.ProviderId', '=', $params['providerId']);
        } else {
            $sql->where('fc.UserId', '=', $params['providerId']);
        }
        $sql->where('fc.UserType', $params['userType']);
        $sql->orderBy('FundCategory');
        $sql->limit(10000);

        return $sql->get();
    }

 }