<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;

class InvestmentOptionCopyPaste extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'FundCategoryId', 'ProductId', 'ProviderId', 'ticker', 'OneYear', 'ThreeYear', 'FiveYear', 'TenYear', 'Expense', 'Score', 'Assets', 'DefaultForProvider', 'DefaultForAdvisor', 'QuarterUpdatedDate', 'QuarterVersion', 'Ticker', 'CreatedBy', 'UpdatedBy'];


    public static $createRules = ['options.*' => 'required',
        'options.*.Name' => 'required',
    ];

    public static $createRuleDefaultForProvider = [
        'options.*' => 'required',
        'options.*.Name' => 'required',
        'options.*.DefaultForProvider' => 'required'
    ];

    public static $createRuleDefaultForProductDefaultProvider = [
        'options.*.DefaultForProvider' => 'required'
    ];

    public static function investmentProductCreateRules($catId = null)
    {
        $arr['options.*'] = 'required';
        $arr['options.*.Expense'] = 'required';

        if ($catId) {
            $arr['options.*.Name'] = "required";
        }

        return $arr;
    }


    public static function investmentCreateRules($catId = null)
    {
        $arr['options.*'] = 'required';
        if ($catId) {
            $arr['options.*.Name'] = "required";
        }

        return $arr;
    }

    public static $advisorCompanyCreateRules = ['options.*.Name' => 'required',
        'options.*.Assets' => 'required',
        'options.*.Expense' => 'required'
    ];

    public static $advisorCompanyCreateMessages = [
        'options.*.Name.required' => 'Fund Name is required',

        'options.*.Assets.required' => 'Assets field is required',
        'options.*.Assets.numeric' => 'Assets field must be Numaric',

        'options.*.Expense.required' => 'Expense field is required',
        'options.*.Expense.numeric' => 'Expense field must be Numaric',
    ];

    public static $createMessages = [
        'options.*.Name.required' => 'Option Name :attribute is required',
        // 'options.*.Name.unique' => 'Option name has already been taken',

//                            'options.*.OneYear.required' => ' OneYear field is required',
        'options.*.OneYear.numeric' => 'OneYear field must be Numeric',

//                            'options.*.ThreeYear.required' => 'ThreeYear field is required',
        'options.*.ThreeYear.numeric' => 'ThreeYear field must be Numeric',

//                            'options.*.FiveYear.required' => ' FiveYear field is required',
        'options.*.FiveYear.numeric' => 'FiveYear field must be Numeric',

//                            'options.*.TenYear.required' => 'TenYear field is required',
        'options.*.TenYear.numeric' => 'TenYear field must be Numeric',

//                            'options.*.Expense.required' => 'Expense field is required',
        'options.*.Expense.numeric' => 'Expense field must be Numeric',

        'options.*.DefaultForProvider.required' => 'Default For Provider field is required',
    ];

    public function fundCategoryCopyPaste()
    {
        return $this->belongsTo('App\Models\FundCategoryCopyPaste', 'FundCategoryId', 'Id');
    }


    /**
     * front end side...
     * @param  string $name category name against search need to be made...
     * @param  object $provider_id against specific Provider Id...
     * @param  string $search_term search term for category's option's name...
     */
    public static function fundNames($search_term)
    {
        $data =
            \DB::table('investment_option_copy_pastes')
                ->select(['Id', 'name'])
                ->whereRaw("ACRONYM(SUBSTRING(name, TRIM(LOCATE(' ', CONCAT(name, ' '))+1))) LIKE '%" . substr($search_term, 0, 3) . "%'")
                ->take(8)
                ->get();

        return $data;
    }

    public static function validateInvestmentExpense($investment_name, $expense, $fund_ids)
    {

        return self::where('Name', trim(strtolower($investment_name)))->whereRaw("FORMAT(Expense,6) = FORMAT('$expense',6)")->whereIn('FundCategoryId', $fund_ids)->get();
    }


    public static function validateFundCategoryInvestment($investment_name, $expense, $fund_id)
    {
        return self::where('Name', trim(strtolower($investment_name)))->where('FundCategoryId', $fund_id)->first();
    }

    public static function checkQuarterlyDate($fund_category_id, $date)
    {
        return self::where(['FundCategoryId' => $fund_category_id])->where(function ($q) use ($date) {
            $q->where('QuarterUpdatedDate', null)
                ->orWhere('QuarterUpdatedDate', '!=', $date);
        })->exists();
    }

    /**
     * This is used to check whether same investment option is added against same provider of product and check expense also
     *
     * @param $params
     * @return mixed
     */
    public static function validateProductInvestmentExpense($params, $fund_category_id)
    {
        $sql = self::where(['Name' => trim(strtolower($params['name']))])
            ->where('ProductId', '=', $params['productId'])
            ->where('ProviderId', '=', $params['providerId'])
            // ->where('Expense', '=', $params['expense'])
            ->where('FundCategoryId', trim(strtolower($fund_category_id)));
        if (!empty($params['id'])) {
            $sql->where('Id', '!=', $params['id']);
        }
        return $sql->first();

    }

    public static function defaultFundCategoryStatus($fund_category_id)
    {
        $response = DB::table('investment_options')->select('DefaultForProvider')->where('FundCategoryId', $fund_category_id)->get();
        foreach ($response as $key => $value) {
            if ($value->DefaultForProvider == 1) {
                return $value->DefaultForProvider;
            }
        }
        return 0;
    }
}
