<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class InvestmentOptionProposal extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'investment_options_proposal'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'FundCategoryId', 'OneYear', 'ThreeYear', 'FiveYear', 'TenYear', 'Score', 'Expense', 'Assets', 'DefaultForProvider', 'DefaultForAdvisor', 'CreatedBy', 'UpdatedBy'];


    public static $createRules = ['options.*' => 'required',
                                  'options.*.Name' => 'required',
                                  'options.*.OneYear' => 'required',
                                  'options.*.ThreeYear' => 'required',
                                  'options.*.FiveYear' => 'required',
                                  'options.*.TenYear' => 'required',
                                  'options.*.Expense' => 'required',
                                  'options.*.DefaultForProvider' => 'required'
                                ]; 

    public static $advisorCompanyCreateRules = ['options.*.Name' => 'required',
                                                'options.*.Assets' => 'required',
                                                'options.*.Expense' => 'required'
                                            ];

    public static $advisorCompanyCreateMessages = [
                            'options.*.Name.required' => 'Option Name :attribute is required',
                            
                            'options.*.Assets.required' => 'Assets field is required',
                            'options.*.Assets.numeric' => 'Assets field must be Numaric',
                        
                            'options.*.Expense.required' => 'Expense field is required',
                            'options.*.Expense.numeric' => 'Expense field must be Numaric',
    ];

    public static $createMessages = [
                            'options.*.Name.required' => 'Option Name :attribute is required',
                        
                            'options.*.OneYear.required' => ' OneYear field is required',
                            'options.*.OneYear.numeric' => 'OneYear field must be Numaric',
                        
                            'options.*.ThreeYear.required' => 'ThreeYear field is required',
                            'options.*.ThreeYear.numeric' => 'ThreeYear field must be Numaric',
                        
                            'options.*.FiveYear.required' => ' FiveYear field is required',
                            'options.*.FiveYear.numeric' =>  'FiveYear field must be Numaric',
                        
                            'options.*.TenYear.required' => 'TenYear field is required',
                            'options.*.TenYear.numeric' => 'TenYear field must be Numaric',
                        
                            'options.*.Expense.required' => 'Expense field is required',
                            'options.*.Expense.numeric' => 'Expense field must be Numaric',
                        ];

    public function setDefaultForProviderAttribute($value)
    {
       $this->attributes['DefaultForProvider'] = (strtolower($value) == 'yes' ? 1 : 0 );
    }

    // public function setDefaultForProviderAttribute($value)
    // {
    //     $this->attributes['DefaultForProvider'] = ($value == 1 ? 1 : 0 );
    // }

    // public function setDefaultForAdvisorAttribute($value)
    // {
    //     $this->attributes['DefaultForAdvisor'] = ($value == "Yes" ? 1 : 0 );
    // }

    public function fundCategory()
    {
        return $this->belongsTo('App\Models\FundCategoryProposal', 'FundCategoryId', 'Id');
    }

        /**
     * front end side...
     * @param  string $name        category name against search need to be made...
     * @param  object $provider_id  against specific Provider Id...
     * @param  string $search_term search term for category's option's name...
     */
    // public static function providerOptions($name, $provider_id, $search_term = '')
    // {
    //     $data['Options'] = \DB::table('investment_options as ios')
    //         ->join('fund_categories as fcs', 'ios.FundCategoryId', '=', 'fcs.Id')
    //         ->where('fcs.Name', '=', $name)
    //         ->where('fcs.UserId', '=', $provider_id)
    //         ->where('fcs.UserType', '=', 'provider')
    //         ->where('ios.Name', 'LIKE', "%$search_term%")
    //         ->select(['ios.Id', 'ios.Name as Name', 'ios.FundCategoryId', 'fcs.Name as CategoryName', 'ios.OneYear', 'ios.ThreeYear', 'ios.FiveYear', 'ios.TenYear', 'ios.Expense', 'ios.DefaultForProvider'])->get();

    //     $data['ProviderLogo'] = Provider::logo($provider_id);

    //     return $data;
    // }
}