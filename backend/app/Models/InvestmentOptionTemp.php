<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Collection\InvestmentOption AS IO;

//traits
use App\Traits\ModelCommon;
use App\Models\Users\Provider;

class InvestmentOptionTemp extends Model
{
    //traits this model using...
    use ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'investment_options_temp';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['FundCategoryId', 'InvestmentId', 'ProviderId', 'ProductId', 'RPAGFundCategory', 'FundCategory', 'InvestmentName', 'UserType',
        'Ticker', 'OneYear', 'ThreeYear', 'FiveYear', 'TenYear', 'Expense', 'DefaultForProvider', 'FundAction', 'IsMatch', 'MatchType', 'QuarterUpdatedDate', 'QuarterVersion', 'TotalFundsInvest'];


    public static function checkExistTempData($provider_id, $user_type)
    {
        return self::where(['ProviderId' => $provider_id, 'UserType' => $user_type])->exists();
    }

    public static function checkIOTempData($provider_id, $fund_name, $fund_id, $investment, $user_type)
    {        
        return self::where(['ProviderId' => $provider_id, 'UserType' => $user_type, 'InvestmentName' => trim(strtolower($investment['Name'])), 
                            'FundCategory' => strtolower($fund_name) ])->first();
    }

    // RPAG data
    public static function saveManualProviderInvestmentRPAG($provider_id, $fund_name, $fund_id, $data, $FundAction, $investment, $match_type, $user_type)
    {
        return self::create(['FundCategoryId' => $fund_id, 'InvestmentId' => $investment->Id, 'ProviderId' => $provider_id, 'UserType' => $user_type,
                            'RPAGFundCategory' => strtolower($data->fund_category), 'FundCategory' => strtolower($fund_name),
                            'InvestmentName' => strtolower($investment->Name), 
                            'Ticker' => $investment->Ticker, 'RPAG_Ticker' => $data->RPAG_Ticker, 
                            'OneYear' => $investment->OneYear, 'RPAG_OneYear' => $data->{'1yr'},
                            'ThreeYear' => $investment->ThreeYear, 'RPAG_ThreeYear' => $data->{'3yr'},
                            'FiveYear' => $investment->FiveYear, 'RPAG_FiveYear' => $data->{'5yr'}, 
                            'TenYear' => $investment->TenYear, 'RPAG_TenYear' => $data->{'10yr'},
                            'Expense' => $investment->Expense, 'RPAG_Expense' => $data->Expense, 
                            'DefaultForProvider' => $investment->DefaultForProvider, 
                            'IsMatch' => ($match_type == 'matched'? 1: 0),
                            'FundAction' => $FundAction, 
                            'MatchType' => $match_type, 
                            'QuarterUpdatedDate' => $data->QuarterUpdatedDate,
                            'QuarterVersion' => $data->QuarterVersion]);
     
    }

    // Investment option data
    public static function saveManualProviderInvestmentOption($provider_id, $fund_name, $fund_id, $FundAction, $investment, $user_type)
    {
        return self::create(['FundCategoryId' => $fund_id, 'InvestmentId' => $investment->Id, 'ProviderId' => $provider_id, 'UserType' => $user_type,
                            'RPAGFundCategory' => '', 'FundCategory' => strtolower($fund_name),
                            'InvestmentName' => strtolower($investment->Name), 'Ticker' => $investment->Ticker, 'OneYear' => $investment->OneYear,
                            'ThreeYear' => $investment->ThreeYear, 'FiveYear' => $investment->FiveYear, 'TenYear' => $investment->TenYear,
                            'Expense' => $investment->Expense, 'DefaultForProvider' => $investment->DefaultForProvider, 'IsMatch' => 0,
                            'FundAction' => $FundAction, 'MatchType' => 'not_match', 'QuarterUpdatedDate' => $investment->QuarterUpdatedDate,
                            'QuarterVersion' => $investment->QuarterVersion]);
    }


            /**
     * admin side...
     * @param  string $name category name against search need to be made...
     * @param  object $provider_id  against specific Provider Id...
     */
    public static function checkFundCategoryInvestment($fund_category, $investment_name, $expense, $provider_id, $user_type, $productId)
    {
        self::where('FundCategory', strtolower($fund_category))
                    ->where('InvestmentName', strtolower($investment_name))
                    ->where('ProviderId', '=', $provider_id)
                    ->where('ProductId', '=', $productId)
                    ->where('UserType', $user_type)
                    ->whereRaw("FORMAT(Expense,5) = FORMAT('$expense',5)")
                    ->count();
    }


    public static function validateFundCategoryInvestment($investment_name, $expense, $fund_category, $provider_id, $row_id, $user_type, $productId = 0){
        
        return self::where('InvestmentName', trim(strtolower($investment_name)))->where('FundCategory', trim(strtolower($fund_category)) )->where(['ProviderId' => $provider_id, 'ProductId' => $productId, 'UserType' => $user_type])->where('Id', '!=', $row_id)->get();
    }

    public static function updateDefaultFundCategory($user_type, $provider_id, $fund_category_id, $product_id){

        if($user_type == 'broker_dealer_product'){
            return self::where(['UserType' => trim($user_type), 'ProductId' => (int) $product_id, 'ProviderId'=> (int) $provider_id, 'FundCategoryId' => $fund_category_id, 'DefaultForProvider' => 1])->update(['DefaultForProvider' => 0]);
        }
        else{   
            return self::where(['UserType' => trim($user_type), 'ProviderId'=> (int) $provider_id, 'FundCategoryId' => (int) $fund_category_id, 'DefaultForProvider' => 1])->update(['DefaultForProvider' => 0]);
        }
    }
 }