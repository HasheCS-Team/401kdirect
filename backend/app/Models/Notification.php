<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class Notification extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';
    
     /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';
    

    protected $fillable = ['SenderId', 'ReceiverId', 'Type', 'Seen', 'seenCount'];

    /*
     * @$s_id: integer, (SenderId, who generate the notification)
     * @$r_id: integer, (ReceiverId, who will receive the notification)
     * @$type: string, Type (i.e. rate, comment)
     * @$s: boolean, Seen, (User seen notification or not, usually click on notifiaction and jump to its content)
     * @$s_c: integer, (SeenCount, number of notifications received count, once clicked, it should become zero)
     * @return:
     */
    public static function saveNotification($s_id, $r_id, $type, $s, $s_c)
    {
        return static::create(['SenderId' => $s_id, 'ReceiverId' => $r_id, 'Type' => $n_type, 'Seen' => $s, 'seenCount' => $s_c]);
    }
}