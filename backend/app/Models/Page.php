<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class Page extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages'; 

        /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'MenuName', 'MenuTitle', 'MenuOrder', 'MenuCssClasses', 'PageId' ,'PageCode', 'PageLink', 'PageDescription', 'IsActive', 'CreatedBy', 'UpdatedBy'
    ];

    public static $create = [
        /*'MenuName' => 'required|max:45',
        'MenuTitle' => 'max:45',
        //'MenuOrder' => 'Integer',
        'MenuCssClasses' => 'required|max:45',
        //'PageId' => 'integer',
        'PageCode' => 'required|integer',
        'PageLink' => 'required|max:100',
        'PageDescription' => 'max:250',*/
    ];

    public function scopeDataTable($query)
    {
        $query->select(['Id', 'MenuName', 'MenuTitle', 'PageId', 'MenuOrder', 'PageCode', 'PageLink', 'IsActive', 'IsDeleted']);
    }

    public function scopeEdit($query)
    {
        $query->select(['Id',
                        'MenuName', 
                        'MenuTitle',
                        'MenuOrder',
                        'MenuCssClasses',
                        'PageId',
                        'PageCode',
                        'PageLink',
                        'PageDescription',
                        'IsActive'
                    ]);
    }

    public function scopeDropDown($query)
    {
        $query->select(['Id', 'MenuName'])->orderBy('Id','desc');
    }

    //Mutators for IsActive Attribute
    public function setIsActiveAttribute($value)
    {
        $this->attributes['IsActive'] = ($value == "true" ? 1 : 0 );
    }

    //Accessor for IsActive Attribute
    public function getIsActiveAttribute($value)
    {
        return $value == 1 ? true : false;
    }

    //Relation with
    public function permissions()
    {
        return $this->hasMany('App\Models\Permission', 'PageId', 'Id')->select(['Id','PermissionName']);
    }

    //Get Permission Name
    public function permissionNameList()
    {
        return $this->hasMany('App\Models\Permission', 'PageId', 'Id')->select(['PermissionName']);
    }

    //Get all Pages with MenuName
    public static function scopePages($query)
    {
        $query->select('Id', 'MenuName','PageId')->orderBy('Id','asc');
    }

    //Count Total Parents
    public function scopeParentCount($query, $id=0)
    {
        $query->select('PageId')->where('PageId','=',$id)->get(); 
    }

    //Count Total Childs
    public function scopeChildCount($query, $id=0)
    {

        $query->select('PageId')->where('PageId','=',$id)->get();   
    }

    //Get all Childs
    public function scopeFindChlid($query, $id=0)
    {
        $query->select('PageId','MenuOrder', 'MenuName')->where('PageId','=',$id)->orderBy('MenuOrder','asc');  
    }

    //Get Page Name
    public static function getPageName($id)
    {
        $page = self::select('MenuName')->where('Id','=',$id)->orderBy('MenuName','asc')->first();
        return $page ? $page->MenuName : '';
    }

    //Relation with Page
    public function scopePage()
    {
        //return $this->belongsTo(self::class, 'PageId');
        return $this->belongsTo('App\Models\Page', 'PageId');
    }

    public function scopeAllMenus($query, $id=0)
    {
        $result = $query->select('Id','MenuName','PageId')->where('PageId','=',$id)->orderBy('MenuOrder','asc');

        foreach ($result as $data) {
            
            //dd($data->MenuName);
            $this->scopeAllMenus($query,$data->Id);
        }

        //$this->allMenus($query,0);
    }

    public static function test($id , $ForSubAdminCreatePage = '')
    {
        $pages = \DB::select("SELECT p.* FROM roles AS r 
                             INNER JOIN user_role AS ur ON r.Id = ur.RoleId
                             INNER JOIN users AS u ON u.Id = ur.UserId
                             INNER JOIN role_permissions AS rps ON r.Id = rps.RoleId
                             INNER JOIN permissions AS ps ON ps.Id = rps.PermissionId 
                             INNER JOIN pages AS p ON p.Id = ps.PageId 
                             WHERE u.Id = $id  AND p.IsActive = 1 order by p.MenuOrder ");
        //this condition call only when sidebar shown in subadmin create page
        if(!empty($ForSubAdminCreatePage) && $ForSubAdminCreatePage !='')
        {
            return self::sidemenu(collect($pages), 0);
        }
        else{
            return self::menu(collect($pages), 0);
        }
    }

    public static function test1($id )
    {

        $pages = \DB::select("SELECT p.* FROM permissions AS ps 
                             INNER JOIN user_permissions AS ups ON ps.Id = ups.PermissionId
                             INNER JOIN users AS u ON u.Id = ups.SubAdminId
                             INNER JOIN pages AS p ON p.Id = ups.PageId 
                             WHERE u.Id = $id  AND p.IsActive = 1 order by p.MenuOrder ");
        //this condition call only when sidebar shown in subadmin create page
        return self::menu(collect($pages), 0);
    }

    private static function menu($pages, $id)
    {
        $page = $pages->where('PageId', $id);

        $main = [];

        foreach ($page as $p)
        {
            $sub['details'] = ['name' => $p->MenuName, 'title' => $p->MenuTitle, 'link' => $p->PageLink,'MenuCssClasses' => $p->MenuCssClasses, 'MenuCssArrow' => $p->MenuCssArrow];

            $sub['sub'] = self::menu($pages, $p->Id);

            $main[] = $sub;
        }

        return $main;
    }

    private static function sidemenu($pages, $id)
    {
        $page = $pages->where('PageId', $id);
        $alldata = [];
        foreach ($page as $p)
        {
            $sub =  ['Pagename' => $p->MenuName, 'Id' => $p->Id, 'Create' => false, 'View' => false, 'Update' => false, 'Del' => false];
            $alldata[] = $sub;
        }

        return $alldata;
    }
}