<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PdfRefrences extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pdf_refrences';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';


    public static function getPdfRefrence($hashe_key)
    {
        return self::where('refrenceNumber', $hashe_key)->first();
    }

    public static function updateSerializeString($hashe_key, $serialize_string)
    {
        $pdfRefrence = self::where('refrenceNumber', $hashe_key)->first();
        
        if ( $pdfRefrence == null )
            return false;

        $pdfRefrence->serializeString = $serialize_string;
        
        if ( $pdfRefrence->save() )
            return true;

        return false;     
    }
}
