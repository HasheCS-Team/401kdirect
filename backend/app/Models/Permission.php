<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;
use App\Models\User_Permissions as UP;

class Permission extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['PermissionTitle', 'PermissionName', 'PermissionCode', 'Description', 'level', 'ParentId', 'type', 'Action', 'CreatedBy', 'UpdatedBy'];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_permissions', 'PermissionId', 'SubAdminId')->withTimestamps();
    }


    //validations...create/update
    public static function getPermissionValidations($id = null)
    {
        if ( $id )
        {
            return [
                'PermissionTitle' => 'required|max:50|unique:permissions,PermissionTitle,'. $id,
                'PermissionName' => 'required|max:50|unique:permissions,PermissionName,'. $id,
                'PermissionCode' => 'required|integer',
                'Description' => 'max:250',
                'type' => 'required'
            ];
        }
        else
        {
            return [
                'PermissionTitle' => 'required|max:50|unique:permissions,PermissionTitle',
                'PermissionName' => 'required|max:50|unique:permissions,PermissionName',
                'PermissionCode' => 'required|integer',
                'Description' => 'max:250',
                'type' => 'required'
            ];
        }
    }


    /**
     * The Permission table coulumn against which acl middelware check its permission...
     *
     * @var string
     */
    const PERMISSION_NAME = 'PermissionName';

    /**
     * The Permission table coulumn against which acl middelware check its permission...
     *
     * @var string
     */
    const PERMISSION_CODE = 'PermissionCode';

    /**
     * The permissions that belong to the role.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_permissions', 'RoleId', 'PermissionId')->withTimestamps();
    }

    /**
     * The permissions that belong to the page.
     */
    public function pages()
    {
        return $this->belongsTo('App\Models\Page', 'PageId', 'Id');
    }

    /**
     * Select Columns For Datatables
     */
    function scopeDataTable($query)
    {
        $query->select(['Id', 'PermissionTitle', 'PermissionName', 'PermissionCode', 'type', 'Description']);
    }

    /**
     * Select Columns for Permission Dropdown
     */
    function scopeDropDown($query)
    {
        $query->select(['Id', 'PermissionName']);
    }

    /**
     * Select Columns for Permission Dropdown For Parent
     */
    function scopeDropDownParent($query)
    {
        $query->select(['Id', 'PermissionTitle'])->whereIn('type', ['menu', 'sub-menu']);
    }


    /**
     * Select Columns for Permission Dropdown For Parent
     */
    function scopeDropDownMenu($query)
    {
        $query->select(['Id', 'PermissionTitle'])->where('type', 'menu');
    }

    /**
     * Select Columns for Permission Dropdown For Parent
     */
    function scopeDropDownForRoles($query, $Id, $type)
    {
        $query->select(['Id', 'PermissionTitle'])->where('type', $type)->where('ParentId', $Id);
    }


    /**
     * Get Sub Menu List with Parent Id
     */
    public  static function getSubMenuActions($permission_id)
    {
        $permission = self::select('Id', 'Action', 'type', 'PermissionName', 'ParentId')->where('ParentId', $permission_id);

        # Add custom column you want by this command
        $data =  $permission->addSelect(DB::raw("'false' as IsCheck"))->get();

        $perms = $data->map(function($item) {

            return ['Id' => $item->Id, 'PermissionName' => $item->PermissionName, 'type' => $item->type, 'Action' => $item->Action, 'IsCheck' => ($item->PermissionName == 'profile' ? 'true': 'false') ];
        });
        
        return $perms ? $perms: 0;
    }

    /**
     * Get Sub Menu List with Parent Id on edit 
     */
    public  static function getEditSubMenuActions($permission_id,$Permission_name,$subuserid)
    {
        $editpermission = self::select('Id', 'Action', 'type', 'PermissionName', 'ParentId')->where('ParentId', $permission_id)->get();

        $perms = $editpermission->map(function($item) use ($subuserid){

                return ['Id' => $item->Id, 'PermissionName' => $item->PermissionName, 'type' => $item->type, 'Action' => $item->Action, 'IsCheck' => UP::getSUbPermission($item->PermissionName,$subuserid) ];
            });

        $result = array_filter($perms->toArray());
        $data =  $result;
        
        return $data ? $data: 0;
    }

    /**
     * Select Columns for Permission Dropdown For Parent
     */
    function scopeDropDownForSubRoles($query, $Id)
    {
        $query->select(['Id', 'PermissionTitle'])->whereIn('type', ['api', 'sub-menu'])->where('ParentId', $Id);
    }



    /**
     * Edit Columns for Edit Permission
     */
    function scopeEdit($query)
    {
        $query->select(['Id','PermissionTitle', 'PermissionName', 'type', 'Action', 'ParentId', 'PermissionCode', 'Description']);
    }

    /**
     * Check Roles and Check Permissions Again Permission Code
     */
    static function checkPermissions($pCode,$roles)
    {
        $flag = false;

        foreach ( $roles as $role )
        {
            if ( $role->permissions->where(Permission::PERMISSION_CODE, $pCode)->count() )
            {
                $flag = true;
                break;
            }
        }
        return $flag;
    }
}