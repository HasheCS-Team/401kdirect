<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'broker_dealer_id', 'fee', 'is_required_product', 'is_include_provider_service', 'fund_categories', 'total_default_percentage', 'pdf_title', 'disclaimer_text', 'logo', 'page_image', 'status', 'is_completed', 'created_by'];

    /**
     * This is used to get providers of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providers()
    {
        return $this->belongsToMany('App\Models\Users\Provider', 'product_providers', 'product_id', 'provider_id')->withTimestamps();
    }

    /**
     * This is used to get fund categories against specific product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fundCategories()
    {
        return $this->hasMany('App\Models\FundCategory', 'UserId', 'id');
    }

    /**
     * This is use to mapped categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryMapping()
    {
        return $this->hasMany('App\Models\ProductCategoryMapping', 'product_id', 'id');
    }

    /**
     * This is used to get products data against broker dealer
     *
     * @param $params
     * @return mixed
     */
    public static function getProductData($params)
    {
        $sql = self::select('id', 'broker_dealer_id', 'name', DB::raw("CONCAT(fee,'','%') AS fee"),
                        DB::raw("DATE_FORMAT(created_at, '%a, %b %d') as formatted_created_at"),
                        DB::raw("DATE_FORMAT(updated_at, '%a, %b %d') as formatted_updated_at"),
                        'deleted_at','is_completed'
                    );
        $sql->where('status', '=', 1)->where('broker_dealer_id', '=', $params['id'])->orderBy('created_at', 'desc');

        return $sql->get();
    }

    /**
     * This is used to get product count
     *
     * @param $params
     * @return mixed
     */
    public static function getProductCount($params)
    {
        return self::where('broker_dealer_id', '=', $params['brokerDealerId'])->where('status', '=', 1)->count();
    }

    /**
     * This is used to add validations for product add information
     *
     * @param string $id
     * @return array
     */
    public static function addProduct($id = '')
    {
        return [
            'fee' => 'required|between:0,99.99',
            'name' => 'required'
        ];
    }

    /**
     * This is used to get products by broker
     *
     * @param $id
     * @return mixed
     */
    public static function getProductsByBroker($id)
    {
        return self::select('id')->where('broker_dealer_id', '=', $id)->get()->toArray();
    }

    public static function getProductInfo($product_id)
    {
        return self::where('id', '=', $product_id)->first();
    }

    /**
     * This is used to check whether product is used by advisor during proposal
     *
     * @param $params
     * @return int
     */
    public static function isProductUsedByAdviser($params)
    {
        $sql = DB::table('benchmarking_results as br')
            ->select('br.ProductId')
            ->join('company_proposal_log as cpl', 'cpl.process_id', '=', 'br.ProcessId')
            ->join('products as p', 'p.id', '=', 'br.ProductId')
            ->where('br.AdvisorId', '=', $params['adviserId'])
            ->where('br.CompanyId', '=', $params['companyId'])
            ->where('p.is_completed', '=', 1)
            ->where('br.ProductId', '>', 0);
        $data = $sql->first();

        return $data;
    }

    /**
     * This is used to delete investment options
     *
     * @param $params
     * @return mixed
     */
    public static function deleteProductInvestmentOption($params)
    {
        $sql = DB::table('investment_options')->where('ProductId', $params['productId']);
        if (!empty($params['fundCategoryId'])) {
            $sql->whereIN('FundCategoryId', $params['fundCategoryId']);
        }
//        dd($params);
        if (!empty($params['providerId'])) {
            $sql->whereIN('providerId', $params['providerId']);
        }

        return $sql->delete();
    }

    public static function getProductInvestmentCost($advisor_id,$company_id, $provider_id){
        $bench_res = DB::table('benchmarking_results as br')
                        ->select('br.ProductId','br.FiduciaryServiceCharges','br.NetAssetsAmount','br.InvestmentCostAmount')
                        ->join('company_proposal_log as cpl', 'cpl.process_id', '=', 'br.ProcessId')
                        ->join('products as p', 'p.id', '=', 'br.ProductId')
                        ->where('br.AdvisorId', '=', $advisor_id)
                        ->where('br.CompanyId', '=', $company_id)
                        ->where('br.ProviderId', '=', $provider_id)
                        ->where('p.is_completed', '=', 1)
                        ->first();
        return ($bench_res) ? $bench_res : 0;
    }

    public static function getProductInvestmentCostNoProvider($advisor_id,$company_id)
    {
                $bench_res = DB::table('benchmarking_results as br')
                        ->select('br.ProductId','br.FiduciaryServiceCharges','br.NetAssetsAmount','br.InvestmentCostAmount')
                        ->join('company_proposal_log as cpl', 'cpl.process_id', '=', 'br.ProcessId')
                        ->join('products as p', 'p.id', '=', 'br.ProductId')
                        ->where('br.AdvisorId', '=', $advisor_id)
                        ->where('br.CompanyId', '=', $company_id)
                        ->where('p.is_completed', '=', 1)
                        ->first();
        return ($bench_res) ? $bench_res : 0;
    }

    public static function getProductETI($productId,$advisor_id,$company_id, $provider_id,$indexformate = true)
    {
        $respone['productId'] = $productId;
            if ( $productId > 0 ) {
                $Product = self::find($productId);
                if ( $Product->is_required_product == 1 || ($Product->is_required_product == 0 && $Product->is_include_provider_service == 1) ) {
                    $FiduciaryServices = self::getProductInvestmentCost($advisor_id,$company_id, $provider_id);
                    $respone['FSFee'] = $FiduciaryServices->FiduciaryServiceCharges;
                    $respone['ICAmmount'] = $FiduciaryServices->InvestmentCostAmount;
                    $respone['FSCharges'] = ($FiduciaryServices->FiduciaryServiceCharges/100)*$FiduciaryServices->NetAssetsAmount;
                }else{
                    $respone['productId'] = 0;
                }
                
            }
        return $respone;
    }

    /**
     * This is used to get product fund categories
     *
     * @param $params
     * @return mixed
     */
    public static function getProductFundCategory($params)
    {
        $sql = \DB::table('products as p')
            ->select('fc.Id', \DB::raw('LOWER(fc.Name) as fund_category'))
            ->join('fund_categories as fc', 'fc.UserId', '=', 'p.Id');
        $sql->where('p.id', '=', $params['id']);
        $sql->where('fc.UserType', '=', 'broker_dealer_product');

        return $sql->groupBy('fc.Name')->orderBy('fc.Name')->get();
    }

}
