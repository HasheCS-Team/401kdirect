<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryMapping extends Model
{
    /**
     * Default fillables
     *
     * @var array
     */
   protected $fillable = ['product_id', 'fund_category_name', 'mapped_category', 'default_percentage'];
}
