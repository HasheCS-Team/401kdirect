<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProposalLog extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'proposal_log';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'advisor_id';

}
