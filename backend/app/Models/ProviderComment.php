<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class ProviderComment extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provider_comments'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The name of the "created by" column.
     *
     * @var string
     */
    // /const CREATED_BY = 'CreateBy';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];


    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['AdvisorId', 'ProviderId', 'Name', 'Designation', 'CommentText' ,'CreatedBy','UpdatedBy'];

    //validations...create/update
    public static function getCommentValidations()
    {
            return [
                'Name' => 'required|max:50',
                'Designation' => 'required|max:50',
                'CommentText' => 'max:250'
            ];
    }

    
    /**
     * The Comment Belongs to Provider.
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Users\Provider','UserId');
    }
   
}