<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

//traits
use App\Traits\ModelCommon;

class ProviderFeeSchedules extends Model
{
    use ModelCommon;
    //primary key and table name
    protected $table = "provider_fee_schedules";
    protected $primaryKey = "Id";

    //timestamps
    const CREATED_AT = 'CreatedDate';
    const UPDATED_AT = 'UpdatedDate';

    //fields
    protected $fillable = ['ServiceModel', 'ProviderId' , 'Type', 'UserId', 'IsDeleted'];
}
