<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class ProviderRating extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provider_ratings'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The name of the "created by" column.
     *
     * @var string
     */
    // /const CREATED_BY = 'CreateBy';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['AdvisorId', 'ProviderId', 'QuestionId', 'Rating' ,'CreatedBy','UpdatedBy'];

    /**
     * The Rating Belongs to Provider.
     */
    public function provider()
    {
       return $this->belongsTo('App\Models\Users\Provider','UserId');
    }

    /**
     * The Rating Belongs to Provider.
     */
    public function ratingProviderQuestion()
    {
       return $this->belongsTo('App\Models\RatingProviderQuestion','Id');
    }

    // /**
    //  * The Rating Belongs to Advisor.
    //  */
    // public function advisor()
    // {
    //     return $this->belongsTo('App\Models\Users\Advisor', 'UserId', 'Id');
    // }

    public static  function getProviderAvgRating($provider_id){

        $provider_ratings = self::select( \DB::raw('ROUND(SUM(Rating)/COUNT(Rating), 2) AS AVG_COUNT, COUNT(Rating) AS TotalNumber, QuestionId'))
        ->where(['ProviderId'=>$provider_id])
        ->groupBy('QuestionId')
        ->get();
        
        $data = [
            'AVG_COUNT'=> 0,
            'TotalNumber'=> 0,
            'participant'=> [
                'AVG_COUNT'=>0
            ],
            'service'=> [
                'AVG_COUNT'=>0
            ],
            'technology' => [
                'AVG_COUNT' =>0
            ]
        ];

        if ( count($provider_ratings) > 0){
            $count = 0;
        
            for ( $i = 0; $i < count($provider_ratings); $i++ )
            {
                $row['AVG_COUNT'] = $provider_ratings[$i]->AVG_COUNT;
                
                $count = $provider_ratings[$i]->TotalNumber;
               
                if ( $provider_ratings[$i]->QuestionId == 1 )
                {
                    $data['technology'] = $row;
                }
               
                if( $provider_ratings[$i]->QuestionId == 2 ){
                    $data['service'] = $row;
                }
               
                if( $provider_ratings[$i]->QuestionId == 3 ){
                    $data['participant']=$row;
                }
            }
            
            $data['TotalNumber'] = $count;
            $data['AVG_COUNT'] = round(($data['technology']['AVG_COUNT'] + $data['service']['AVG_COUNT'] + $data['participant']['AVG_COUNT']) / $count, 2) ;
        }
       
        return $data;
    }

}