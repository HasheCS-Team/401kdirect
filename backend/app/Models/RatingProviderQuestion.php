<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use App\Traits\ModelCommon;

class RatingProviderQuestion extends Model
{
    //traits this model using...
    use ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rating_provider_questions'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "created by" column.
     *
     * @var string
     */
    // /const CREATED_BY = 'CreateBy';


     public function scopeListQuestion($query)
     {
        $query->select('Id','Questions');
     }

    /**
    * One to Many relation with question.
     *
     * @var string
     */
    public function providerRating()
    {
        return $this->hasMany('App\Models\ProviderRating', 'QuestionId', 'Id');
    }


    //public $timestamps = false;
   
}