<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;

class Role extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles'; 
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['RoleName', 'Description', 'IsActive', 'CreatedBy', 'UpdatedBy'];

    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_role', 'RoleId', 'UserId')->withTimestamps();
    }

    /**
     * The permissions that belong to the role.
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission','role_permissions','RoleId', 'PermissionId')->withTimestamps();
    }

    public static function getRoleValidations($id = null)
    {
        if ( $id )
        {
            return [
                'RoleName' => 'required|max:50|unique:roles,RoleName,' . $id,
                'Description' => 'max:50'
            ];
        }
        else
        {
            return [
                'RoleName' => 'required|max:50|unique:roles,RoleName',
                'Description' => 'max:50'
            ];
        }
    }

    public static function scopeEdit($query)
    {
        $query->select(['Id', 'RoleName', 'Description', 'IsActive']);
    }

    public static function scopeDropdown($query)
    {
        $query->select(['Id', 'RoleName']);
    }

    public static function scopeDataTable($query)
    {
        $query->select(['Id', 'RoleName', 'Description', 'CreatedDate', 'UpdatedDate']);
    }

    /**
    * This is used to get roles by name
    *
    * @param $name
    * @return mixed
    */

    public static function getRoleByName($name)
    {
        return self::select('Id')->where('RoleName', '=', $name)->first();
    }
}