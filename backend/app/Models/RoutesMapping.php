<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use App\Models\Users\Provider;

class RoutesMapping extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'routes_mapping'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     protected $fillable = [ 'Route', 'Description','CreatedBy','UpdatedBy','IsDeleted','DeletedBy'];

    public static function routesmappingValidation($id = null)
    {
        $arr = [
            'Route' => 'required|unique:routes_mapping,Route|min:1|max:200',
            'Description' => 'required|min:1|max:500',
        ];

        if ( $id )
        {
            $arr['Route'] = 'required|unique:routes_mapping,Route,'. $id;
            $arr['Description'] = 'required|min:1|max:500';
        }
        return $arr;
    }

    
 }