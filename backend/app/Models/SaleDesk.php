<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\User;
use App\Models\Broker as BD;

/**
 * This is sales desk model class
 *
 * Class SaleDesk
 * @package App\Models
 */
class SaleDesk extends Model
{
    protected $fillable = ['user_id', 'broker_id'];

    /**
     * This is used to get sales desk user for listing
     *
     * @param $params
     * @return mixed
     */
    public static function getSalesDesk($params)
    {
        $sql = DB::table('sale_desks as sd')
            ->select('sd.id', 'sd.logo', 'LastName', 'u.Id as user_id', 'u.IsVerified','FirstName','Email',
                DB::raw("CONCAT(FirstName, ' ', LastName) AS full_name"))
            ->join('users as u', 'u.Id', '=', 'sd.user_id')
            ->where('broker_id', '=', $params['brokerId'])->WhereNull('u.IsDeleted')->orderBy('created_at', 'desc');

        return $sql->get();
    }

    public function userId()
    {
        return $this->Id;
    }
    public static function saleDeskUserUpdateRules($id = null)
    { 
        $arr = ['FirstName' => 'required|min:3|max:20',
                'LastName' => 'min:3|max:20',
                'CellPhone' => 'required'];
        return $arr;  
    }
    //get total advisor where sale brokerdealer is equal to advisor brokerdealer
    public static function totaladvisor($saleDeskUserInfo) {
        $saleDeskBrokerDealerName = BD::find($saleDeskUserInfo->broker_id);
        $totalAdvisor = User::where('AccountType' , 'Advisor')->where('BrokerDealer', $saleDeskBrokerDealerName->firm_name)->where('IsVerified', '1')->count();
       return $totalAdvisor;
    }

    //get advisor from sale desk dashboard search
    public static function AdvisorSearch($saleDeskUserInfo , $searchString) {

        $saleDeskBrokerDealerName = BD::find($saleDeskUserInfo->broker_id);
        $SearchAdvisorResult = DB::select('select *, CONCAT(FirstName ," ",LastName) as Name FROM users WHERE AccountType = "Advisor" AND BrokerDealer = "'.$saleDeskBrokerDealerName->firm_name.'" AND IsVerified = "1" AND IsDeleted IS NULL AND (FirstName LIKE "%'.$searchString.'%" OR LastName LIKE "%'.$searchString.'% " OR Email LIKE "%'.$searchString.'%" )');
       return $SearchAdvisorResult;
    }

    public static $updateProfilePicture = [
        'uploadFile' => 'required|max:2000|mimes:jpg,jpeg,png,gif,bmp,tiff'
    ]; 

}
