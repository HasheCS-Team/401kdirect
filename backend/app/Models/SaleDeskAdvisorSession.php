<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use App\Models\Users\Provider;

class SaleDeskAdvisorSession extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sale_desk_advisor_sessions'; 

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'LoginTime';
    const UPDATED_AT = 'UpdatedAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     protected $fillable = [ 'AdvisorId', 'SaledeskId','IsDeleted'];

 }