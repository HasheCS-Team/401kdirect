<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

//traits
use App\Traits\ModelCommon;
use DB;

class Setting extends Model
{
    //traits this model using...
    use SoftDeletes, ModelCommon;

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Key', 'Value', 'Type', 'Status',  'CreatedBy', 'UpdatedBy'
    ];
    // protected $casts = [
    // 'Status' => 'boolean',
    //  ];
     
     public static $settingRules = [
        'Key' => 'required|max:150',
        'Value' => 'required|max:150',
        'Type' => 'required|in:admin,frontEnd'
    ];
    
    public function setStatusAttribute($value) 
    {
        $this->attributes['Status'] = ($value == "true" ? 1 : 0 );
    }

    public function setKeyAttribute($value) 
    {
        $this->attributes['Key'] = ucfirst($value);
    }
      
    public static function scopeDataTable($query)
    {    
        $query->select(['Id', 'Key', DB::raw("REPLACE(Value,',',', ') as Value"),'Type', 'Status', 'CreatedDate', 'UpdatedDate'])->orderBy('id', 'DESC');
    }
       
    public function scopeEdit($query)
    {   
        $query->select(['Key', 'Value', 'Type', 'Status']);
    }
}