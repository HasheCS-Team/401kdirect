<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TpaFlag extends Model{

    protected $table = 'tpa_flag';
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    protected $fillable = ['CompanyId', 'AdvisorId', 'Flag'];

    public static function getCompanyFlag($company_id, $advisor_id)
    {
        $sql = self::select('Flag')->where(['CompanyId' => $company_id, 'AdvisorId' =>  $advisor_id])->first();
        return $sql ? $sql->Flag : '';
    }
}
?>