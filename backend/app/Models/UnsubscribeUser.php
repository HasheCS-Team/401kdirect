<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;

use DB;

class UnsubscribeUser extends Model
{
    //traits this model using...
    use ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_unsubscribe';

    
    protected $fillable = ['Email'];

    public $timestamps = false;

    public static $unsubscribeEmailValidation = [ 
        'Email' => 'required|email|unique:user_unsubscribe,Email',
       ];
    
}