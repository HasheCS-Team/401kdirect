<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Usa_zips extends Model
{
    protected $table = 'usa_zips';

    public static function getStateDiff( $start, $end)
    {
        return DB::table('usa_zips')
                       ->select('State', 'ZipCode')
                       ->whereRaw('ZipCode BETWEEN ? and ?', [$start,$end] )
                       //->where('State', 'AK')
                       ->get();
    	// return self::select('State')
		   //      //->where('ZipCode', '>=', $start)
		   //      //->where('ZipCode', '<=', $end)
		   //      //->groupBy('State')
		   //      ->get();

    }

    /* get zip codes from starting and ending range */
    public static function zipCodeRange($start_range, $end_range)
    {
        $response = DB::table('usa_zips')
                       ->select('ZipCode')
                       ->whereBetween('ZipCode', array($start_range, $end_range));

        return $response ? $response->pluck('ZipCode') : 0;
    }

    public static function getAllZipFromRange($start, $end){
    	return self::select('State', 'ZipCode')
                ->where('ZipCode', '>=', $start)
                ->where('ZipCode', '<=', $end)
                ->groupBy('ZipCode')
                ->get();

    }

    /* get all zip codes of a state */
    public static function allStateZipCodes($state)
    {
        $response = DB::table('usa_zips')
                       ->select('ZipCode')
                       ->where('State', '=', $state)
                       ->distinct('ZipCode')
                       ->pluck('ZipCode');

        return $response;
    }

    public static function getState($zipcode)
    {
      $response = DB::table('usa_zips')
                       ->select('State')
                       ->where('ZipCode', $zipcode)
                       ->distinct('State')
                       ->first();

        return $response ? $response->State: '';
    }

    public static function getStateZipCodes($zipCode)
    {
        $response = DB::table('usa_zips')
                       ->select('State')
                       ->whereIn('ZipCode', $zipCode)
                       ->distinct('ZipCode')
                       ->pluck('State');

        return $response;
    }

    public static function getStateRangeZipCodes($zipCode)
    {
        $tempArry = [];

        foreach ($zipCode as $key => $value) {
          
          if( strpos(trim($value), '-') !== false  )
          {
              $tempArry[] = self::getRangesState(trim($value));  

          }
          elseif (strpos(trim($value), '-') === false ) 
          {
            $temp = DB::table('usa_zips')
                         ->select('State')
                         ->where('ZipCode', trim($value))
                         ->first(); 

            $obj = $temp ? $temp->State: '';

            if($obj)
            {
              array_push($tempArry, $obj);
            }
          }
        }

        return array_unique($tempArry);
    }

    public static function getRangesState($zipCode)
    {
      $zips = explode('-', $zipCode);

      if($zips[0])
      {
        $start = $zips[0];
        $end = $zips[1];

        $response = self::select('State')
                          ->where('ZipCode', '>=', $start)
                          ->where('ZipCode', '<=', $end)
                          ->groupBy('ZipCode')
                          ->first();

        return $response ? $response->State: '';
      }
    }
}
