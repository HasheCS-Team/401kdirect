<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Users\Provider;
use App\Models\Provider\Wholesaler;
use App\Models\AdvisorCompany;
use DB;
use Config;
use File;

//traits
use App\Traits\ModelCommon;

class User extends Authenticatable
{
    //traits this model using...
    use SoftDeletes, ModelCommon;
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['IsDeleted', 'UpdatedDate'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_user_id',
        'FirstName',
        'LastName',
        'Email',
        'Password',
        'Gender',
        'ProfileImage',
        'AccountType',
        'IsActive',
        'IsVerified',
        'BirthDate',
        'State',
        'City',
        'BusinessAddress',
        'BrokerDealer',
        'SSOBrokerDealer',
        'BrokerDealerId',
        'ZipCode',
        'OfficePhone',
        'CellPhone',
        'About',
        'CreatedBy',
        'PasswordResetToken',
        'UpdatedBy',
        'IsNewSession',
        'InvitationToken',
        'InvitedBy',
        'Status',
        'isApproved',
        'CountryCode',
        'ParentId',
        'Source'
    ];
    
    public static $changeEmailValidation = [ 
        'Email' => 'required|email|unique:users,Email',
       ];
    
    public static $firstNameValidation = [ 
        'FirstName' => 'required',
       ]; 

    public static function userAddedByAdmin($id = null)
    {
        $arr = [
            // 'Roles' => 'required',
            // 'Roles.*' => 'integer',
            'FirstName' => 'required|min:3|max:20',
            'LastName' => 'min:3|max:20',
            // 'Gender' => 'required|in:Male,Female',
            'State' => 'required',
            'City' => 'required',
            'ZipCode' => 'required',
            'BusinessAddress' => 'max:100',
            'CellPhone' => 'required',
            'BrokerDealer' => 'required',
            // 'ProfileImage' => 'max:2000|mimes:jpg,jpeg,png,gif,bmp,tiff'
        ];

        if ( $id )
        {
            $arr['Email'] = 'required|email|unique:users,Email,'. $id;
            $arr['Password'] = 'min:6|max:50';
            $arr['ConfirmPassword'] = 'same:Password';
        }
        else
        {
            $arr['Email'] = 'required|email|unique:users,Email';
        }

        return $arr;
    }

    public static function adminSubUserAdded($id = null)
    {
        $arr = [
            'FirstName' => 'required|min:3|max:20',
            'LastName' => 'min:3|max:20',
            'State' => 'required',
            'ZipCode' => 'required',
            'BusinessAddress' => 'max:100',
            'CellPhone' => 'required',
            'Password' => 'required|min:6|max:50',
        ];

        if ( $id )
        {
            $arr['Email'] = 'required|email|unique:users,Email,'. $id;
            $arr['Password'] = 'min:6|max:50';
        }
        else
        {
            $arr['Email'] = 'required|email|unique:users,Email';
        }

        return $arr;
    }

    //validation rules for signup...
    public static $signup = [
        'FirstName' => 'required|min:3|max:30',
        'LastName' => 'required|min:3|max:30',
        'Email' => 'required|email|unique:users,Email',
        'AccountType' => 'required|in:Advisor,Provider,TPA,IM,Provider Wholesaler,Sale Desk User',
    ];

    /**
     * This is used to add validations for broker dealer signup
     *
     * @param string $id
     * @return array
     */
    public static function brokerSignup($id = '')
    {
        return [
            'FirstName' => 'required|min:1|max:20',
            'LastName' => 'min:1|max:20',
            'State' => 'required',
            'City' => 'required',
            'ZipCode' => 'required',
            'BusinessAddress' => 'max:100',
            'CellPhone' => 'required',
            'Email' => 'required|email|unique:users,Email,' . $id
        ];
    }

    //validation rules for signup Provider...
    public static $signupProvider = [
        'FirmName'=>'required'
    ];

    /*
     * Front-End
     * User Profile Validation
     */
    public static function userProfileValidate()
    {
        $arr = [
            // 'Email' => 'required',
            'FirstName' => 'required',
            'LastName' => 'required'
        ];

        return $arr;
    }

    /*
     * Front-End
     * User Profile Validation
     */
    public static function userProfileImageValidate()
    {
        $arr = [
            'ProfileImage' => 'max:2000|mimes:jpg,jpeg,png,gif,bmp'
        ];

        return $arr;
    }

    //validation rules for login...
    public static $login = [ 'Email' => 'required|email', 'Password' => 'required' ];

    //validation rules for forget password request...
    public static $email = ['Email' => 'required|email'];
    
    //validation rules for after forget password email, reset password...
    public static $resetPassword = ['Password' => 'required|min:6|max:50', 'ConfirmPassword' => 'required|same:Password'];

    //ADMIN side...
    // public static $updateBasicInfo = [
    //     'FirstName' => 'required|min:3|max:30',
    //     'LastName' => 'required|min:3|max:30',
    //     'Email' => 'required|email|unique:users,Email',
    //     'Gender' => 'required|in:Male,Female',
    //     'State' => 'required',
    //     'City' => 'required',
    //     'ZipCode' => 'required',
    //     'BusinessAddress' => 'max:100',
    //     'CellPhone' => 'required'
    // ];
     public static function updateBasicInfoProvider($id = null)
        {
            $arr = [
                'FirstName' => 'required|min:1|max:20',
                'LastName' => 'min:1|max:20',
                'State' => 'required',
                'City' => 'required',
                'ZipCode' => 'required',
                'BusinessAddress' => 'max:100',
                'CellPhone' => 'required',
            ];
            
            return $arr;
        }
    public static function updateBasicInfo($id = null)
    {
            // 'Gender' => 'required|in:Male,Female',
        $arr = [
            'FirstName' => 'required|min:1|max:20',
            'LastName' => 'min:1|max:20',
            'State' => 'required',
            'City' => 'required',
            'ZipCode' => 'required',
            'BusinessAddress' => 'max:100',
            'CellPhone' => 'required',
        ];

        if ( $id )
        {
            $arr['Email'] = 'required|email|unique:users,Email,'. $id;
        }
        else
        {
            $arr['Email'] = 'required|email|unique:users,Email';
        }
        return $arr;
    }

    //ADMIN side...
    public static $updateProfilePicture = [
        'ProfileImage' => 'required|max:2000|mimes:jpg,jpeg,png,gif,bmp,tiff'
    ];

    //ADMIN side...
    public static $updatePassword = [
        'CurrentPassword' => 'required|min:6|max:50',
        'NewPassword' => 'required|min:6|max:50',
        'ConfirmPassword' => 'required|same:NewPassword'
    ];

    public static function scopeFrontEndUser($query, $email)
    {
        $query->where('Email', $email);
    }

    public static function scopeDataTable($query)
    {
        $query->select(['Id', 'FirstName','LastName','Email', 'Gender', 'BirthDate', 'IsActive', 'IsVerified','AccountType','isApproved'])
              ->orderBy('Id','DESC');
    }

    public static function scopeEdit($query)
    {
        $query->select(['Id',
                        'FirstName',
                        'LastName',
                        'Email',
                        'Gender',
                        'ProfileImage',
                        'BrokerDealer',
                        'State',
                        'City',
                        'BusinessAddress',
                        'ZipCode',
                        'OfficePhone',
                        'CellPhone',
                        'AccountType',
                        'ParentId',
                    ]);
    }

    public static function scopeShow($query)
    {
        $query->select(['Id',
                        'FirstName',
                        'LastName',
                        'Email',
                        'Gender',
                        'ProfileImage',
                        'State',
                        'City',
                        'BusinessAddress',
                        'BrokerDealer',
                        'ZipCode',
                        'OfficePhone',
                        'AccountType',
                        'CreatedDate',
                        'UpdatedDate',
                        'CellPhone',
                        'About',
                        'CreatedBy',
                        'UpdatedBy'
                    ]);
    }

    public static function scopeFirstLastName($query, $id)
    {
        $query->select(['FirstName', 'LastName'])->where('Id', $id);
    }

    public static function scopeFullname($query, $id)
    {
        $query->select(\DB::raw("CONCAT(FirstName, ' ', LastName) AS Name"))
              ->where('Id', $id);
    }

    public function scopeSearch($query, $param)
    {
        $query->where('Email', 'LIKE', "%$param%")
              ->orWhere('FirstName', 'LIKE', "%$param%")
              ->orWhere('LastName', 'LIKE', "%$param%")
              ->select(['Id', 'Email']);
    }

    public function scopeProviderAutoFill($query)
    {
       $query->select('Id', 'Email');
    }

    // public function setIsVerifiedAttribute($value)
    // {
    //     $this->attributes['IsVerified'] = ($value == "active" ? 1 : 0 );
    // }

    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'user_role', 'UserId', 'RoleId')->withTimestamps();
    }

    /**
     * The permissions that belong to the user.
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'user_permissions', 'SubAdminId', 'PermissionId')->withTimestamps();
    }

    /**
     * Get Current logged In user Id...
     */
    public function userId()
    {
        return $this->Id;
    }

    /**
     * Get the Advisor specific user record associated with the user.
     */
    public function advisor()
    {
        return $this->hasOne('App\Models\Users\Advisor', 'UserId', 'Id')
        ->select(['UserId', 'FirmName', 'NumberOfPlans', 'TotalPlanAssets','BrokerDisclaimer', 'PdfTitle']);
    }

    /**
     * Get the provider specific user record associated with the user.
     */
    public function provider()
    {
        return $this->hasOne('App\Models\Users\Provider', 'UserId', 'Id');
    }

    /**
     * Get the broker specific user record associated with the user.
     */
    public function brokerDealer()
    {
        return $this->hasOne('App\Models\Broker', 'user_id', 'Id');
    }

    /**
     * Get the wholesaler specific user record associated with the user.
     */
    public function wholesaler()
    {
        return $this->hasOne('App\Models\Provider\Wholesaler', 'UserId', 'Id');
    }

    /**
     * Get the Investment Manager specific user record associated with the user.
     */
    public function investmentManager()
    {
        return $this->hasOne('App\Models\Users\InvestmentManager', 'UserId', 'Id')->select('UserId','Logo', 'FirmName', 'Description');
    }

    /**
     * Get the Investment Manager wholesaler specific user record associated with the user.
     */
    public function investmentManagerWholesaler()
    {
        return $this->hasOne('App\Models\InvestmentManager\Wholesaler', 'UserId', 'Id');
    }

    /**
     * Get the Investment Manager wholesaler specific user record associated with the user.
     */
    public function saleDeskUser()
    {
        return $this->hasOne('App\Models\SaleDesk', 'user_id', 'Id');
    }

    public function userType()
    {
        return $this->AccountType;
    }

    public static function getProviderData($ids, $advisor_id, $company_id)
    {
        //get current provider is seletced
        $currentProviderCount = 0;
        $CurrentProvideresult = AdvisorCompany::select('OldCurrentProvider')->where('Id', $company_id)->first();
        $CurrProvideresult = $CurrentProvideresult->OldCurrentProvider;
        if (in_array($CurrProvideresult, $ids)) {
            $currentProviderCount = 1;
            unset($ids[array_search($CurrProvideresult, $ids)]);
        }
        //$response = self::select('Id', 'FirstName', 'LastName', 'Email')->whereIn('id', $ids)->get();
        $response = self::select('users.Id', 'users.FirstName', 'users.LastName', 'users.Email', 'p.FirmName')
            ->join('providers as p', 'p.UserId', '=', 'users.Id')
            ->whereIn('id', $ids)->get();
        foreach ($response as $key => $value) {
            $wholesaler = DB::select("CALL sp_proposal_wholesaler_assignment($advisor_id, $company_id, $value->Id)");

            if (count($wholesaler) > 0) {
                $response[$key]->Wholesaler = DB::table('users as u')
                    ->select(DB::raw('CONCAT(FirstName, " ", LastName) AS Name'), 'u.Email')
                    ->where('u.Id', $wholesaler[0]->WholesalerId)
                    ->first();

                $wholesaler_status = $wholesaler[0]->STATUS;
                $other_matched_wholesaler = DB::select("CALL sp_wholesaler_internal_external_match($advisor_id, $company_id, $value->Id, '$wholesaler_status')");

                $priority  = Provider::showPriority($value->Id);

                if (count($other_matched_wholesaler) > 0) {
                    $response[$key]->MatchedWholesalerData = $other_matched_wholesaler[0];
                } else {
                    $response[$key]->MatchedWholesalerData = null;
                }

                if ($priority == 'i' && $wholesaler[0]->STATUS !== 'Internal' && $response[$key]->MatchedWholesalerData &&  $response[$key]->Wholesaler) {
                     $wholesaler = $response[$key]->Wholesaler;
                     $response[$key]->Wholesaler = $response[$key]->MatchedWholesalerData;
                     $response[$key]->MatchedWholesalerData = $wholesaler;
                }

            } else {
                $response[$key]->Wholesaler = "not found";
            }
        }
        if (isset($currentProviderCount) && $currentProviderCount == 1) {
            if ($CurrProvideresult != 0) {
                //get provider data of already selected
                $providersData = self::select('users.Id', 'users.FirstName', 'users.LastName', 'users.Email', 'p.FirmName')
                    ->join('providers as p', 'p.UserId', '=', 'users.Id')
                    ->where('p.UserId', $CurrProvideresult)->first();
                //get provider rm wholesaler

                $rmData = DB::table('providers_wholesaler as pw')
                    ->join('users as u', 'u.Id', '=', 'pw.UserId')
                    ->join('providers as p', 'p.UserId', '=', 'pw.ProviderId')
                    ->select('u.Id', 'u.FirstName', 'u.LastName', 'u.Email', 'pw.Status', DB::raw("IF(pw.STATUS = 'External' , IF(p.WholesalerPriority = 'e', 3, 2),IF(pw.STATUS = 'Internal', IF(p.WholesalerPriority = 'i', 3, 2), IF(pw.STATUS = 'Default_Wholesaler',1,0) )) AS priority"))
                    ->where('pw.ProviderId', $CurrProvideresult)
                    ->where('pw.IsRmWholesaler', 1)
                    ->where('u.IsVerified', 1)
                    ->get();

                //if rm wholesaler exist then  pick rm wholsaler
                if (count($rmData) > 0) {
                    if (count($rmData) == 1) {
                        $providerData = clone ($providersData);
                        $value = reset($rmData);
                        $providerData->Wholesaler = (object) [
                            'Name' => $value->FirstName . " " . $value->LastName,
                            'Email' => $value->Email,
                        ];
                        $response[count($response)] = $providerData;
                    } else {

                        $providerData = clone ($providersData);
                        $maxIndex = array_search(max(array_column($rmData, 'priority')), array_column($rmData, 'priority'));
                        $maxIndexValue = $rmData[$maxIndex];
                        $providerData->Wholesaler = (object) [
                            'Name' => $maxIndexValue->FirstName . " " . $maxIndexValue->LastName,
                            'Email' => $maxIndexValue->Email,
                        ];

                        $minIndex = array_search(min(array_column($rmData, 'priority')), array_column($rmData, 'priority'));
                        $minIndexValue = $rmData[$minIndex];

                        $providerData->MatchedWholesalerData = (object) [
                            'Name' => $minIndexValue->FirstName . " " . $minIndexValue->LastName,
                            'Email' => $minIndexValue->Email,
                        ];
                        $response[count($response)] = $providerData;
                    }
                }
                //if rm not exist then pick default wholesaler
                else {
                    $defaultWholesaler = DB::table('providers_wholesaler as pw')
                        ->join('users as u', 'u.Id', '=', 'pw.UserId')
                        ->select('u.Id', 'u.FirstName', 'u.LastName', 'u.Email')
                        ->where('pw.ProviderId', $CurrProvideresult)
                        ->where('pw.Status', 'Default_Wholesaler')
                        ->first();
                    if (count($defaultWholesaler) > 0) {
                        $providersData->Wholesaler = (object) [
                            'Name' => $defaultWholesaler->FirstName . " " . $defaultWholesaler->LastName,
                            'Email' => $defaultWholesaler->Email,
                        ];
                        $response[count($response)] = $providersData;
                    }
                }
            }
        }
        return $response;
    }

    public static function getUserData($id)
    {
       return self::select('FirstName','LastName', 'Email', 'BrokerDealer', 'AccountType')->where('id', $id)->first();
    }

    // For Multiple users
    public static function getUsersData($ids)
    {
       return self::select('Id', 'FirstName', 'LastName', 'Email')->whereIn('id', $ids)->get();
    }

    public static function getwholesalerData($id)
    {
       return self::select('Id', 'FirstName','LastName', 'Email')->where('id', $id)->first();
    }

    public static function scopeNameList($query, $ids = [])
    {
        $query->select('Id', \DB::raw("CONCAT(FirstName, ' ', LastName) AS Name"))
              ->whereIn('Id', $ids);
    }

    public static function getUserId($email)
    {
        return self::select('Id')->where('Email', $email)->first();
    }

    public static function dashboardCountSuperAdmin()
    {
        return DB::select('SELECT (SELECT COUNT(*) FROM users AS u WHERE u.IsDeleted IS NULL) AS userCount,
                            (SELECT COUNT(*) FROM advisor_companies AS ac) AS advisorCount, (SELECT COUNT(*) FROM collection_third_party_administrators AS cta WHERE cta.IsDeleted IS NULL) AS ctaCount,
                            (SELECT COUNT(*) FROM investment_managers AS im WHERE im.IsDeleted IS NULL) AS imCount,
                            (SELECT COUNT(*) FROM providers AS p WHERE p.IsDeleted IS NULL) AS pCount,
                            (SELECT COUNT(*) FROM advisors AS a WHERE a.IsDeleted IS NULL) AS aCount');    
    }

    public static function cronProviderDefaultWholesaler () 
    {
        $providers = Provider::leftJoin('providers_wholesaler as pw', 'providers.UserId', '=', 'pw.ProviderId')
        ->select('providers.UserId as providerUserId', 'pw.UserId as providerWholeUserId', 'pw.Status')
        ->groupBy('providers.UserId')
        ->get();

        foreach ($providers as $key => $p) {
            $where = [
                'ProviderId'    => $p->providerUserId,
                'Status'        => 'Default_Wholesaler'
            ];
            $check = DB::table('providers_wholesaler')->where($where)->count();

            if ($check == 0) 
            {
                $user = User::find((int)$p->providerUserId);
                if ($user) {
                    $user_pw = $user->replicate(['AccountType','Email']);
                    $user_pw->Email = strstr($user->Email, '@', true).'@default.com';
                    $user_pw->AccountType = 'Provider Wholesaler';
                    if ($user_pw->save()){
                        $defaultWholesaler = Wholesaler::create([
                            'UserId'        => $user_pw->Id,
                            'ProviderId'    => (int)$p->providerUserId,
                            'Status'        => 'Default_Wholesaler'
                        ]);

                        print_r($defaultWholesaler->Status);
                    }
                }
            }
        }
    }
}
