<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//traits
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ModelCommon;

use DB;

class UserLogin extends Model
{
    //traits this model using...
    use ModelCommon;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_login';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['CreatedDate', 'UpdatedDate'];

    
    protected $fillable = ['ParentId', 'SubAdminId', 'UserType', 'IsLogin'];    
}