<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class User_Permissions extends Model
{
    protected $table = 'user_permissions';
    protected $primaryKey = 'Id';
    const CREATED_AT = 'CreatedDate';
    const UPDATED_AT = 'UpdatedDate';

    protected $fillable = [ 'ParentId', 'SubAdminId', 'PageId','PermissionId','ChildPermissions','Type','PageName','Action','ParentPermission','PermissionName','CreatedBy', 'UpdatedBy'];


     public static function getSUbPermission($permisson_name,$subuserid)
    {
    	if($permisson_name == 'profile')
    	{
    		return true;
    	}
        return self::select('Id')->Where('PermissionName', $permisson_name)->Where('SubAdminId', $subuserid)->exists();
    }

    
}
