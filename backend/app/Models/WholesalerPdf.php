<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class WholesalerPdf extends Model
{
    protected $table = 'provider_wholesaler_uploaded_files';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'IsDeleted';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $fillable = ['AdvisorId', 'CompanyId', 'ProviderWholesalerId', 'FileName', 'OriginalFileName', 'Size', 'CreatedBy', 'UpdatedBy']; 
}
