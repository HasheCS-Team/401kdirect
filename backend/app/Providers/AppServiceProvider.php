<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Log;
use DB;
use Validator;
use App\Validations\FileValidation as FileValidation;

use Illuminate\Database\Eloquent\Relations\Relation;
use App\Models\AdvisorCompany;
use App\Models\Users\Provider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //custom validations...
        Validator::extend('is_pdf_file', 'App\Validations\FileValidation@isPdfFileExtend');
        Validator::replacer('is_pdf_file', 'App\Validations\FileValidation@isPdfFileReplacer');
        Validator::extend('is_excel_or_csv_file', 'App\Validations\FileValidation@isExcelorCSVFileExtend');
        Validator::extend('is_postal_code_valid', 'App\Validations\PhoneZipCodeValidation@isPostalCodeValidExtend');
        Validator::extend('is_phone_number_valid', 'App\Validations\PhoneZipCodeValidation@isPhoneNumberValidExtend');
        Validator::extend('email_verification', 'App\Validations\CompanyValidation@isEmailVerified');
        Validator::extend('zip_verification', 'App\Validations\ZipCodeValidation@inZipVerified');
        Validator::extend('im_zip_verification', 'App\Validations\ZipCodeValidation@iMZipVerified');
        // Validator::replacer('zip_verification', 'App\Validations\ZipCodeValidation@inZipVerifiedReplacer');
        Validator::extend('in_usa_zip_exits', 'App\Validations\ZipCodeValidation@inUsaZipExits');
        Validator::extend('in_between_required_length', 'App\Validations\GeneralValidation@CheckMinMaxLength');

        Validator::extend('email_change_request_id', 'App\Validations\GeneralValidation@EmailChangeIDValidation');
        Validator::extend('default_fund_status', 'App\Validations\GeneralValidation@defaultFundStatus');

        Validator::extend('html_tags_check', 'App\Validations\HtmlTagsValidation@checkHtmlTagExist');


        DB::listen(function ($query) {
            // $query->sql
            // $query->bindings
            // $query->time

            // dd($query);

            Log::debug($query->sql . ' ' . $query->time);
        });

        // dd(Provider::TYPE_RELATION_VALUE, AdvisorCompany::TYPE_RELATION_VALUE);

        // dd(Provider::TYPE_RELATION_VALUE , Provider::class);
        // dd(AdvisorCompany::TYPE_RELATION_VALUE , AdvisorCompany::class);

        Relation::morphMap([
            Provider::TYPE_RELATION_VALUE => Provider::class,
            AdvisorCompany::TYPE_RELATION_VALUE => AdvisorCompany::class
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
