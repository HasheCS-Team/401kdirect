<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\SomeListener',
        ],
        'App\Events\SignUpEvent' => [
            'App\Listeners\SignUpListener',
        ],
        'App\Events\ProposalEmailEvent' => [
            'App\Listeners\ProposalEmailListener',
        ],
        'App\Events\ProposalShownEvent' => [
            'App\Listeners\ProposalShownListener',
        ],
        'App\Events\LoginEmailEvent' => [
            'App\Listeners\LoginEmailListener',
        ],
        'App\Events\ResetPasswordEvent' => [
            'App\Listeners\ResetPasswordListener',
        ],
        'App\Events\WholesalerResetPasswordEvent' => [
            'App\Listeners\WholesalerResetPasswordListener',
        ],
        'App\Events\ProviderConfirmEvent' => [
            'App\Listeners\ProviderConfirmListener',
        ],
        'App\Events\SignUpAccountVerifyEvent' => [
            'App\Listeners\SignUpAccountVerifyListener',
        ],
        'App\Events\ChangeRequestServiceEvent' => [
            'App\Listeners\ChangeRequestServiceListener',
        ],
        'App\Events\ChangeRequestInvestmentEvent' => [
            'App\Listeners\ChangeRequestInvestmentListener',
        ],
        'App\Events\InviteAdvisorSignupEvent' => [
            'App\Listeners\InviteAdvisorSignupListener',
        ],
        'App\Events\AdvisorActivationReminderEvent' => [
            'App\Listeners\AdvisorActivationReminderListener',
        ],
        'App\Events\ForgotPasswordEvent' => [
            'App\Listeners\ForgotPasswordListener',
        ],
        'App\Events\SendConfirmationEmail' => [
            'App\Listeners\SendConfirmationEmailListener',
        ],
        'App\Events\ProvidersAndInvestmentManagersEvent' => [
            'App\Listeners\ProvidersInvestmentManagersListener',
        ],
        'App\Events\InvestmentManagersEvent' => [
            'App\Listeners\InvestmentManagersListener',
        ],
        'App\Events\NotificationForChangeInvestmentEvent' => [
            'App\Listeners\NotificationForChangeInvestmentListener',
        ],
        'App\Events\WholesalerSignupEvent' => [
            'App\Listeners\WolesalerSignupListener',
        ],
        'App\Events\AccountActivationEvent' => [
            'App\Listeners\AccountActivationListener',
        ],
        'App\Events\ProposalStatusChange' => [
            'App\Listeners\ProposalStatusChangeListener',
        ], 
        'App\Events\NewAccountEvent' => [
            'App\Listeners\NewAccountListener',
        ],
        'App\Events\OpportunityReminderEvent' => [
            'App\Listeners\OpportunityReminderListener',
        ],
        'App\Events\AdvisorApprovedEmail' => [
            'App\Listeners\AdvisorApprovedEmailListener',
        ],
        'App\Events\AdvisorsInvitationEvent' => [
            'App\Listeners\AdvisorsInvitationListener',
        ],
        'App\Events\AdminInviteApprovedEvent' => [
            'App\Listeners\AdminInviteApprovedListener',
        ], 
        'App\Events\SignUpAdvisorAccountApproveEvent' => [
            'App\Listeners\SignUpAdvisorAccountApproveListener',
        ],
        'App\Events\InviteProviderWholesalerEvent' => [
            'App\Listeners\InviteProviderWholesalerListener',
        ], 
        'App\Events\ProposalDeleteEvent' => [
            'App\Listeners\ProposalDeleteListener',
        ], 
         'App\Events\UnsubscribeNewsletterEvent' => [
            'App\Listeners\UnsubscribeNewsletterListener',
        ],
        'App\Events\EmailChangeRequestEvent' => [
            'App\Listeners\EmailChangeRequestListener',
        ],
        'App\Events\ApproveEmailChangeRequestEvent' => [
            'App\Listeners\ApproveEmailChangeRequestListener',
        ],
        'App\Events\DisapproveEmailChangeRequestEvent' => [
            'App\Listeners\DisapproveEmailChangeRequestListener',
        ],'App\Events\SSOEvent' => [
            'App\Listeners\SSOListener',
        ],
        'App\Events\OpportunityAssignedEvent' => [
            'App\Listeners\OpportunityAssignedListener',
        ],
        'App\Events\OpportunityStatusEmailEvent' => [
            'App\Listeners\OpportunityStatusEmailListener',
        ],
        'App\Events\ProviderConnectionEvent' => [
            'App\Listeners\ProviderConnectionListener',
        ],
        'App\Events\PartnershipRequestEmailEvent' => [
            'App\Listeners\PartnershipRequestEmailListener',
        ],
        'App\Events\ProposalResumeEvent' => [
            'App\Listeners\ProposalResumeEventListener',
        ],
        'App\Events\ResetProposalEvent' => [
            'App\Listeners\CompanyProposalLogEventListener',
            'App\Listeners\ResetProposalFundsEventListener',
            'App\Listeners\ResetBenchmarkResultsEventListener',
            'App\Listeners\ResetBenchmarksEventListener',
            'App\Listeners\BenchmarkDefaultResultsEventListener',
            'App\Listeners\ResetProposalNotificationsEventListener',
            'App\Listeners\ResetCampaignEventListener'
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        \Event::listen('Aacotroneo\Saml2\Events\Saml2LoginEvent', function (\Aacotroneo\Saml2\Events\Saml2LoginEvent $event) {
//           dd($event);
//            createAndAuthenticateUser($data);
        });
        //
    }
}
