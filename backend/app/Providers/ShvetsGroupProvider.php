<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use ShvetsGroup\LaravelEmailDatabaseLog\EmailLogger;

class ShvetsGroupProvider extends EventServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        MessageSending::class => [
            EmailLogger::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

//         $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
    }
}
