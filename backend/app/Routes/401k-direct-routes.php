<?php

Route::group(['prefix' => 'api', 'middleware' => 'AllowCors'], function () {
    
    Route::get('dol-companies/{search}', 'Direct\\BenchmarkController@dolCompanies');
    Route::get('my-plan-providers-and-tpa', 'Direct\\BenchmarkController@myPlanProviderAndTpa');
    Route::get('dol-company/{id}', 'Direct\\BenchmarkController@dolCompany');
    Route::post('benchmark', 'Direct\\BenchmarkController@index');
    Route::post('myplan', 'Direct\\BenchmarkController@myPlan');

    Route::get('test/{pid}', 'Direct\\BenchmarkController@CalculateInvestmentCostPerformanceBenchMark');

});