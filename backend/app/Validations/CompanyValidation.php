<?php
namespace App\Validations;


use Request;
use App\Models\AdvisorCompany;

class CompanyValidation
{
	public static $successStatus = "ok";
	
    public function __construct()
    { 

    }

    public function isEmailVerified($attribute, $value, $parameters, $validator)
    {
        return checkdnsrr(explode("@", $value)[1]) ? true : false;
    }


    public static function  idValidation ($companyId,$adivosrId){


    	$msg_code = "ok";

    	if(is_null($companyId) || trim($companyId) == '' )
    	{
    		
    		$msg_code = "companyIdNull";
    	
    	}else
    	{

	    	if(!AdvisorCompany::where(['Id'=>$companyId,'AdvisorId'=>$adivosrId])->first())
	        {
	           $msg_code = "companyNotFound";
	        }
		}	

        return $msg_code;

	}

}