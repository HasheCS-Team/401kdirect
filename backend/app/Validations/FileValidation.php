<?php
namespace App\Validations;

use Request;

class FileValidation
{ 
    public function __construct()
    { 

    }

    public function isPdfFileExtend($attribute, $value, $parameters, $validator)
    {
        // $Validator->messages()->add($attribute, ['error message!', 'another error message']);
        // $validator->getMessageBag()->add($attribute, 'error message!');

    	if ( Request::hasFile($attribute) && Request::file($attribute)->isValid() )
    	{
        	return in_array($value->getMimeType(), getPdfMimeType());
    	}

    	return false;

        // dd($attribute, $value, $parameters, $value->getMimeType());
    }

    public function isPdfFileReplacer($message, $attribute, $rule, $parameters)
    {
    	//dd($message, $attribute, $rule, $parameters);
    	return $message;
    }

    public function isExcelorCSVFileExtend($attribute, $value, $parameters, $validator)
    {
    	if ( Request::hasFile($attribute) && Request::file($attribute)->isValid() )
    	{
        	return in_array($value->getClientMimeType(), getExcelMimeType()) && in_array($value->getClientOriginalExtension(), getExcelFileExtensions());
    	}

    	return false;
    }
}