<?php
namespace App\Validations;

use Request;
use DB;
use App\Models\User;
use App\Models\EmailChangeRequest;


class GeneralValidation
{	
	public function EmailChangeIDValidation($attribute, $value, $parameters, $validator)

    {
        $id = $parameters[0];
        $idCheck = DB::table('change_email_history')
                        ->where('Id',$id)
                        ->where('Status','requested')
                        ->first();

        if($idCheck){
            return true;
        }
        return false;
    }

    public function CheckMinMaxLength($attribute, $value, $parameters, $validator)
    {
        $min = $parameters[0];
        $max = $parameters[1];
        $strngLength = strlen($value);
        if ($strngLength >= $min && $strngLength <= $max){
           return true;
        } else {
            return false;
        }
    }

    public function defaultFundStatus($attribute, $value, $parameters, $validator)
    {
        $defaultFundId = $parameters[0];

        $defaultFundExistsCheck = DB::table('collection_default_mapping')
                                ->where('Id',$defaultFundId)
                                ->where('Status','Default')
                                ->first();

        if($defaultFundExistsCheck)
        {
            return true;
        }
        return false;
    }
}
