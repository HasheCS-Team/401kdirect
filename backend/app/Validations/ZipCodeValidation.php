<?php
namespace App\Validations;

use Request;
use DB;
use App\Models\Users\Provider;
use App\Models\Provider\Wholesaler;
use App\Models\Provider\WholesalerZips;

class ZipCodeValidation
{	
    public function inZipVerified($attribute, $value, $parameters, $validator)
    {   
        if( count($parameters) )
        {
            if( count($parameters) == 2 )
            {
                $provider_id = (int) $parameters[0];
                $wholesaler_id = (int) $parameters[1];
                
                $response_zips = implode(",", Db::table('providers_wholesaler as pw')
                    ->join('providers_wholesaler_zips as pwz', 'pw.UserId', '=', 'pwz.WholesalerId')
                    ->select('pwz.ZipCode')
                    ->where('pw.ProviderId', $provider_id)
                    ->where('pwz.WholesalerId', '!=', $wholesaler_id)
                    ->whereIn('pwz.ZipCode', $value)
                    ->pluck('pwz.ZipCode') );
            }
            else
            {
                $provider_id = (int) $parameters[0];

                $response_zips = implode(",", Db::table('providers_wholesaler as pw')
                    ->join('providers_wholesaler_zips as pwz', 'pw.UserId', '=', 'pwz.WholesalerId')
                    ->select('pwz.ZipCode')
                    ->where('pw.ProviderId', $provider_id)
                    ->whereIn('pwz.ZipCode', $value)
                    ->pluck('pwz.ZipCode') );
            }

        // if( WholesalerZips::whereIn('ZipCode', $value) )
        // {            
            // $response = implode(",", WholesalerZips::select('ZipCode')->whereIn('ZipCode', $value)->where('WholesalerId', '!=' , 136)->pluck('ZipCode')->toArray());
            
            if( count($response_zips) && !empty($response_zips) )
            {
                $validator->addReplacer('zip_verification', function($message, $attribute, $rule, $parameters) use ($response_zips) {
                    return str_replace([':zip'], $response_zips, $message);
                });

                return false;
            }
            else
            {
                return true;
            }
        }
    }


    public function iMZipVerified($attribute, $value, $parameters, $validator)
    {   
        if( count($parameters) )
        {
            if( count($parameters) == 2 )
            {
                $investment_manager_id = (int) $parameters[0];
                $wholesaler_id = (int) $parameters[1];
                
                $response_zips = implode(",", Db::table('investment_manager_wholesalers as imw')
                    ->join('investment_manager_wholesaler_zips as imwz', 'imw.UserId', '=', 'imwz.WholesalerId')
                    ->select('imwz.ZipCode')
                    ->where('imw.InvestmentManagerId', $investment_manager_id)
                    ->where('imwz.WholesalerId', '!=', $wholesaler_id)
                    ->whereIn('imwz.ZipCode', $value)
                    ->pluck('imwz.ZipCode') );
            }
            else
            {
                $investment_manager_id = (int) $parameters[0];

                $response_zips = implode(",", Db::table('investment_manager_wholesalers as imw')
                    ->join('investment_manager_wholesaler_zips as imwz', 'imw.UserId', '=', 'imwz.WholesalerId')
                    ->select('imwz.ZipCode')
                    ->where('imw.InvestmentManagerId', $investment_manager_id)
                    ->whereIn('imwz.ZipCode', $value)
                    ->pluck('imwz.ZipCode') );
            }
            
            if( count($response_zips) && !empty($response_zips) )
            {
                $validator->addReplacer('im_zip_verification', function($message, $attribute, $rule, $parameters) use ($response_zips) {
                    return str_replace([':zip'], $response_zips, $message);
                });

                return false;
            }
            else
            {
                return true;
            }
        }
    }

    // public function inZipVerifiedReplacer($message, $attribute, $rule, $parameters)
    // {
    //     dd($message, $attribute, $rule, $parameters);

    //     // str_replace([':min'], $parameters, $message);

    //     return $message . 'inside replacer';
    // }


    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function inUsaZipExits($attribute, $value, $parameters, $validator)
    {
        $zip2 = str_pad($value,5,"0",STR_PAD_LEFT);
        $zipCheck = DB::table('usa_zips')->select('City')->where('ZipCode', $zip2)->first();
        if($zipCheck){
            return true;
        }
        return false;
    }
}