<?php

/*
|--------------------------------------------------------------------------
| READ URLS
|--------------------------------------------------------------------------
|
*/

//http run on by default 80 port and https on 443 port...
//in laravel case $_SERVER['SCRIPT_NAME'] always contain index.php as the current file for all requests, so, easily able to get base path...
if (!defined('APP_BASE_URL')) {
    define('APP_BASE_URL', ( env('APP_URL') . dirname(dirname(@$_SERVER['SCRIPT_NAME']))) );
}
if (!defined('READ_BASE_URL')) {
    define('READ_BASE_URL', APP_BASE_URL . '/uploaded_files');
}
if (!defined('READ_PROFILE_PICS')) {
    define('READ_PROFILE_PICS', READ_BASE_URL . '/profile_pics/');
}

/* PROVIDER IMAGES */
if (!defined('READ_PROVIDER_PICS')) {
    define('READ_PROVIDER_PICS', READ_BASE_URL . '/provider_pics/');
}
if (!defined('READ_PROVIDER_300x300')) {
    define('READ_PROVIDER_300x300', READ_BASE_URL . '/provider_pics/300x300/');
}
if (!defined('READ_PROVIDER_300x169')) {
    define('READ_PROVIDER_300x169', READ_BASE_URL . '/provider_pics/300x169/');
}

/* SALE DESK USER IMAGES */
if (!defined('READ_SALE_DESK_USER_PICS')) {
    define('READ_SALE_DESK_USER_PICS', READ_BASE_URL . '/sale_desk_user_pics/');
}
if (!defined('READ_SALE_DESK_USER_300x300')) {
    define('READ_SALE_DESK_USER_300x300', READ_BASE_URL . '/sale_desk_user_pics/300x300/');
}
if (!defined('READ_SALE_DESK_USER_300x169')) {
    define('READ_SALE_DESK_USER_300x169', READ_BASE_URL . '/sale_desk_user_pics/300x169/');
}

/* INVESTMENT MANAGER IMAGES */
if (!defined('READ_INVESTMENT_MANAGER_PICS')) {
    define('READ_INVESTMENT_MANAGER_PICS', READ_BASE_URL . '/Investment_manager_pics/');
}
if (!defined('READ_INVESTMENT_MANAGER_300x300')) {
    define('READ_INVESTMENT_MANAGER_300x300', READ_BASE_URL . '/Investment_manager_pics/300x300/');
}
if (!defined('READ_INVESTMENT_MANAGER_300x169')) {
    define('READ_INVESTMENT_MANAGER_300x169', READ_BASE_URL . '/Investment_manager_pics/300x169/');
}
if (!defined('READ_FILE_TEMPLATES')) {
    define('READ_FILE_TEMPLATES', READ_BASE_URL . '/file_templates/');
}
if (!defined('READ_EMAIL_TEMPLATES')) {
    define('READ_EMAIL_TEMPLATES', READ_BASE_URL . '/email_templates_pics');
}

/* BROKER DEALER IMAGES */
if (!defined('READ_BROKER_DEALER_PICS')) {
    define('READ_BROKER_DEALER_PICS', READ_BASE_URL . '/broker_dealer_pics/');
}
if (!defined('READ_BROKER_DEALER_300x300')) {
    define('READ_BROKER_DEALER_300x300', READ_BASE_URL . '/broker_dealer_pics/300x300/');
}
if (!defined('READ_BROKER_DEALER_300x169')) {
    define('READ_BROKER_DEALER_300x169', READ_BASE_URL . '/broker_dealer_pics/300x169/');
}

/* Define Sales Desk path */
if (!defined('READ_SALES_DESK_PICS')) {
    define('READ_SALES_DESK_PICS', READ_BASE_URL . '/sales_desk_pics/');
}
if (!defined('READ_SALES_DESK_300x300')) {
    define('READ_SALES_DESK_300x300', READ_BASE_URL . '/sales_desk_pics/300x300/');
}
if (!defined('READ_SALES_DESK_300x169')) {
    define('READ_SALES_DESK_300x169', READ_BASE_URL . '/sales_desk_pics/300x169/');
}

/* Define product images path */
if (!defined('READ_PRODUCT_300x300')) {
    define('READ_PRODUCT_300x300', READ_BASE_URL . '/product_pics/300x300/');
}
if (!defined('CLIENT_ADMIN_SIDE_URL')) {
    define('CLIENT_ADMIN_SIDE_URL', env('APPLICATION_URL').'/401k-admin/');
}
if (!defined('CLIENT_FRONTEND_URL')) {
    define('CLIENT_FRONTEND_URL', env('APP_ENV') != 'local' ? env('APPLICATION_URL').'/' : env('APPLICATION_URL') .'/login-page/');
}
if (!defined('CLIENT_ADVISOR_SIDE_URL')) {
    define('CLIENT_ADVISOR_SIDE_URL', env('APPLICATION_URL').'/401k-frontend/auth/');
}
if (!defined('CLIENT_PROVIDER_SIDE_URL')) {
    define('CLIENT_PROVIDER_SIDE_URL', env('APPLICATION_URL').'/401k-provider/auth/');
}
if (!defined('CLIENT_FRONTEND_PROVIDER_SIDE_URL')) {
    define('CLIENT_FRONTEND_PROVIDER_SIDE_URL', env('APPLICATION_URL').'/401k-frontend/selecting-provider/');
}
if (!defined('READ_PDF_FILE')) {
    define('READ_PDF_FILE', READ_BASE_URL, 'write_pdf_files');
}

/*
|--------------------------------------------------------------------------
| WRITE PATHS
|--------------------------------------------------------------------------
|
*/


// define('READ_BASE_URL', ((!empty(@$_SERVER['HTTPS']) && @$_SERVER['HTTPS'] != 'off') || @$_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://" . @$_SERVER['HTTP_HOST'] . dirname(dirname(@$_SERVER['SCRIPT_NAME']))  . '/uploaded_files');
if (!defined('WRITE_UPLOADED_FILES_BASEPATH')) {
    define('WRITE_UPLOADED_FILES_BASEPATH', realpath(base_path() . '/uploaded_files/'));
}
//for public folder
if (!defined('DOWNLOAD_UPLOADED_FILES_BASEPATH')) {
    define('DOWNLOAD_UPLOADED_FILES_BASEPATH', realpath(base_path() . '/public/'));
}

/* PROVIDER IMAGES */
if (!defined('WRITE_PROFILE_PICS')) {
    define('WRITE_PROFILE_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/profile_pics/');
}
if (!defined('WRITE_PROVIDER_PICS')) {
    define('WRITE_PROVIDER_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/provider_pics/');
}
if (!defined('WRITE_PROVIDER_300x300')) {
    define('WRITE_PROVIDER_300x300', WRITE_UPLOADED_FILES_BASEPATH . '/provider_pics/300x300/');
}
if (!defined('WRITE_PROVIDER_300x169')) {
    define('WRITE_PROVIDER_300x169', WRITE_UPLOADED_FILES_BASEPATH . '/provider_pics/300x169/');
}


/* SALE DESK USER IMAGES */
if (!defined('WRITE_SALE_DESK_USER_PICS')) {
    define('WRITE_SALE_DESK_USER_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/sale_desk_user_pics/');
}
if (!defined('WRITE_SALE_DESK_USER_300x300')) {
    define('WRITE_SALE_DESK_USER_300x300', WRITE_UPLOADED_FILES_BASEPATH . '/sale_desk_user_pics/300x300/');
}
if (!defined('WRITE_SALE_DESK_USER_300x169')) {
    define('WRITE_SALE_DESK_USER_300x169', WRITE_UPLOADED_FILES_BASEPATH . '/sale_desk_user_pics/300x169/');
}


/* INVESTMENT MANAGER IMAGES */
if (!defined('WRITE_INVESTMENT_MANAGER_PICS')) {
    define('WRITE_INVESTMENT_MANAGER_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/Investment_manager_pics/');
}
if (!defined('WRITE_INVESTMENT_MANAGER_300x300')) {
    define('WRITE_INVESTMENT_MANAGER_300x300', WRITE_UPLOADED_FILES_BASEPATH . '/Investment_manager_pics/300x300/');
}
if (!defined('WRITE_INVESTMENT_MANAGER_300x169')) {
    define('WRITE_INVESTMENT_MANAGER_300x169', WRITE_UPLOADED_FILES_BASEPATH . '/Investment_manager_pics/300x169/');
}
if (!defined('WRITE_FILE_TEMPLATES')) {
    define('WRITE_FILE_TEMPLATES', WRITE_UPLOADED_FILES_BASEPATH . '/file_templates/');
}
if (!defined('READ_WRITE_TMP_FILES')) {
    define('READ_WRITE_TMP_FILES', WRITE_UPLOADED_FILES_BASEPATH . '/tmp_files/');
}
if (!defined('WRITE_EMAIL_TEMPLATES')) {
    define('WRITE_EMAIL_TEMPLATES', WRITE_UPLOADED_FILES_BASEPATH . '/email_templates_pics');
}
if (!defined('DOWNLOAD_FILE_TEMPLATES')) {
    define('DOWNLOAD_FILE_TEMPLATES', DOWNLOAD_UPLOADED_FILES_BASEPATH . '/file_templates/');
}


/* Broker Dealer IMAGES */
if (!defined('WRITE_BROKER_DEALER_PICS')) {
    define('WRITE_BROKER_DEALER_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/broker_dealer_pics/');
}
if (!defined('WRITE_BROKER_DEALER_300x300')) {
    define('WRITE_BROKER_DEALER_300x300', WRITE_UPLOADED_FILES_BASEPATH . '/broker_dealer_pics/300x300/');
}
if (!defined('WRITE_BROKER_DEALER_300x169')) {
    define('WRITE_BROKER_DEALER_300x169', WRITE_UPLOADED_FILES_BASEPATH . '/broker_dealer_pics/300x169/');
}

/* Sales Desk Read Images */
if (!defined('WRITE_SALES_DESK_PICS')) {
    define('WRITE_SALES_DESK_PICS', WRITE_UPLOADED_FILES_BASEPATH . '/sales_desk_pics/');
}
if (!defined('WRITE_SALES_DESK_300x300')) {
    define('WRITE_SALES_DESK_300x300', WRITE_UPLOADED_FILES_BASEPATH . '/sales_desk_pics/300x300/');
}
if (!defined('WRITE_SALES_DESK_300x169')) {
    define('WRITE_SALES_DESK_300x169', WRITE_UPLOADED_FILES_BASEPATH . '/sales_desk_pics/300x169/');
}


//Product Read Images
if (!defined('WRITE_PRODUCT_300x300')) {
    define('WRITE_PRODUCT_300x300', WRITE_UPLOADED_FILES_BASEPATH . '/product_pics/300x300/');
}

//For wholesalerpdf
if (!defined('WRITE_PDF_FILE')) {
    define('WRITE_PDF_FILE', WRITE_UPLOADED_FILES_BASEPATH . '/write_pdf_files/');
}

/*
|--------------------------------------------------------------------------
| TEST FILES PATH
|--------------------------------------------------------------------------
|
*/
if (!defined('TEST_FILES_BASEURL')) {
    define('TEST_FILES_BASEURL', realpath(base_path() . '/test_files/'));
}

return [
	
	'APP_BASE_URL' => APP_BASE_URL,
 	'TEST_PATH' => TEST_FILES_BASEURL,

	'WRITE_PROFILE_PICS' => WRITE_PROFILE_PICS,
	'READ_PROFILE_PICS' => READ_PROFILE_PICS,

	'WRITE_PDF_FILE' => WRITE_PDF_FILE,
	'READ_PDF_FILE' => READ_PDF_FILE,

	'WRITE_INVESTMENT_MANAGER_PICS' => WRITE_INVESTMENT_MANAGER_PICS,
	'READ_INVESTMENT_MANAGER_PICS' => READ_INVESTMENT_MANAGER_PICS,

	'WRITE_INVESTMENT_MANAGER_300x300' => WRITE_INVESTMENT_MANAGER_300x300,
	'READ_INVESTMENT_MANAGER_300x300' => READ_INVESTMENT_MANAGER_300x300,

	'WRITE_INVESTMENT_MANAGER_300x169' => WRITE_INVESTMENT_MANAGER_300x169,
	'READ_INVESTMENT_MANAGER_300x169' => READ_INVESTMENT_MANAGER_300x169,

	'WRITE_PROVIDER_PICS' => WRITE_PROVIDER_PICS,
	'READ_PROVIDER_PICS' => READ_PROVIDER_PICS,


	'WRITE_PROVIDER_300x300' => WRITE_PROVIDER_300x300,
	'READ_PROVIDER_300x300' => READ_PROVIDER_300x300,

	'WRITE_PROVIDER_300x169' => WRITE_PROVIDER_300x169,
	'READ_PROVIDER_300x169' => READ_PROVIDER_300x169,

    // Broker Dealer Images
    'WRITE_BROKER_DEALER_PICS' => WRITE_BROKER_DEALER_PICS,
    'READ_BROKER_DEALER_PICS' => READ_BROKER_DEALER_PICS,

    'WRITE_BROKER_DEALER_300x300' => WRITE_BROKER_DEALER_300x300,
    'READ_BROKER_DEALER_300x300' => READ_BROKER_DEALER_300x300,

    'WRITE_BROKER_DEALER_300x169' => WRITE_BROKER_DEALER_300x169,
    'READ_BROKER_DEALER_300x169' => READ_BROKER_DEALER_300x169,
    //End Broker Dealer

    // Sales Desk Images
    'WRITE_SALES_DESK_PICS' => WRITE_SALES_DESK_PICS,
    'READ_SALES_DESK_PICS' => READ_SALES_DESK_PICS,

    'WRITE_SALES_DESK_300x300' => WRITE_SALES_DESK_300x300,
    'READ_SALES_DESK_300x300' => READ_SALES_DESK_300x300,

    'WRITE_SALES_DESK_300x169' => WRITE_SALES_DESK_300x169,
    'READ_SALES_DESK_300x169' => READ_SALES_DESK_300x169,
    // End Sales desk images

    //Product Read Images
    'WRITE_PRODUCT_300x300' => WRITE_PRODUCT_300x300,
    'READ_PRODUCT_300x300' => READ_PRODUCT_300x300,

	'WRITE_SALE_DESK_USER_PICS' => WRITE_SALE_DESK_USER_PICS,
	'WRITE_SALE_DESK_USER_300x300' => WRITE_SALE_DESK_USER_300x300,
	'WRITE_SALE_DESK_USER_300x169' => WRITE_SALE_DESK_USER_300x169,

	'READ_SALE_DESK_USER_PICS' => READ_SALE_DESK_USER_PICS,
	'READ_SALE_DESK_USER_300x300' => READ_SALE_DESK_USER_300x300,
	'READ_SALE_DESK_USER_300x169' => READ_SALE_DESK_USER_300x169,

	'READ_FILE_TEMPLATES' => READ_FILE_TEMPLATES,
	'WRITE_FILE_TEMPLATES' => WRITE_FILE_TEMPLATES,

	'READ_EMAIL_TEMPLATES' => READ_EMAIL_TEMPLATES,
	'WRITE_EMAIL_TEMPLATES' => WRITE_EMAIL_TEMPLATES,
    'DOWNLOAD_FILE_TEMPLATES' => DOWNLOAD_FILE_TEMPLATES,

	'READ_WRITE_TMP_FILES' => READ_WRITE_TMP_FILES,

	'EMAIL_CONFIRMATION_TIMEOUT' => 300, //300 minutes means 5 hours... confirmation-email token will expire...
	'RESET_PASSWORD_TOKEN_TIMEOUT' => 300,

	'PASSWORD_RESET_LINK' => CLIENT_ADMIN_SIDE_URL . 'auth/verify_password_token/',
	'PASSWORD_RESET_LINK_FRONTEND' => CLIENT_FRONTEND_URL . 'verify_password_token/',
	'CLIENT_FRONTEND_PROVIDER_SIDE_URL' => CLIENT_FRONTEND_PROVIDER_SIDE_URL
];