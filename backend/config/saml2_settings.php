<?php

//This is variable is an example - Just make sure that the urls in the 'idp' config are ok.
//$idp_host = env('SAML2_IDP_HOST', 'http://localhost:8000/simplesaml');
$idp_host = env('SAML2_IDP_HOST', 'https://pmpqac.nfp.com');
return $settings = array(

    /**
     * If 'useRoutes' is set to true, the package defines five new routes:
     *
     *    Method | URI                      | Name
     *    -------|--------------------------|------------------
     *    POST   | {routesPrefix}/acs       | saml_acs
     *    GET    | {routesPrefix}/login     | saml_login
     *    GET    | {routesPrefix}/logout    | saml_logout
     *    GET    | {routesPrefix}/metadata  | saml_metadata
     *    GET    | {routesPrefix}/sls       | saml_sls
     */
    'useRoutes' => false,

    'routesPrefix' => '/saml2',

    /**
     * which middleware group to use for the saml routes
     * Laravel 5.2 will need a group which includes StartSession
     */
    'routesMiddleware' => ['saml2'],

    /**
     * Indicates how the parameters will be
     * retrieved from the sls request for signature validation
     */
    'retrieveParametersFromServer' => false,

    /**
     * Where to redirect after logout
     */
    'logoutRoute' => '/',

    /**
     * Where to redirect after login if no other option was provided
     */
    'loginRoute' => '/',


    /**
     * Where to redirect after login if no other option was provided
     */
    'errorRoute' => '/',




    /*****
     * One Login Settings
     */



    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => true, //@todo: make this depend on laravel config

    // Enable debug mode (to print errors)
    'debug' => env('APP_DEBUG', false),

    // If 'proxyVars' is True, then the Saml lib will trust proxy headers
    // e.g X-Forwarded-Proto / HTTP_X_FORWARDED_PROTO. This is useful if
    // your application is running behind a load balancer which terminates
    // SSL.
    'proxyVars' => false,

    // Service Provider Data that we are deploying
    'sp' => array(

        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        'x509cert' => env('SAML2_SP_x509',
            '-----BEGIN CERTIFICATE-----
MIIGOzCCBSOgAwIBAgIJAOHE4oRcSzIvMA0GCSqGSIb3DQEBCwUAMIG0MQswCQYD
VQQGEwJVUzEQMA4GA1UECBMHQXJpem9uYTETMBEGA1UEBxMKU2NvdHRzZGFsZTEa
MBgGA1UEChMRR29EYWRkeS5jb20sIEluYy4xLTArBgNVBAsTJGh0dHA6Ly9jZXJ0
cy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5LzEzMDEGA1UEAxMqR28gRGFkZHkgU2Vj
dXJlIENlcnRpZmljYXRlIEF1dGhvcml0eSAtIEcyMB4XDTE4MDUwNzA1MjkyMFoX
DTE5MDUwNzA1MjkyMFowPTEhMB8GA1UECxMYRG9tYWluIENvbnRyb2wgVmFsaWRh
dGVkMRgwFgYDVQQDDA8qLjQwMWtwbGFucy5jb20wggEiMA0GCSqGSIb3DQEBAQUA
A4IBDwAwggEKAoIBAQDA7Psk8KqWK1o80P3jo1TPU4muVWhXfqTLdVeLFG+1Bjqs
9kegnTOxXeq3AfsitomOkHV2gyt84h2HJRBujoQtH1a1+8XRg/8T9CwpdXg9Aq/w
c949i+VhcqfrJY8zKdjcU+356Tv3jfA2Tmx5/4GFmt3p38BTgtouf3lbB9/AVlP/
qxUJtWwdcPF/5R9HY0B2bjw7iXfmMHfWfOhQqU9tnS9PNFmHMlcXV6LnZlrzYF0d
hJ8zsdISp5+OiceBYpmzCj64ZMtz0OgkgrRPC4qx9Lgdj16dlgpYovmJxxtZhOW9
M2y4nnaoUX/PlUwteqcCrB0H54hXvGwI/QxuETSxAgMBAAGjggLEMIICwDAMBgNV
HRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAOBgNVHQ8B
Af8EBAMCBaAwNwYDVR0fBDAwLjAsoCqgKIYmaHR0cDovL2NybC5nb2RhZGR5LmNv
bS9nZGlnMnMxLTgyNy5jcmwwXQYDVR0gBFYwVDBIBgtghkgBhv1tAQcXATA5MDcG
CCsGAQUFBwIBFitodHRwOi8vY2VydGlmaWNhdGVzLmdvZGFkZHkuY29tL3JlcG9z
aXRvcnkvMAgGBmeBDAECATB2BggrBgEFBQcBAQRqMGgwJAYIKwYBBQUHMAGGGGh0
dHA6Ly9vY3NwLmdvZGFkZHkuY29tLzBABggrBgEFBQcwAoY0aHR0cDovL2NlcnRp
ZmljYXRlcy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5L2dkaWcyLmNydDAfBgNVHSME
GDAWgBRAwr0njsw0gzCiM9f7bLPwtCyAzjApBgNVHREEIjAggg8qLjQwMWtwbGFu
cy5jb22CDTQwMWtwbGFucy5jb20wHQYDVR0OBBYEFMkn8J26W7Se+CUT6RitR+Ye
YKQeMIIBBAYKKwYBBAHWeQIEAgSB9QSB8gDwAHUApLkJkLQYWBSHuxOizGdwCjw1
mAT5G9+443fNDsgN3BAAAAFjORPU6QAABAMARjBEAiBMVS0oFGST9aJrIrdIPo2g
kzSebZydemuiuV95jyHDMgIgCP0o46rhnLZ/xqNDgbPjBLkLJ0EgYMnyO2Z+JFV2
WfAAdwB0ftqDMa0zEJEhnM4lT0Jwwr/9XkIgCMY3NXnmEHvMVgAAAWM5E9hAAAAE
AwBIMEYCIQCLKkgbMjeyDrG7+aSh9LJsv6PJMMOspuas3HPfRzil3gIhANk5PD7x
uEi0B9Xn48AVM3icqBOhZbtRScAT2wJwTP9bMA0GCSqGSIb3DQEBCwUAA4IBAQAD
vFOrPESvT129XMTjht3T1v9CQpLj+b6v0iBUwUnl6hzVaTfmrKuu4q+V77igt77W
J2ys6oz2ibXrYoLBdnZo4v6SuyFD50nHbVgEHfEVZcOG2rwEimGoJ0ReHqGojV2T
1fiw+felUGouoWPmeM9UitUXYgkvtRfpFit9nmQ2GpMKyLktAWAppY3YJ06hCOL6
7JW8zB9Smp1uJNsTnhZY6u5WTbOT29dSeQnFh3EUBBm0cr5OFCj1gsf8FXFxNYSR
SzCOgakL3+4SOzOvVv67qXYnwAYI9YVZZwYYut+lMp0x1j7jZeQt+m6Gl7ArWTSm
Ptrp9/MiVPVL+m6tl/sl
-----END CERTIFICATE-----'
        ),
        'privateKey' => env('SAML2_SP_PRIVATEKEY',
            '-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDA7Psk8KqWK1o8
0P3jo1TPU4muVWhXfqTLdVeLFG+1Bjqs9kegnTOxXeq3AfsitomOkHV2gyt84h2H
JRBujoQtH1a1+8XRg/8T9CwpdXg9Aq/wc949i+VhcqfrJY8zKdjcU+356Tv3jfA2
Tmx5/4GFmt3p38BTgtouf3lbB9/AVlP/qxUJtWwdcPF/5R9HY0B2bjw7iXfmMHfW
fOhQqU9tnS9PNFmHMlcXV6LnZlrzYF0dhJ8zsdISp5+OiceBYpmzCj64ZMtz0Ogk
grRPC4qx9Lgdj16dlgpYovmJxxtZhOW9M2y4nnaoUX/PlUwteqcCrB0H54hXvGwI
/QxuETSxAgMBAAECggEALdHaz+osyZ+gPeNw+TiYcuknQPVd47HbV8sVsJ147mh9
VQjk+Bt95HkmvrJEv/jUcLyUeUIr+OwHEUrXPTqx37masXwLfng4A7gxKOOSXAMg
YB5UwQ9RujQt92YjPYaJz8JLOdiP+nqGe49iqhLp4x5l6B7RIcRpJzP1UY5sMNSm
IuwZ+Bpob4FJ49Dy91szlFfSKQII52smFObpvoF7Otb5SMOyCTgjmYWGrm6En6Zp
WZeSTtjioonJ23wRdwN3Xj2im4z63wloGe+rfInpnIus33gxEboQwlDZnJIQW1Tp
YRrcnViD1pBC2Vm9b8xJODzmxt7clTaki7TafNe51QKBgQDvOO+etZQrTc1AVrme
BuRvH66sGoWLbRSgwy5WO763NhnbrAB5aEP0YhZ+EUQUN3z/1SlNKctnqPnVOOYt
PskoeY4NN4ThylGStqgkHrVpSME2a3tm7977V7uQvWH7ArJJBSKvS7BXhDJtnhPF
wHAmuVkYwtHeUpusVNr4O3eaJwKBgQDOdNJPIbaRa4e1fBUsWMCO3e/1bgi+gyQB
kvQRV8lszrmNCtpbXFvDHELNPVYmVIzRxx3CqXNkjPeuUg4IcDVfGsc6UOezpjx9
fR7YSisaLt/28ey/pPq7DQxiPtsrNEd9jSaaUuib280076iGWe8ZkLgqnKe9hm6I
Cmy+7JS5ZwKBgDgBPWALpJ+qvEW2yAfuFybI5QzXvLISnISyjTOVEyYzXURndUj3
FwmsuRVUl3qgLvx5/Axbn4oqLis0AfdPvqpiItaHS+3MfuJ5Ls4d/lyistE5wo8r
0ylGtdvB6qFoVTLxJ9igQK2Mfn3ZR0nBrr/yHqDEY3vIHTfwOb9ptKt7AoGAZC1b
Pvpk0/sRRoaG35XRGYGnRbe2lnqbpgfgVen62skUT9PMUDhobivwXhY9DyxHiCLN
1f7S9x9Pr04xcPbBbvxXX3yScSFgiKOFq/2RHItSMUkCT5uF7e1O7P0C69vaNJmg
+ZDxPW9SCzWe2FIbKpSGSGbrHZN1Xp1wVtQE9lECgYEAx3kJtjlUpRiJ8QUNApps
Z3oxiXPfUiqhnu53nWF34uhpD8BQ34B81ZoSpbbjOLgPDwryTPCU1fprGF4+iGBN
lZJmGhReZZjaZjsvv1wuK1AfYFFG8+5F/xqnkFxjdj7Zppi1Fz0tsCRPZy5y9lZ8
A2CEXrSJSiiXtHcvvJJAyPI=
-----END PRIVATE KEY-----'
        ),

        // Identifier (URI) of the SP entity.
        // Leave blank to use the 'saml_metadata' route.
        'entityId' => env('SAML2_SP_ENTITYID',''),

        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array(
            // URL Location where the <Response> from the IdP will be returned,
            // using HTTP-POST binding.
            // Leave blank to use the 'saml_acs' route
            'url' => '',
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        // Remove this part to not include any URL Location in the metadata.
        'singleLogoutService' => array(
            // URL Location where the <Response> from the IdP will be returned,
            // using HTTP-Redirect binding.
            // Leave blank to use the 'saml_sls' route
            'url' => '',
        ),
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array(
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => env('SAML2_IDP_ENTITYID', $idp_host . '/saml2/idp/metadata.php'),
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array(
            // URL Target of the IdP where the SP will send the Authentication Request Message,
            // using HTTP-Redirect binding.
//            'url' => $idp_host . '/saml2/idp/SSOService.php',
//            'url' => $idp_host . '/saml2/logout',
            'url' => $idp_host . '/PMPRichUI.SAMLServices/api/Account/AuthenticateSAMLRequest',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array(
            // URL Location of the IdP where the SP will send the SLO Request,
            // using HTTP-Redirect binding.
            'url' => $idp_host . '/saml2/idp/SingleLogoutService.php',
        ),
        // Public x509 certificate of the IdP
        'x509cert' => env('SAML2_IDP_x509',
            '-----BEGIN CERTIFICATE-----
MIICxDCCAi2gAwIBAgIBADANBgkqhkiG9w0BAQ0FADB/MQswCQYDVQQGEwJ1czEL
MAkGA1UECAwCVFgxDDAKBgNVBAoMA05GUDERMA8GA1UEAwwIcnBhZy5jb20xDzAN
BgNVBAcMBkF1c3RpbjEMMAoGA1UECwwDTkZQMSMwIQYJKoZIhvcNAQkBFhRybWFz
Y2FyZW5oYXNAbmZwLmNvbTAeFw0xOTAxMjgxODIxMzNaFw0yMDAxMjgxODIxMzNa
MH8xCzAJBgNVBAYTAnVzMQswCQYDVQQIDAJUWDEMMAoGA1UECgwDTkZQMREwDwYD
VQQDDAhycGFnLmNvbTEPMA0GA1UEBwwGQXVzdGluMQwwCgYDVQQLDANORlAxIzAh
BgkqhkiG9w0BCQEWFHJtYXNjYXJlbmhhc0BuZnAuY29tMIGfMA0GCSqGSIb3DQEB
AQUAA4GNADCBiQKBgQCuXiVYf4a8vSDREN4ksGROCEzt70uK+yw/kbz8+nCJmVSF
x9+oXn31+PPn0XV7uAVPXWmT45ji+pwAbbmELL+aoYnQoGRtCKlvjUCN4vWkBYUS
Mk9vjY/Ebehy6mus8VbDznSIFL4ipvCqzPmuIKoqsdtj/m+Epe/7gy1bPq8rXwID
AQABo1AwTjAdBgNVHQ4EFgQUNHF2AuChbpS9N1kPItcmqHGM+LMwHwYDVR0jBBgw
FoAUNHF2AuChbpS9N1kPItcmqHGM+LMwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0B
AQ0FAAOBgQBfhWvb3WtSg9EMwLwz+hQnfRtgQwoUNyfF/AiWBifrqc1riMr4a0bk
dO5SIZAgp+9PtdM6XCargdUUbHm9gk47to0fbLIAEaSx3ys7Vyh+wttjWDIhhzBm
kfXQlvBPWrTk2RAF7LddYtRebosCC1gTwvQGge6XaImctKsAOzh5dQ==
-----END CERTIFICATE-----'),
        /*
         *  Instead of use the whole x509cert you can use a fingerprint
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
         */
        // 'certFingerprint' => '',
    ),



    /***
     *
     *  OneLogin advanced settings
     *
     *
     */
    // Security settings
    'security' => array(

        /** signatures and encryptions offered */

        // Indicates that the nameID of the <samlp:logoutRequest> sent by this SP
        // will be encrypted.
        'nameIdEncrypted' => false,

        // Indicates whether the <samlp:AuthnRequest> messages sent by this SP
        // will be signed.              [The Metadata of the SP will offer this info]
        'authnRequestsSigned' => false,

        // Indicates whether the <samlp:logoutRequest> messages sent by this SP
        // will be signed.
        'logoutRequestSigned' => false,

        // Indicates whether the <samlp:logoutResponse> messages sent by this SP
        // will be signed.
        'logoutResponseSigned' => false,

        /* Sign the Metadata
         False || True (use sp certs) || array (
                                                    keyFileName => 'metadata.key',
                                                    certFileName => 'metadata.crt'
                                                )
        */
        'signMetadata' => false,


        /** signatures and encryptions required **/

        // Indicates a requirement for the <samlp:Response>, <samlp:LogoutRequest> and
        // <samlp:LogoutResponse> elements received by this SP to be signed.
        'wantMessagesSigned' => false,

        // Indicates a requirement for the <saml:Assertion> elements received by
        // this SP to be signed.        [The Metadata of the SP will offer this info]
        'wantAssertionsSigned' => false,

        // Indicates a requirement for the NameID received by
        // this SP to be encrypted.
        'wantNameIdEncrypted' => false,

        // Authentication context.
        // Set to false and no AuthContext will be sent in the AuthNRequest,
        // Set true or don't present thi parameter and you will get an AuthContext 'exact' 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'
        // Set an array with the possible auth context values: array ('urn:oasis:names:tc:SAML:2.0:ac:classes:Password', 'urn:oasis:names:tc:SAML:2.0:ac:classes:X509'),
        'requestedAuthnContext' => true,
    ),

    // Contact information template, it is recommended to suply a technical and support contacts
    'contactPerson' => array(
        'technical' => array(
            'givenName' => 'name',
            'emailAddress' => 'no@reply.com'
        ),
        'support' => array(
            'givenName' => 'Support',
            'emailAddress' => 'no@reply.com'
        ),
    ),

    // Organization information template, the info in en_US lang is recomended, add more if required
    'organization' => array(
        'en-US' => array(
            'name' => 'Name',
            'displayname' => 'Display Name',
            'url' => 'http://url'
        ),
    ),

/* Interoperable SAML 2.0 Web Browser SSO Profile [saml2int]   http://saml2int.org/profile/current

   'authnRequestsSigned' => false,    // SP SHOULD NOT sign the <samlp:AuthnRequest>,
                                      // MUST NOT assume that the IdP validates the sign
   'wantAssertionsSigned' => true,
   'wantAssertionsEncrypted' => true, // MUST be enabled if SSL/HTTPs is disabled
   'wantNameIdEncrypted' => false,
*/

);
