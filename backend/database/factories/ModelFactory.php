<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name, 
        'profile_image' => $faker->imageUrl(400, 350, 'cats', true, 'Faker'),
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'role_name' => $faker->name,
        'role_description' => $faker->realText(20)
    ];
});

$factory->define(App\Models\Permission::class, function (Faker\Generator $faker) {
    return [
        'p_name' => $faker->name,
        'p_description' => $faker->realText(20)
    ];
});