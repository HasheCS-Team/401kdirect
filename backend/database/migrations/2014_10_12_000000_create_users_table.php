<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender')->nullable();
            $table->string('profile_image');
            $table->string('account_type')->default('normal');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->date('date_of_birth')->nullable();
            $table->boolean('is_verified')->default(false);
            $table->string('phone_number')->nullable();
            $table->string('home_address')->nullable();
            $table->string('bio_info')->nullable();
            $table->string('confirmation_code')->nullable();
            $table->string('country_name')->nullable();
            $table->string('country_code', 5)->nullable();
            $table->string('state_name', 50)->nullable();
            $table->string('city_name', 50)->nullable();
            $table->integer('postal_code')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('latitude', 20)->nullable();
            $table->string('longitude', 20)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
