<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('notification_creater_id');
            $table->integer('notification_receiver_id');
            $table->integer('content_id');
            $table->string('notification_type', 15);
            $table->integer('seen_count'); //if user view notification bar, notification count becomes zero
            $table->integer('viewed');  // user viewed particular post  or not...
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
