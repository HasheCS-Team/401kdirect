<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExcelCompanyInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excel_company_info', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('sf_spons_ein');
            $table->date('sf_tax_prd');
            $table->string('sf_plan_name');
            $table->string('sf_sponsor_name');
            $table->string('sf_sponsor_dfe_dba_name')->nullable();
            $table->string('sf_spons_us_address1')->nullable();
            $table->string('sf_spons_us_address2')->nullable();
            $table->string('sf_spons_us_city', 50);
            $table->string('sf_spons_us_state', 20);
            $table->string('sf_spons_us_zip', 20);
            $table->string('sf_spons_phone_num', 20)->nullable();
            $table->integer('sf_business_code');
            $table->integer('sf_partcp_account_bal_cnt')->nullable();
            $table->integer('sf_net_assets_eoy_amt');
            $table->integer('sf_tot_income_amt');
            $table->string('sf_admin_signed_name')->nullable();
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('excel_company_info');
    }
}
