<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTpaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('third_party_administrators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank');
            $table->string('tpa_master_name');
            $table->string('city');
            $table->string('state');
            $table->integer('qualifying_assets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('third_party_administrators');
    }
}
