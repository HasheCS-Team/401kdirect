<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductInvestmentOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_investment_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('fund_category_id');
            $table->decimal('one_year')->nullable();
            $table->decimal('three_year')->nullable();
            $table->decimal('five_year')->nullable();
            $table->decimal('ten_year')->nullable();
            $table->decimal('expense')->nullable();
            $table->tinyInteger('default_provider')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_investment_options');
    }
}
