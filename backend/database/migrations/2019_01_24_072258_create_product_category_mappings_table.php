<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoryMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('fund_category_name');
            $table->string('mapped_category')->nullable();
            $table->decimal('default_percentage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_category_mappings');
    }
}
