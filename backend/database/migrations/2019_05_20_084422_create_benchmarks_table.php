<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenchmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benchmarks', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('CompanyId');
            $table->timestamp('ProcessId');
            $table->json('CostingInfo');
            $table->timestamp('IsDeleted');
            $table->timestamp('CreatedDate');
            $table->timestamp('UpdatedDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('benchmarks');
    }
}
