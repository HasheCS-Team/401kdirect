<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints

    	App\Models\User::truncate();
        App\Models\Role::truncate();
        App\Models\Permission::truncate();
        DB::table('user_role')->truncate();  
        DB::table('role_permission')->truncate();  

        //seeding for users table...
    	factory(App\Models\User::class, 2)->create(['password' => bcrypt(123456)]);
        //seeding for roles table...
        $this->generateRoles();
        //seeding for permissions table...
        $this->generatePermissions();
        //seeding for user_role (many to many relationship) table...
        $this->generateUserRole();
        //seeding for role_permission (many to many relationship) table...
        $this->generateRolePermission();

        // $this->call(UsersTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }

    private function generateUserRole()
    {
        $user = App\Models\User::orderBy(DB::raw('RAND()'))->take(5)->get(['id'])->map(function($item){ return $item->id; })->toArray();
        $roles = App\Models\Role::orderBy(DB::raw('RAND()'))->take(5)->get(['id'])->map(function($item){ return $item->id; })->toArray();

        for ( $i = 0; $i < 5; $i++ )
        {
            shuffle($user);shuffle($roles);
            DB::table('user_role')->insert(['user_id' => $user[0], 'role_id' => $roles[0], "created_at" =>  \Carbon\Carbon::now(), "updated_at" => \Carbon\Carbon::now()]);
        }
    }

    private function generateRolePermission()
    {
        $roles = App\Models\Role::orderBy(DB::raw('RAND()'))->take(5)->get(['id'])->map(function($item){ return $item->id; })->toArray();
        $permissions = App\Models\Permission::orderBy(DB::raw('RAND()'))->take(10)->get(['id'])->map(function($item){ return $item->id; })->toArray();

        for ( $i = 0; $i < 10; $i++ )
        {
            shuffle($roles);shuffle($permissions);
            DB::table('role_permission')->insert(['permission_id' => $permissions[0], 'role_id' => $roles[0], "created_at" =>  \Carbon\Carbon::now(), "updated_at" => \Carbon\Carbon::now()]);
        }
    }

    private function generatePermissions()
    {
         $permissions = ['creat-user', 'edit-user', 'delete-user', 'update-user', 'creat-post', 'edit-post', 'delete-post', 'update-post', 'delete-company', 'update-company'];

        for ( $i = 0; $i < count($permissions); $i++ )
        {
            factory(App\Models\Permission::class, 1)->create(['p_name' => $permissions[$i]]);
        }
    }

    private function generateRoles()
    {
        $roles = ['Admin', 'Broker', 'Company', 'TPA'];

        for ( $i = 0; $i < count($roles); $i++ )
        {
            factory(App\Models\Role::class, 1)->create(['role_name' => $roles[$i]]);
        }
    }
}