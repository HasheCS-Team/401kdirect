import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LandingCompanyComponent } from './landing-company/landing-company.component';
import { LandingBenchmarkComponent } from './landing-benchmark/landing-benchmark.component';
import { LandingCompanyInformationComponent } from './landing-company-information/landing-company-information.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'select-company', component: LandingCompanyComponent },
  { path: 'company-information/:companyId', component: LandingCompanyInformationComponent },
  { path: 'benchmark', component: LandingBenchmarkComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: LayoutComponent, children: [{ path: 'home', component: DashboardComponent }] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
