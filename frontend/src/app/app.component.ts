import { Component } from '@angular/core';

export interface PassportResponse {
  // Properties
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-test';

  constructor() {}

}
