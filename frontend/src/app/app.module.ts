import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LandingCompanyComponent } from './landing-company/landing-company.component';
import { LandingBenchmarkComponent } from './landing-benchmark/landing-benchmark.component';
import { LandingCompanyInformationComponent } from './landing-company-information/landing-company-information.component';
import { MyPlanCardComponent } from './landing-benchmark/my-plan-card/my-plan-card.component';
import { CostCardComponent } from './landing-benchmark/cost-card/cost-card.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    DashboardComponent,
    LandingPageComponent,
    LandingCompanyComponent,
    LandingBenchmarkComponent,
    LandingCompanyInformationComponent,
    MyPlanCardComponent,
    CostCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
