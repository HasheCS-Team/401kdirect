import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cost-card',
  templateUrl: './cost-card.component.html',
  styleUrls: ['./cost-card.component.scss']
})
export class CostCardComponent implements OnInit {

  @Input() caption: String;
  @Input() color: String;
  @Input() cost: any;

  constructor() { }

  ngOnInit() {
  }

}
