import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingBenchmarkComponent } from './landing-benchmark.component';

describe('LandingBenchmarkComponent', () => {
  let component: LandingBenchmarkComponent;
  let fixture: ComponentFixture<LandingBenchmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingBenchmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingBenchmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
