import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-landing-benchmark',
  templateUrl: './landing-benchmark.component.html',
  styleUrls: ['./landing-benchmark.component.scss']
})
export class LandingBenchmarkComponent implements OnInit {

  benchmarkResult = {}
  myPlan = {
    currentTotalCost: 0,
    hardDollarAmount: 0,
    currentProviderId: 0,
    tpaId: 0
  }
  myPlanBox = {
    totalCostAmount: 0,
    totalCostPercentage: 0,

    perParticipant: 0,

    otherCostAmount: 0,
    otherCostPercentage: 0,

    InvestmentCostAmount: 0,
    InvestmentCostPercentage: 0
  }
  providers = []
  TPAs = []
  loadingDropdowns = false

  @ViewChild('myPlanModalClose', {static: false}) myPlanModalClose : ElementRef

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
    this.benchmarkResult = JSON.parse(localStorage.getItem('data-result'));
    this.populateMyPlanDropdowns()
  }

  toCurrency(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  populateMyPlanDropdowns(){
    this.loadingDropdowns = true
    this.companyService.getMyPlanProviderAndTpa().subscribe((result: any) => {
      this.providers = result.providers
      this.TPAs = result.tpa
      this.loadingDropdowns = false
    })
    if(localStorage.getItem('data-myplan') !== null){
      this.myPlan = JSON.parse(localStorage.getItem('data-myplan'))
    }
    if(localStorage.getItem('data-myplanbox') !== null){
      this.myPlanBox = JSON.parse(localStorage.getItem('data-myplanbox'))
    }
  }

  updateInformationSave(){
    localStorage.setItem('data-myplan', JSON.stringify(this.myPlan));
    let companyData = JSON.parse(localStorage.getItem('data')).company_data;
    this.companyService.getMyPlan({
      TotalCost: this.myPlan.currentTotalCost, 
      NetAssetsEoyAmount: companyData.FullPlanAssets,
      HardDollarAmount: this.myPlan.hardDollarAmount,
      ParticipentAccountBalance: companyData.ParticipentAccountBalance,
    }).subscribe((result: any) => {
      this.myPlanBox.totalCostAmount = result.totalCostAmount
      this.myPlanBox.totalCostPercentage = result.totalCostPercentage
      this.myPlanBox.otherCostAmount = result.otherCostAmount
      this.myPlanBox.otherCostPercentage = result.otherCostPercentage
      this.myPlanBox.InvestmentCostAmount = result.InvestmentCostAmount
      this.myPlanBox.InvestmentCostPercentage = result.InvestmentCostPercentage
      this.myPlanBox.perParticipant = result.perParticipant
      localStorage.setItem('data-myplanbox', JSON.stringify(this.myPlanBox));
      this.myPlanModalClose.nativeElement.click();
    })    
  }

}
