import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-my-plan-card',
  templateUrl: './my-plan-card.component.html',
  styleUrls: ['./my-plan-card.component.scss']
})
export class MyPlanCardComponent implements OnInit {

  @Input() myPlanBox: any;
  
  constructor() { }

  ngOnInit() {
  }

}
