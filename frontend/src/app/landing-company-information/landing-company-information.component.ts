import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from '../services/company.service';


@Component({
  selector: 'app-landing-company-information',
  templateUrl: './landing-company-information.component.html',
  styleUrls: ['./landing-company-information.component.scss']
})
export class LandingCompanyInformationComponent implements OnInit {

  companyId = 0;
  companyData = {
		SponsorName: "",
		Address1: "",
		City: "",
		State: "",
		ZipCode: "",
		AdminSignedName: "", 
		PhoneNumber: "",
		Email: "",
		PlanName: "",
		FullPlanAssets: "",
		TotalIncomeAmount: "",
		ParticipentAccountBalance: "",
		Percentage: "0.00",
		TpaFlag: "TPA",
		CurrentBroker: "0"
  }
  loadingInformation = false
  
  constructor(private companyService: CompanyService, private activatedRoute: ActivatedRoute, private router:Router) {
    this.activatedRoute.params.subscribe(params => {
      this.companyId = params['companyId'];
      this.getCompanyInformation(this.companyId);
    });
  }

  ngOnInit() {
    console.log("init")
  }

  getCompanyInformation(id){
    this.loadingInformation = true
    this.companyService.getDolCompany(id).subscribe((company: any) => {
      this.companyData.SponsorName = company.sf_sponsor_name
      this.companyData.Address1 = company.sf_spons_us_address1
      this.companyData.City = company.sf_spons_us_city
      this.companyData.State = company.sf_spons_us_state
      this.companyData.ZipCode = company.sf_spons_us_zip
      this.companyData.AdminSignedName = ""
      this.companyData.PhoneNumber = company.sf_spons_phone_num
      this.companyData.Email = ""
      this.companyData.PlanName = company.sf_plan_name
      this.companyData.FullPlanAssets = company.sf_net_assets_eoy_amt
      this.companyData.TotalIncomeAmount = company.sf_tot_income_amt
      this.companyData.ParticipentAccountBalance = company.sf_partcp_account_bal_cnt
      this.loadingInformation = false
    })
  }

  benchmark(){
    this.loadingInformation = true
    // 1. Make data objects
    let data = {
      company_data: {
        SponsorName: this.companyData.SponsorName,
        Address1: this.companyData.Address1,
        City: this.companyData.City,
        State: this.companyData.State,
        ZipCode: this.companyData.ZipCode,
        AdminSignedName: this.companyData.AdminSignedName,
        PhoneNumber: this.companyData.PhoneNumber,
        Email: this.companyData.Email,
        PlanName: this.companyData.PlanName,
        FullPlanAssets: this.companyData.FullPlanAssets,
        TotalIncomeAmount: this.companyData.TotalIncomeAmount,
        ParticipentAccountBalance: this.companyData.ParticipentAccountBalance,
        TpaFlag: this.companyData.TpaFlag,
        CurrentBroker: this.companyData.CurrentBroker,
      },
      annual_compensation: {
        percent: this.companyData.Percentage,
		    year: 1
      }
    }
    // 2. Store inside local storage
    localStorage.setItem('data', JSON.stringify(data));
    // 3. Post to benchmark service
    this.companyService.benchMark(data).subscribe((result: any) => {
      // 4. Fetch results & store on local storage as well
      localStorage.setItem('data-result', JSON.stringify(result));
      // 5. Remove myPlan data for now
      localStorage.removeItem('data-myplan');
      localStorage.removeItem('data-myplanbox');
      // 5. Redirect to benchmark page to display results
      this.loadingInformation = false
      this.router.navigate(['/benchmark']);
    })        
  }

}
