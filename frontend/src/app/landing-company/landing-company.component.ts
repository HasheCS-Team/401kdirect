import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../services/company.service';

@Component({
  selector: 'app-landing-company',
  templateUrl: './landing-company.component.html',
  styleUrls: ['./landing-company.component.scss']
})
export class LandingCompanyComponent implements OnInit {

  dolCompanies = []
  selectedCompanyId = 0
  searchCompany = ''
  companyLoading = false

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
    
  }

  search(searchString){
    this.dolCompanies = []
    if(searchString.length > 2){
      this.loadDolCompanies()
    }
  }

  selectCompany(companyId){
    this.selectedCompanyId = companyId
  }

  loadDolCompanies(){
    this.companyLoading = true
    this.companyService.getDolCompanies(this.searchCompany).subscribe((response: any) => {
      this.dolCompanies = response
      this.companyLoading = false
    })
  }

}
