import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
//import {Observable} from 'rxjs';

export interface LoginResponse {
  token: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 
  ngOnInit(): void {
    
  }

  email = '';
  password = '';

  constructor(private loginService:LoginService, private router:Router) {}

  login(){
    this.loginService.attempt(this.email, this.password).subscribe((response: LoginResponse) => {
      console.log(response);
      localStorage.setItem('token', response['access_token']);
      this.router.navigate(['/home']);
    });
  }

}
