import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http:HttpClient) {}

  getDolCompanies(searchCompany){
    return this.http.get(environment.apiUrl + '401k-direct/api/dol-companies/' + searchCompany);
  }

  getMyPlanProviderAndTpa(){
    return this.http.get(environment.apiUrl + '401k-direct/api/my-plan-providers-and-tpa');
  }

  getDolCompany(id){
    return this.http.get(environment.apiUrl + '401k-direct/api/dol-company/' + id);
  }

  benchMark(data){
    return this.http.post(environment.apiUrl + '401k-direct/api/benchmark', data);
  }

  getMyPlan(data){
    return this.http.post(environment.apiUrl + '401k-direct/api/myplan', data);
  }

}
