import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
//import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) {}

  attempt(email:string, password:string){
    let formData: FormData = new FormData();
    formData.append('username', email);
    formData.append('password', password);
    formData.append('grant_type', 'password');
    formData.append('client_id', environment.client_id.toString());
    formData.append('client_secret', environment.client_secret);

    return this.http.post(environment.apiUrl + 'oauth/token', formData)
  }
}
